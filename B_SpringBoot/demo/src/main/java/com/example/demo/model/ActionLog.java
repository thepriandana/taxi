package com.example.demo.model;

import java.time.LocalDateTime;

public class ActionLog {
    private EnumAction action;
    private LocalDateTime time;
    public EnumAction getAction() {
        return action;
    }
    public void setAction(EnumAction action) {
        this.action = action;
    }
    public LocalDateTime getTime() {
        return time;
    }
    public void setTime(LocalDateTime time) {
        this.time = time;
    }
    public ActionLog(EnumAction action) {
        this.action = action;
        this.time = LocalDateTime.now();
    }
    
    
}