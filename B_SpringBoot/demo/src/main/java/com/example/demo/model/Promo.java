package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;
@Document(collection = "Promo")
public class Promo{
    @Id
    private String id;
    private String title;
    private String body; //use html here
    private String dateStart;
    private String dateEnd;
    private String code;
    private float discount;
    public String getBody() {
        return body;
    }
    public void setBody(String body) {
        this.body = body;
    }
    public Promo(String title, String dateStart, String dateEnd, String code, float discount, String body) {
        this.title = title;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.code = code;
        this.discount = discount;
        this.body = body;
    }
    public String getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDateStart() {
        return dateStart;
    }
    public void setDateStart(String date) {
        this.dateStart = date;
    }
    public String getDateEnd() {
        return dateEnd;
    }
    public void setDateEnd(String date) {
        this.dateEnd = date;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public float getDiscount() {
        return discount;
    }
    public void setDiscount(float discount) {
        this.discount = discount;
    }
}
