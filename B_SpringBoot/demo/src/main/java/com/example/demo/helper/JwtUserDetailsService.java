package com.example.demo.helper;

import java.util.ArrayList;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.repository.TaxiRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    TaxiRepository repo;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<com.example.demo.model.User> u = repo.findByEmailIgnoringCase(username);
		if(u.isPresent()){
			return new User(u.get().getEmail(), u.get().getPassword(),
					new ArrayList<>());
		}else{
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}