package com.example.demo.helper;

// import java.io.BufferedOutputStream;
// import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.drew.imaging.ImageMetadataReader;
// import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
// import com.drew.metadata.file.FileSystemDirectory;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

  private final Path root = Paths.get("uploads");

  @Override
  public void init() {
    try {
      Files.createDirectories(root);
      Files.createDirectories(Paths.get("backups"));
    } catch (IOException e) {
      throw new RuntimeException("Could not initialize folder for upload!");
    }
  }

  @Override
  public void saveAs(MultipartFile file, String name) {
    try {
      Files.copy(file.getInputStream(), this.root.resolve(name), StandardCopyOption.REPLACE_EXISTING);
      Path fileX = root.resolve(name);
      Resource resource = new UrlResource(fileX.toUri());
      if (resource.exists() || resource.isReadable()){
        Metadata metadata = ImageMetadataReader.readMetadata(resource.getInputStream());
        ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
        if (directory != null) {
            directory.setDate(ExifSubIFDDirectory.TAG_DATETIME, new Date());
            directory.setDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL, new Date());
            directory.setDate(ExifSubIFDDirectory.TAG_DATETIME_DIGITIZED, new Date());
        }
        ExifIFD0Directory ifd0Dir = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
        if(ifd0Dir != null){
          ifd0Dir.setDate(ExifIFD0Directory.TAG_DATETIME, new Date());
          ifd0Dir.setDate(ExifIFD0Directory.TAG_DATETIME_ORIGINAL, new Date());
          ifd0Dir.setDate(ExifIFD0Directory.TAG_DATETIME_DIGITIZED, new Date());
        }
        GpsDirectory gpsDirectory = metadata.getFirstDirectoryOfType(GpsDirectory.class);
          if(gpsDirectory != null){
            gpsDirectory.setObject(GpsDirectory.TAG_DATE_STAMP, null);
            gpsDirectory.setObject(GpsDirectory.TAG_TIME_STAMP, null);
            gpsDirectory.setDate(GpsDirectory.TAG_DATETIME, null);
            gpsDirectory.setDate(GpsDirectory.TAG_DATETIME_ORIGINAL, null);
            gpsDirectory.setDate(GpsDirectory.TAG_DATETIME_DIGITIZED, null);
          }
          // BufferedOutputStream stream= new BufferedOutputStream(new FileOutputStream(Paths.get("backups").resolve(name)));;
          // stream.write(resource.getInputStream().readAllBytes());
          // stream.close();
        }
      } catch (Exception e) {
        if (e instanceof FileAlreadyExistsException) {
          throw new RuntimeException("A file of that name already exists.");
        }
        throw new RuntimeException(e.getMessage());
    }
  }

  @Override
  public void save(MultipartFile file) {
    try {
      Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
    } catch (Exception e) {
      if (e instanceof FileAlreadyExistsException) {
        throw new RuntimeException("A file of that name already exists.");
      }

      throw new RuntimeException(e.getMessage());
    }
  }

  @Override
  public Resource load(String filename) {
    try {
      Path file = root.resolve(filename);
      Resource resource = new UrlResource(file.toUri());

      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("Could not read the file!");
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("Error: " + e.getMessage());
    }
  }

  @Override
  public void deleteAll() {
    FileSystemUtils.deleteRecursively(root.toFile());
  }

  @Override
  public Stream<Path> loadAll() {
    try {
      return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
    } catch (IOException e) {
      throw new RuntimeException("Could not load the files!");
    }
  }
}
