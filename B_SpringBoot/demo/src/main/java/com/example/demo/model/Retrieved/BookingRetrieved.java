package com.example.demo.model.Retrieved;

import com.example.demo.model.Booking;
import com.example.demo.model.LatLng;

public class BookingRetrieved extends Booking{
    private String promo;
    public BookingRetrieved(String payment, LatLng startLoc, String startLocName, LatLng endLoc, String endLocName,
            String note, String rideClass, String promo) {
        super(payment, startLoc, startLocName, endLoc, endLocName, note);
        super.setRideClass(rideClass);
        this.promo = promo;
    }
    public String getPromo() {
        return promo;
    }
    public void setPromo(String promo) {
        this.promo = promo;
    }

}
