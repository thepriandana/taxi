package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.MessageResponse;
import com.example.demo.helper.Settings;
import com.example.demo.model.Booking;
import com.example.demo.model.Driver;
import com.example.demo.model.EnumPayment;
import com.example.demo.model.LatLng;
import com.example.demo.model.Payment;
import com.example.demo.model.User;
import com.example.demo.model.Conveter.DashboardDate;
import com.example.demo.model.PaymentMethod;
import com.example.demo.model.RideClass;
import com.example.demo.model.Car;
import com.example.demo.model.Parsing.CustomJob;
import com.example.demo.model.Retrieved.BookingRetrieved;
import com.example.demo.repository.BookingRepository;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.TaxiRepository;
import com.example.demo.repository.PaymentMethodRepository;
import com.example.demo.repository.PaymentRepository;
import com.example.demo.repository.RideClassRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class BookingController {
	final String apiLoc = Settings.BookingAPI;
	@Value("${jwt.secret}")
	private String secret;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	TaxiRepository userRepo;
	@Autowired
	CarRepository carRepo;
	@Autowired
	RideClassRepository rideRepo;
	@Autowired
	BookingRepository bookingRepo;
	@Autowired
	PaymentMethodRepository paymentMRepository;
	@Autowired
	PaymentRepository paymentRepository;
	//find current active api?/booking/active
	@GetMapping(apiLoc+"/dashboard")
	public ResponseEntity<?> dashboard(@RequestHeader (name="Authorization") String token){
		try {
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(userq.isPresent() && userq.get().getRoles().equals("admin")){
					//number of new booking and still requested today, new car request, new payment otg
					//total new boking today and total profit today
					HashMap<String, Object> information = new HashMap<String, Object>();
					List<DashboardDate> o = bookingRepo.groupByStartTime();
					java.util.Date date= new java.util.Date();
					Calendar cal = Calendar.getInstance();
					// cal.setTime(date);
					Calendar cal2 = Calendar.getInstance(); //tommorrow
					cal2.add(Calendar.DATE, 1);
					Calendar calX1 = Calendar.getInstance(); //yesterday
					calX1.add(Calendar.DATE, -1);
					Calendar calX2 = Calendar.getInstance(); //yesterday
					calX2.add(Calendar.DATE, -2);

					List<RideClass> rc = rideRepo.findAll();
					List<Booking> todayBookings = bookingRepo.findByStartTimeBetween(calX1.getTime(), cal.getTime());
					Float todayRevenue = Float.parseFloat("0.0");
					Float todayDriverIncome = Float.parseFloat("0.0");
					int todayCompletion = 0;
					List<Booking> activeList = new ArrayList<>();
					for(Booking b: todayBookings){
						if(b.getStatus() == 3){
							RideClass c = rc.stream().filter(x->x.getId().equals(b.getRideClass())).collect(Collectors.toList()).get(0);
							todayDriverIncome += Float.parseFloat(c.getDriverIncome()) * (float) b.getTotalKM();
							todayRevenue += (Float.parseFloat(c.getPrice()) - Float.parseFloat(c.getDriverIncome())) * (float) b.getTotalKM();
							todayCompletion++;
							System.out.println(c.toString());
						}
						if(b.getStatus() == 1){
							activeList.add(b);
						}
					}

					List<Booking> yesterdayBookings = bookingRepo.findByStartTimeBetween(calX1.getTime(), calX2.getTime());
					Float yesterdayRevenue = Float.parseFloat("0.0");
					Float yesterdayDriverIncome = Float.parseFloat("0.0");
					int yesterdayCompletion = 0;
					for(Booking b: yesterdayBookings){
						if(b.getStatus() == 3){
							RideClass c = rc.stream().filter(x->x.getId().equals(b.getRideClass())).collect(Collectors.toList()).get(0);
							yesterdayDriverIncome += Float.parseFloat(c.getDriverIncome()) * (float) b.getTotalKM();
							yesterdayRevenue += (Float.parseFloat(c.getPrice()) - Float.parseFloat(c.getDriverIncome())) * (float) b.getTotalKM();
							yesterdayCompletion++;
						}
					}

					int yesterdayBooking = yesterdayBookings.size();

					int todayBooking = todayBookings.size();
					int monthBooking = o.stream().filter(c -> c.getMonth() == cal.get(Calendar.MONTH)+1 && c.getYear() == cal.get(Calendar.YEAR)).mapToInt(x -> x.getCount()).sum();
					int yearBooking = o.stream().filter(c -> c.getYear() == cal.get(Calendar.YEAR)).mapToInt(x -> x.getCount()).sum();;

					int todayGoal = (int) Math.ceil(o.stream().filter(c -> c.getDayOfMonth() == cal.get(Calendar.DAY_OF_MONTH) && c.getMonth() == cal.get(Calendar.MONTH)+1).mapToInt(x -> x.getCount()).average().orElse(0.0));
					int monthGoal = (int) Math.ceil(o.stream().filter(c -> c.getMonth() == cal.get(Calendar.MONTH)+1).mapToInt(x -> x.getCount()).average().orElse(0.0));
					int yearGoal = (int) Math.ceil(o.stream().filter(c -> c.getYear() == c.getYear()).mapToInt(x -> x.getCount()).average().orElse(0.0));
					information.put("todayRevenue", todayRevenue);
					information.put("todayDriver", todayDriverIncome);
					information.put("todayBooking", todayBooking);
					information.put("todayCompletion", todayCompletion);


					information.put("yesterdayRevenue", yesterdayRevenue);
					information.put("yesterdayDriver", yesterdayDriverIncome);
					information.put("yesterdayBooking", yesterdayBooking);
					information.put("yesterdayCompletion", yesterdayCompletion);

					information.put("monthBooking", monthBooking); //find average of each day
					information.put("yearBooking", yearBooking); //find average of each day
					information.put("todayGoal", todayGoal); //find average of each day
					information.put("monthGoal", monthGoal); //find average of each day
					information.put("yearGoal", yearGoal); //find average of each day

					information.put("bookingToday", todayBookings);
					information.put("activeToday", activeList);
					information.put("monthActivity", o.stream().filter(c -> c.getMonth() == cal.get(Calendar.MONTH)+1 && c.getYear() == cal.get(Calendar.YEAR)));
					return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(information));
				}
			}
			return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e){
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping(apiLoc+"/nav")
	public ResponseEntity<?> nav(@RequestHeader (name="Authorization") String token){
		try {
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(userq.isPresent() && userq.get().getRoles().equals("admin")){
					//number of new booking and still requested today, new car request, new payment otg
					//total new boking today and total profit today
					int totalCar = carRepo.findByVerifiedIsFalse().size();
					int activeBookingx = bookingRepo.findByStatus(0).size();
					int statusPayment = paymentRepository.findByStatus(0).size();
					HashMap<String, Object> information = new HashMap<String, Object>();
					information.put("car", totalCar); //new verication car
					information.put("booking", activeBookingx); //requested booking
					information.put("payment", statusPayment);
					return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(information));
				}
			}
			return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e){
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping(apiLoc+"/active")
	public ResponseEntity<?> getActiveOrder(@RequestHeader (name="Authorization") String token){
		try {
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(userq.isPresent()){
					Pageable dummy = PageRequest.of(0, 1);
					List<Booking> tmpList = new ArrayList<Booking>();
					if(userq.get().getRoles().equals("driver")){
						Driver drv = (Driver) userq.get();
						tmpList.addAll(bookingRepo.findAllByDriverOrderByIdDesc(userq.get().getId(), dummy).get());
					}else{
						tmpList.addAll(bookingRepo.findByCustomerOrderByIdDesc(userq.get().getId(), dummy).get());
					}
					if(tmpList.size()>0){
						return ResponseEntity.ok(new MessageResponse(tmpList.get(0)));
					}
					return ResponseEntity.ok(new MessageResponse("Nothing"));
				}
			}
			return new ResponseEntity<>(new MessageResponse("Token"), HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e){
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping(apiLoc+"/cancel")
	public ResponseEntity<?> cancelActiveOrder(@RequestHeader (name="Authorization") String token){
		try {
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(userq.isPresent()){
					Pageable dummy = PageRequest.of(0, 1);
					Optional<List<Booking>> bookings = bookingRepo.findByCustomerOrderByIdDesc(userq.get().getId(), dummy);
					if(bookings.isPresent()){
						if(bookings.get().get(0).getStatus() == 0){
							bookings.get().get(0).setStatus(2);
							bookingRepo.save(bookings.get().get(0));
						}
					}
					return ResponseEntity.ok(new MessageResponse("Nothing"));
				}
			}
			return new ResponseEntity<>(new MessageResponse("Token"), HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e){
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping(apiLoc+"/rating")
	public ResponseEntity<?> setRating(@RequestBody Booking data, @RequestHeader (name="Authorization") String token){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && (_user.get().getRoles().equals("admin") || _user.get().getRoles().equals("client"))){
					Optional<Booking> b = bookingRepo.findById(data.getId());
					if(b.isPresent() && (b.get().getRating() == 0)){
						Booking _data = b.get();
						_data.setRating(data.getRating());
						bookingRepo.save(_data);
						return ResponseEntity.ok(new MessageResponse("Success"));
					}
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
		}catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"+e.getMessage()));
		}
	}

	@GetMapping(apiLoc)
	public ResponseEntity<?> getAll(@RequestHeader (name="Authorization") String token, @RequestParam(required = false) String customer, @RequestParam(required = false) String driver){
		try {
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				List<Booking> vList = new ArrayList<Booking>();
				if(userq.isPresent()){
					if(userq.get().getRoles().equals("admin")){
						if(customer != null && !customer.equals("")){
							vList.addAll(bookingRepo.findBookingByCustomer(customer));
						}else if(driver != null && !driver.equals("")){
							vList.addAll(bookingRepo.findBookingByDriver(driver));
						}else{
							vList.addAll(bookingRepo.findAll());
						}
					}else if(userq.get().getRoles().equals("client")){
						vList.addAll(bookingRepo.findBookingByCustomer(userq.get().getId()));
					}else if(userq.get().getRoles().equals("driver")){
						vList.addAll(bookingRepo.findBookingByDriver(userq.get().getId()));
					}
					return new ResponseEntity<>(vList, HttpStatus.OK);
				}
			}
			return new ResponseEntity<>(new MessageResponse("Token"), HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e){
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping(apiLoc+"/{id}")
	public ResponseEntity<?> getByID(@RequestHeader (name="Authorization") String token, @PathVariable("id") String id){
		try{
			//use for getting the detail only (one)
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				Optional<Booking> b = bookingRepo.findById(id);
				if(b.isPresent() && userq.isPresent()){
					if(userq.get().getRoles().equals("admin")){
						return ResponseEntity.ok(new MessageResponse(b.get()));
					}else if(b.get().getdriver() != null && b.get().getdriver().equals(userq.get().getId())){
						return ResponseEntity.ok(new MessageResponse(b.get()));
					}else if(b.get().getcustomer() != null  && b.get().getcustomer().equals(userq.get().getId())){
						return ResponseEntity.ok(new MessageResponse(b.get()));
					}
				}
			}
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}catch(Exception e){
			return new ResponseEntity<>(e,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping(apiLoc)
	public ResponseEntity<?> create(@RequestBody BookingRetrieved data, @RequestHeader (name="Authorization") String token){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && (_user.get().getRoles().equals("admin") || _user.get().getRoles().equals("client"))){
					Optional<PaymentMethod> paymentM = paymentMRepository.findById(data.getPayment());
					Optional<RideClass> rideC = rideRepo.findById(data.getRideClass());
					if(paymentM.isPresent() && rideC.isPresent()){
						//Total KM * id ride class
						int totalKM = 0;
						WebClient client = WebClient.create();
						ResponseEntity<String> response = client.get()
						.uri("https://maps.googleapis.com/maps/api/distancematrix/json?destinations="+
						data.getStartLoc().getLat()+","+data.getStartLoc().getLng()
						+"&origins="+ data.getEndLoc().getLat()+","+data.getEndLoc().getLng()
						+"&key=")
						.retrieve()
						.toEntity(String.class)
						.block();
						if(response.getStatusCodeValue() == 200){
							JSONObject  jsonObject = new JSONObject(response.getBody());
							totalKM = jsonObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance").getInt("value");

							Float totalPrice = totalKM * Float.parseFloat(rideC.get().getPrice());
							String last="";
							if(_user.get().getRoles().equals("admin")){
								paymentRepository.save(new Payment(data.getcustomer(), data.getPayment(), data.getPromo(), EnumPayment.taxi_payment, totalPrice));
								last = paymentRepository.findByUserIDOrderByIdDesc(data.getcustomer()).get().getId();
							}else if(_user.get().getRoles().equals("client")){
								paymentRepository.save(new Payment(_user.get().getId(), data.getPayment(), data.getPromo(), EnumPayment.taxi_payment, totalPrice));
								last = paymentRepository.findByUserIDOrderByIdDesc(_user.get().getId()).get().getId();
							}

							// add make payment first
							Booking _data = new Booking(last,
							data.getStartLoc(), data.getStartLocName(),
							data.getEndLoc(), data.getEndLocName(),
							data.getNote());
							if(_user.get().getRoles().equals("admin") == false){
								_data.setcustomer(_user.get().getId());
							}
							// //set total km
							_data.setTotalKM(totalKM/1000);
							return ResponseEntity.ok(new MessageResponse(bookingRepo.save(_data)));
						}
					}
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
		}catch (Exception e){
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"+e.getMessage()));
		}
	}

	@PutMapping(apiLoc+"/{id}")
	public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody Booking xdata){
		try{
			Optional<Booking> b = bookingRepo.findById(id);
			if(b.isPresent()){
				Booking _data = b.get();
				// _data.setDriverID(xdata.getDriverID());
				// _data.setDriverName(xdata.getDriverName());
				// _data.setPayment(xdata.getPayment());
				_data.setNote(xdata.getNote());
				// _data.setPromoID(xdata.getPromoID());
				_data.setRating(xdata.getRating());
				_data.setStartLoc(xdata.getStartLoc());
				_data.setStartLocName(xdata.getStartLocName());
				_data.setStartTime(xdata.getStartTime());
				_data.setEndLoc(xdata.getEndLoc());
				_data.setEndLocName(xdata.getEndLocName());
				_data.setEndTime(xdata.getEndTime());
				_data.setStatus(xdata.getStatus());
				bookingRepo.save(_data);
				return new ResponseEntity<>(HttpStatus.OK);
			}
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping(apiLoc+"/{id}")
	public ResponseEntity<Booking> delete(@PathVariable("id") String id, @RequestHeader (name="Authorization") String token){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && _user.get().getRoles().equals("admin")){
					bookingRepo.deleteById(id);
					return new ResponseEntity<>(HttpStatus.OK);
				}
			}
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping(apiLoc+"/job")
	public ResponseEntity<?> findNearbyJob(@RequestHeader (name="Authorization") String token, @RequestBody LatLng latLng){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && (_user.get().getRoles().equals("admin") || _user.get().getRoles().equals("driver"))){
					if(_user.get().getRoles().equals("driver")){
						Driver drv = (Driver) _user.get();
						if(drv.isOffline()){
							return ResponseEntity.badRequest().body(new MessageResponse("Driver offline"));
						}else if(drv.isTakingJob()){
							return ResponseEntity.badRequest().body(new MessageResponse("Already on job"));
						}
					}

					Double lat0 = Double.valueOf(latLng.getLat()), radius = 0.0005, lng0 = Double.valueOf(latLng.getLng());
					Double lat1=lat0 - (radius/111.12), lat2=lat0 + (radius/111.12),
					lng1=lng0 - ((radius*2)/111.2)*(Math.cos(lng0)), lng2=lng0 + ((radius*2)/111.2)*(Math.cos(lng0));
					List<Booking> list = bookingRepo.findBookingByStartLoc(lat1, lng1, lat2, lng2);
					List<CustomJob> vList = new ArrayList<CustomJob>();
					vList.addAll(list.stream().map(u -> new CustomJob(u)).toList());
					return ResponseEntity.ok(new MessageResponse(vList));
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wong"));
		}catch(Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}
	}
	@PostMapping(apiLoc+"/takejob")
	public ResponseEntity<?> takeJob(@RequestBody String idX, @RequestHeader (name="Authorization") String token){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && _user.get().getRoles().equals("driver")){
					Pageable dummy = PageRequest.of(0, 1);
					List<Booking> tmpList = new ArrayList<Booking>();
					tmpList.addAll(bookingRepo.findAllByDriverOrderByIdDesc(_user.get().getId(), dummy).get());
					if(tmpList.size() >0 && tmpList.get(0).getStatus() == 1){
						return ResponseEntity.badRequest().body(new MessageResponse("Please finish the last order"));
					}
					Optional<Booking> c = bookingRepo.findById(idX);
					if(c.isPresent() && c.get().getStatus() == 0){
						c.get().setdriver(_user.get().getId());
						c.get().setStatus(1);
						bookingRepo.save(c.get());

						Driver driver = (Driver) _user.get();
						driver.setTakingJob(true);
						userRepo.save(driver);
						return ResponseEntity.ok(new MessageResponse("Success"));
					}
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}catch(Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}
	}

	@PostMapping(apiLoc+"/updateLastLocation")
	public ResponseEntity<?> updateLocation(@RequestBody LatLng latLng, @RequestHeader (name="Authorization") String token){
		try{
			//get the last job, update the latitute and longitude
			//response with message if exist, status of job,
			//lat lng, start and end destination
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && _user.get().getRoles().equals("driver")){
					Driver driver = (Driver) _user.get();
					if(driver.isTakingJob()){
						Pageable dummy = PageRequest.of(0, 1);
						Optional<List<Booking>> b = bookingRepo.findBookingByDriverOrderByIdDesc(_user.get().getId(), dummy);
						if(b.isPresent()){
							if(b.get().get(0).getStatus() == 1){
								//there is still active order
								b.get().get(0).setDriverLoc(latLng);
								bookingRepo.save(b.get().get(0));
								return ResponseEntity.ok(new MessageResponse("Success"));
							}
						}
					}
					return ResponseEntity.ok(new MessageResponse("Not in active job"));
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}catch(Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}
	}
	@GetMapping(apiLoc+"/finishJob")
	public ResponseEntity<?> finishJob(@RequestHeader (name="Authorization") String token){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && _user.get().getRoles().equals("driver")){
					Pageable dummy = PageRequest.of(0, 1);
					Optional<List<Booking>> b = bookingRepo.findBookingByDriverOrderByIdDesc(_user.get().getId(), dummy);
					if(b.isPresent()){
						if(b.get().get(0).getStatus() > 1){
							Driver driver = (Driver) _user.get();
							driver.setTakingJob(false);
							//check the driver car first
							Optional<Car> c = carRepo.findBydriver(_user.get().getId());
							if(c.isPresent()){
								Optional<RideClass> rc = rideRepo.findById(c.get().getRideClass());
								if(rc.isPresent()){
									driver.addBalance(Float.valueOf(rc.get().getPrice()));
									userRepo.save(driver);
									b.get().get(0).setStatus(3);
									bookingRepo.save(b.get().get(0));
									return ResponseEntity.ok(new MessageResponse("Success"));
								}
							}
						}
					}
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}catch(Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}
	}
	@GetMapping(apiLoc+"/pickUp")
	public ResponseEntity<?> pickUp(@RequestHeader (name="Authorization") String token){
		try{
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				Optional<User> _user = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
				if(_user.isPresent() && _user.get().getRoles().equals("driver")){
					Pageable dummy = PageRequest.of(0, 1);
					Optional<List<Booking>> b = bookingRepo.findBookingByDriverOrderByIdDesc(_user.get().getId(), dummy);
					if(b.isPresent()){
						if(b.get().get(0).getStatus() == 1){
							b.get().get(0).setStatus(91);
							bookingRepo.save(b.get().get(0));
							return ResponseEntity.ok(new MessageResponse("Success"));
						}
					}
				}
			}
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}catch(Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Something Wrong"));
		}
	}
}
