package com.example.demo.model.Parsing;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.model.User;
public class CustomUser {
    private String id;
    private String name;
    private Float balance;
    private String email;
    public CustomUser(User u){
        this.id= u.getId();
        this.name = u.getName();
        this.balance = u.getBalance();
        this.email = u.getEmail();
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Float getBalance() {
        return balance;
    }
    public void setBalance(Float balance) {
        this.balance = balance;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
}