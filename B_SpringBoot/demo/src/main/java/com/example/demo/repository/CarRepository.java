package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Car;

public interface CarRepository extends MongoRepository<Car, String>{
    Car findByPlate(String plate);
    @Query("FROM Car c WHERE c.lat >= :lat1 AND c.lat <= :lat2 AND c.lng >= :lng1 AND c.lng <= :lng2")
    List<Car> findCarByLatAndLng(@Param("lat1") Double lat1, @Param("lng1") Double lng1, @Param("lat2") Double lat2, @Param("lng2") Double lng2);
    @Query("FROM Car c WHERE c.driver = :drv")
    Optional<Car> findBydriver(@Param("drv") String driver);
    List<Car> findByVerifiedIsFalse();
    // List<Car> findCarByLatIsLessThanEqualLat1(Double lat1, Double lng1, Double lat2, Double lng2);
    // AndLatGreaterThanEqualLat2AndLngLessThanEqualLng1AndLngGreaterThanEqualLng2
}
