package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;
@Document(collection = "RideClass")
public class RideClass{
    @Id
    private String id;
    private String className;
    private String price; //per /KM
    private String driverIncome; //per /KM
    private int pax; //maximum customer pax
    public RideClass(String className, String price, String driverIncome, int pax) {
        this.className = className;
        this.price = price;
        this.pax = pax;
        this.driverIncome = driverIncome;
    }
    public String getId() {
        return id;
    }
    public String getClassName() {
        return className;
    }
    public void setClassName(String className) {
        this.className = className;
    }
    public String getDriverIncome() {
        return driverIncome;
    }
    public void setDriverIncome(String driverIncome) {
        this.driverIncome = driverIncome;
    }
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public int getPax() {
        return pax;
    }
    public void setPax(int pax) {
        this.pax = pax;
    }
}