package com.example.demo.model;

public class LocationLog {
    private String id_location_log;
    private LatLng location;
    private String id_users;
    public String getId_location_log() {
        return id_location_log;
    }
    public void setId_location_log(String id_location_log) {
        this.id_location_log = id_location_log;
    }
    public LatLng getLocation() {
        return location;
    }
    public void setLocation(LatLng location) {
        this.location = location;
    }
    public String getId_users() {
        return id_users;
    }
    public void setId_users(String id_users) {
        this.id_users = id_users;
    }
    public LocationLog(String id_location_log, LatLng location, String id_users) {
        this.id_location_log = id_location_log;
        this.location = location;
        this.id_users = id_users;
    }
    
}
