package com.example.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Withdrawal;

public interface WithdrawalRepository extends MongoRepository<Withdrawal, String>{
    
    List<Withdrawal> findWithdrawalByUserID(@Param("userID") String userID);
}
