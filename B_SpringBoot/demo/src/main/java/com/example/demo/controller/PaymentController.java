package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.Settings;
import com.example.demo.model.Payment;
import com.example.demo.model.User;
import com.example.demo.repository.PaymentRepository;
import com.example.demo.repository.TaxiRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials = "true")
@RestController
@RequestMapping(Settings.ApiMap)
public class PaymentController {
    final String apiLoc = Settings.PaymentAPI;
    @Value("${jwt.secret}")
    private String secret;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    TaxiRepository userRepo;
    @Autowired
    PaymentRepository paymentRepository;

    @GetMapping(apiLoc)
    public ResponseEntity<?> getAllPayments(@RequestHeader(name = "Authorization") String token) {
        try {
            jwtTokenUtil = new JwtTokenUtil();
            if (token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
                if (userq.isPresent()) {
                    if (userq.get().getRoles().equals("admin")) {
                        List<Payment> list = new ArrayList<Payment>();
                        list.addAll(paymentRepository.findAll());
                        return new ResponseEntity<>(list, HttpStatus.OK);
                    }
                }
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(apiLoc + "/{id}")
    public ResponseEntity<?> getPaymentsById(@RequestHeader(name = "Authorization") String token,
            @PathVariable("id") String id) {
        try {
            jwtTokenUtil = new JwtTokenUtil();
            if (token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
                if (userq.isPresent()) {
                    if (userq.get().getRoles().equals("admin")) {
                        Optional<Payment> data = paymentRepository.findById(id);
                        return data.map(Payment -> new ResponseEntity<>(Payment, HttpStatus.OK))
                                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
                    }
                }
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(apiLoc)
    public ResponseEntity<?> create(@RequestHeader(name = "Authorization") String token) {
        try {
            jwtTokenUtil = new JwtTokenUtil();
            if (token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
                if (userq.isPresent()) {
                    if (userq.get().getRoles().equals("admin")) {
                    }
                }
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(apiLoc + "/{id}")
    public ResponseEntity<?> updatePayments(@RequestHeader(name = "Authorization") String token,
            @PathVariable("id") String id, Payment xdata) {
        try {
            jwtTokenUtil = new JwtTokenUtil();
            if (token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
                if (userq.isPresent()) {
                    if (userq.get().getRoles().equals("admin")) {
                        Optional<Payment> p = paymentRepository.findById(id);
                        if (p.isPresent()) {
                            Payment _data = p.get();
                            _data.setPaymentMethod(xdata.getPaymentMethod());
                            _data.setPaymentRetrieve(xdata.getPaymentRetrieve());
                            _data.setPromo(xdata.getPromo());
                            // _data.addAction(null);
                            return new ResponseEntity<>(paymentRepository.save(_data), HttpStatus.OK);
                        }
                    }
                }
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(apiLoc + "/{id}")
    public ResponseEntity<Payment> delete(@RequestHeader(name = "Authorization") String token,
            @PathVariable("id") String id) {
        try {
            jwtTokenUtil = new JwtTokenUtil();
            if (token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
                if (userq.isPresent() && userq.get().getRoles().equals("admin")) {
                    paymentRepository.deleteById(id);
                    return new ResponseEntity<>(HttpStatus.OK);
                }
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
