package com.example.demo.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
// import com.example.demo.model.User;
// import java.io.Serializable;
// import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
// import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
public class Driver extends User{
    private boolean offline;
    private boolean verified;
    private boolean signupComplete;
    @JsonProperty(access = Access.WRITE_ONLY)
    private boolean takingJob;
    private EmergencyContact emergencyDetail;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String SIM;
    @JsonProperty(access = Access.WRITE_ONLY)
    private ArrayList<String> STNK;
    private String profilePicture;
    //status -1 denied, 0 not yet, 1 uploaded, 2 accepted
    private int statusSIM;
    private int statusSTNK;
    private int statusPhotoProfile;
    public int getStatusSIM() {
        return statusSIM;
    }
    public void setStatusSIM(int statusSIM) {
        this.statusSIM = statusSIM;
    }
    public int getStatusSTNK() {
        return statusSTNK;
    }
    public void setStatusSTNK(int statusSTNK) {
        this.statusSTNK = statusSTNK;
    }
    public int getStatusPhotoProfile() {
        return statusPhotoProfile;
    }
    public void setStatusPhotoProfile(int statusPhotoProfile) {
        this.statusPhotoProfile = statusPhotoProfile;
    }
    public EmergencyContact getEmergencyDetail() {
        return emergencyDetail;
    }
    public void setEmergencyDetail(EmergencyContact emergencyDetail) {
        this.emergencyDetail = emergencyDetail;
    }
    public ArrayList<String> getSTNK() {
        return STNK;
    }
    public void setSTNK(ArrayList<String> sTNK) {
        STNK = sTNK;
    }
    public void changeSTNK(String sTNK, int num){
        boolean found = false;
        for(String s: STNK){
            if(s.endsWith("-"+num)){
                s = sTNK;
                found = true;
                break;
            }
        }
        if(!found){
            STNK.add(sTNK);
        }
    }
    public void addSTNK(String sTNK) {
        STNK.add(sTNK);
    }
    public boolean isSignupComplete() {
        return signupComplete;
    }
    public void setSignupComplete(boolean signupComplete) {
        this.signupComplete = signupComplete;
    }
    public String getSIM() {
        return SIM;
    }
    public void setSIM(String sIM) {
        SIM = sIM;
    }
    public String getProfilePicture() {
        return profilePicture;
    }
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
    public boolean isOffline() {
        return offline;
    }
    public void setOffline(boolean offline) {
        this.offline = offline;
    }
    public boolean isVerified() {
        return verified;
    }
    public void setVerified(boolean verified) {
        this.verified = verified;
    }
    public Driver() {
        //no instance
    }
    public Driver(String u, String e, String p, String r){
        super(u, e, p, "driver");
        offline = false;
        verified = false;
        signupComplete = false;
        STNK = new ArrayList<String>();
        statusSIM = 0;
        statusSTNK = 0;
        statusPhotoProfile = 0;
    }
    public boolean isTakingJob() {
        return takingJob;
    }
    public void setTakingJob(boolean takingJob) {
        this.takingJob = takingJob;
    }
}
