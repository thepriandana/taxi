package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.MessageResponse;
import com.example.demo.helper.Settings;
import com.example.demo.model.Withdrawal;
import com.example.demo.model.User;
import com.example.demo.repository.TaxiRepository;
import com.example.demo.repository.WithdrawalRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class WithdrawalController {
  final String apiLoc = Settings.WithdrawalAPI;
  @Autowired
  WithdrawalRepository repository;
  @Value("${jwt.secret}")
  private String secret;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  TaxiRepository userRepo;
  @GetMapping(apiLoc)
  public ResponseEntity<?> getAll(@RequestHeader(name="Authorization") String token){
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent()){
          List<Withdrawal> list = new ArrayList<Withdrawal>();
          if(userq.get().getRoles().equals("admin")){
            list.addAll(repository.findAll());
          }else if(userq.get().getRoles().equals("driver")){
            list.addAll(repository.findWithdrawalByUserID(userq.get().getId()));
          }
          return new ResponseEntity<>(list, HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }catch (Exception e){
      return new ResponseEntity<>(new MessageResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @GetMapping(apiLoc+"/{id}")
  public ResponseEntity<Withdrawal> getByID(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent()){
          if(userq.get().getRoles().equals("admin") || userq.get().getRoles().equals("driver")){
            Optional<Withdrawal> data = repository.findById(id);
            return data.map(Withdrawal -> new ResponseEntity<>(Withdrawal, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }catch(Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @PostMapping(apiLoc)
  public ResponseEntity<?> create(@RequestHeader(name="Authorization") String token, @RequestBody Withdrawal data){
    try{//reduce balance if it requested
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent()){
          if(userq.get().getRoles().equals("driver") && data.getUserID().equals(userq.get().getId()) == false){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
          }else{
            Optional<User> userw = userRepo.findById(data.getUserID());
            if(userw.isPresent()){
              if(userw.get().getBalance()>=data.getBalance()){
                Withdrawal _data = repository.save(data);
                userw.get().addBalance(data.getBalance() *-1);
                userRepo.save(userw.get());
                return new ResponseEntity<>(_data, HttpStatus.OK);
              }
            }
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }catch (Exception e){
      return ResponseEntity.badRequest().body(new MessageResponse("Username alreay exist"));
    }
  }
  @PutMapping(apiLoc+"/{id}")
  public ResponseEntity<Withdrawal> update(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id, @RequestBody Withdrawal xdata){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          Optional<Withdrawal> data = repository.findById(id);
          if(data.isPresent()){//on canceled rollback balance on driver account
            //no 2 sended, -1 canceled, -2 denied
            if(data.get().getStatus() == 0 || data.get().getStatus() == 1){
              if(xdata.getStatus() == -1 || xdata.getStatus() == -2){
                //rollback the data
                Optional<User> userw = userRepo.findById(data.get().getUserID());
                if(userw.isPresent()){
                  userw.get().addBalance(data.get().getBalance());
                  userRepo.save(userw.get());
                }
              }
              Withdrawal _data = data.get();
              _data.setStatus(xdata.getStatus());
              return new ResponseEntity<>(repository.save(_data), HttpStatus.OK);
            }
          }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @DeleteMapping(apiLoc+"/{id}")
  public ResponseEntity<Withdrawal> delete(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          repository.deleteById(id);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
