package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.helper.Settings;
import com.example.demo.model.Notification;
import com.example.demo.model.User;
import com.example.demo.repository.TaxiRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class NotificationController {
    final String apiLoc = Settings.NotificationAPI;
    @Autowired
    TaxiRepository userRepo;
    private List<Notification>rAll(){
        try {
            List<HashMap<String, Notification>> list = userRepo.findAll().stream().map(u -> u.getNotif()).toList();
            List<Notification> vList = new ArrayList<Notification>();
            for (HashMap<String,Notification> hashMap : list) {
                 vList.addAll(hashMap.values());
            }
            return vList;
        }catch (Exception e){
            return null;
        }
    }
    @GetMapping(apiLoc)
    public ResponseEntity<List<?>> getAll(){
        try {
            return new ResponseEntity<>(rAll(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
    @GetMapping(apiLoc+"/{id}")
    public ResponseEntity<?> getByID(@PathVariable("id") String id){
        try{
            if(id.split("-").length==2)
                return new ResponseEntity<>(userRepo.findById(id.split("-")[0]).get().getNotif().get(id.split("-")[1]), HttpStatus.OK);
            else
                return new ResponseEntity<>(userRepo.findById(id).get().getNotif(), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public ResponseEntity<Notification> create(@PathVariable("uid") String uid, @RequestBody Notification data){
        try{
            Optional<User> _user = userRepo.findById(uid);
            if(_user.isEmpty())
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            else{
                _user.get().addNotification(new Notification(data.getTitle(), data.getSubtitle(), data.getHtmlString()));
                userRepo.save(_user.get());
                return new ResponseEntity<>(null, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping(apiLoc+"/{id}")
    public ResponseEntity<Notification> update(@PathVariable("id") String id, @RequestBody Notification xdata){
        Optional<User> data = userRepo.findById(id.split("-")[0]);
        if(data.isPresent()){
            Notification _data = data.get().getNotif().get(id.split("-")[1]);
            _data.setTitle(xdata.getTitle());
            _data.setSubtitle(xdata.getSubtitle());
            _data.setHtmlString(xdata.getHtmlString());
            userRepo.save(data.get());
            return new ResponseEntity<>(null, HttpStatus.OK);
        }else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @DeleteMapping(apiLoc+"/{id}")
    public ResponseEntity<Notification> delete(@PathVariable("id") String id){
        try{
            //not yet 
            Optional<User> data = userRepo.findById(id.split("-")[0]);
            if(data.isPresent()){
                userRepo.save(data.get());
                return new ResponseEntity<>(HttpStatus.OK);
            }else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
