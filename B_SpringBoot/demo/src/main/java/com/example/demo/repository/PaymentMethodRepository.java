package com.example.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.PaymentMethod;
public interface PaymentMethodRepository extends MongoRepository<PaymentMethod, String>{
}
