package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
@Document(collection = "Withdrawal")
public class Withdrawal{
    @Id
    private String id;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String userID;
    private Float balance;
    private LocalDateTime created;
    private int status; //0 request, 1 accepted, 2 sended, -1 canceled, -2 denied
    public Withdrawal(String userID, Float balance) {
        this.userID = userID;
        this.created = LocalDateTime.now();
        this.balance = balance;
        status = 0;
    }
    public String getId() {
        return id;
    }
    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID) {
        this.userID = userID;
    }
    public LocalDateTime getCreated() {
        return created;
    }
    public Float getBalance() {
        return balance;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

}
