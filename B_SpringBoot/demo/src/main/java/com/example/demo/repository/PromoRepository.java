package com.example.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Promo;

public interface PromoRepository extends MongoRepository<Promo, String>{
    
}
