package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Payment;

public interface PaymentRepository extends MongoRepository<Payment, String>{
    Optional<Payment> findByUserIDOrderByIdDesc(@Param("userID") String customer);
    List<Payment> findByStatus(int status);
}
