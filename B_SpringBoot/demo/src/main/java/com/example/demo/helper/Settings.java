package com.example.demo.helper;

public class Settings {
  public static final String CrossOrigin = "http://localhost:4200"; //angular http://localhost:4200
  public static final String ApiMap = "/api";
  public static final String UserAPI= "/user";
  public static final String DriverAPI= "/driver";
  public static final String CarAPI = "/car";
  public static final String NotificationAPI = "/notif";
  public static final String PaymentAPI = "/payment";
  public static final String PaymentMAPI = "/paymentM";
  public static final String PromoAPI = "/promo";
  public static final String RideCAPI = "/rideclass";
  public static final String BookingAPI = "/booking";
  public static final String WithdrawalAPI = "/withdrawal";
}
