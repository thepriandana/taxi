package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.Settings;
import com.example.demo.model.RideClass;
import com.example.demo.model.User;
import com.example.demo.repository.RideClassRepository;
import com.example.demo.repository.TaxiRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class RideClassController {
  final String apiLoc = Settings.RideCAPI;
  @Autowired
  RideClassRepository repository;
  @Value("${jwt.secret}")
  private String secret;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  TaxiRepository userRepo;
  @GetMapping(apiLoc)
  public ResponseEntity<List<RideClass>> getAll(){
    try {
      List<RideClass> list = new ArrayList<RideClass>();
      list.addAll(repository.findAll());
      return new ResponseEntity<>(list, HttpStatus.OK);
    }catch (Exception e){
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @GetMapping(apiLoc+"/{id}")
  public ResponseEntity<RideClass> getByID(@PathVariable("id") String id){
    Optional<RideClass> data = repository.findById(id);
    return data.map(RideClass -> new ResponseEntity<>(RideClass, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }
  @PostMapping(apiLoc)
  public ResponseEntity<RideClass> create(@RequestHeader(name="Authorization") String token, @RequestBody RideClass data){
    try{jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){

          RideClass _data = repository.save(new RideClass(data.getClassName(), data.getPrice(), data.getDriverIncome(), data.getPax()));
          return new ResponseEntity<>(_data, HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }catch (Exception e){
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @PutMapping(apiLoc+"/{id}")
  public ResponseEntity<RideClass> update(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id, @RequestBody RideClass xdata){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          Optional<RideClass> data = repository.findById(id);
          if(data.isPresent()){
            RideClass _data = data.get();
            _data.setDriverIncome(xdata.getDriverIncome());
            _data.setClassName(xdata.getClassName());
            _data.setPrice(xdata.getPrice());
            _data.setPax(xdata.getPax());
            return new ResponseEntity<>(repository.save(_data), HttpStatus.OK);
          }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @DeleteMapping(apiLoc+"/{id}")
  public ResponseEntity<RideClass> delete(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          repository.deleteById(id);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
