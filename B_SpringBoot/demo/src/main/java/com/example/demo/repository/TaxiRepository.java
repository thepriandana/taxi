package com.example.demo.repository;
import java.util.List;
// import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.example.demo.model.User;


public interface TaxiRepository extends MongoRepository<User, String>{
    Optional<User> findByEmailIgnoringCase(String email);
    Optional<User> findByEmailAndPassword(String email, String password);
    List<User> findByRoles(String roles);
}
