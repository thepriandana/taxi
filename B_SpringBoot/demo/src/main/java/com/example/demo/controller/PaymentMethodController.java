package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.MessageResponse;
import com.example.demo.helper.Settings;
import com.example.demo.model.PaymentMethod;
import com.example.demo.model.User;
import com.example.demo.repository.PaymentMethodRepository;
import com.example.demo.repository.TaxiRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class PaymentMethodController {
    final String apiLoc = Settings.PaymentMAPI;
    @Autowired
    PaymentMethodRepository repository;
    @Value("${jwt.secret}")
    private String secret;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    TaxiRepository userRepo;
    @GetMapping(apiLoc)
    public ResponseEntity<?> getAll(){
        try {
            List<PaymentMethod> list = new ArrayList<PaymentMethod>();
            list.addAll(repository.findAll());
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new MessageResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping(apiLoc+"/{id}")
    public ResponseEntity<PaymentMethod> getByID(@PathVariable("id") String id){
        Optional<PaymentMethod> data = repository.findById(id);
        return data.map(PaymentMethod -> new ResponseEntity<>(PaymentMethod, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @PostMapping(apiLoc)
    public ResponseEntity<?> create(@RequestHeader(name="Authorization") String token, @RequestBody String data){
        try{
          jwtTokenUtil = new JwtTokenUtil();
          if (token.startsWith("Bearer ")) {
            String jwtToken = token.substring(7);
            Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
            if(userq.isPresent() && userq.get().getRoles().equals("admin")){
              PaymentMethod _data = repository.save(new PaymentMethod(data));
              return new ResponseEntity<>(_data, HttpStatus.OK);
            }
          }
          return new ResponseEntity<>(new MessageResponse("Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(new MessageResponse("Username alreay exist"));
        }
    }
    @PutMapping(apiLoc+"/{id}")
    public ResponseEntity<PaymentMethod> update(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id, @RequestBody PaymentMethod xdata){
      try{
          jwtTokenUtil = new JwtTokenUtil();
          if (token.startsWith("Bearer ")) {
            String jwtToken = token.substring(7);
            Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
            if(userq.isPresent() && userq.get().getRoles().equals("admin")){
              Optional<PaymentMethod> data = repository.findById(id);
              if(data.isPresent()){
                  PaymentMethod _data = data.get();
                  _data.setName(xdata.getName());
                  return new ResponseEntity<>(repository.save(_data), HttpStatus.OK);
              }
            }
          }
          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
          return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping(apiLoc+"/{id}")
    public ResponseEntity<PaymentMethod> delete(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
        try{
          jwtTokenUtil = new JwtTokenUtil();
          if (token.startsWith("Bearer ")) {
            String jwtToken = token.substring(7);
            Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
            if(userq.isPresent() && userq.get().getRoles().equals("admin")){
              repository.deleteById(id);
              return new ResponseEntity<>(HttpStatus.OK);
            }
          }
          return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
