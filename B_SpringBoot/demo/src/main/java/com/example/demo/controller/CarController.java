package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.helper.Settings;
import com.example.demo.helper.FilesStorageService;
import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.MessageResponse;
import com.example.demo.model.Car;
import com.example.demo.model.User;
import com.example.demo.model.Parsing.CarAround;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.TaxiRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class CarController {
  final String apiLoc = Settings.CarAPI;
  @Autowired
  CarRepository repository;
  @Value("${jwt.secret}")
  private String secret;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  TaxiRepository userRepo;
  @Autowired
  FilesStorageService storageService;

  @GetMapping(apiLoc)
  public ResponseEntity<?> getAll(@RequestParam(required = false) String plate,
  @RequestParam(required = false) Double lat,
  @RequestParam(required = false) Double lng,
  @RequestParam(required = false) Double radius){
    try {
      List<CarAround> list = new ArrayList<CarAround>();
      if(lat != null && lng != null){
        //radius in km
        radius = (radius == null) ? 0.9 : radius;
        Double lat1=lat - (radius/111.12), lat2=lat + (radius/111.12),
        lng1=lng - ((radius*2)/111.2)*(Math.cos(lng)), lng2=lng + ((radius*2)/111.2)*(Math.cos(lng));
        // list.addAll(repository.findCarByLatAndLng(lat1, lng1, lat2, lng2));
        Random r = new Random();
        for(int i=0; i<5; i++){
          list.add(new CarAround(
          (lat>0) ? r.nextDouble(lat1,lat2) : (r.nextDouble(lat2*-1,lat1*-1) *-1),
          (lng>0) ? r.nextDouble(lng2,lng1) : (r.nextDouble(lng1*-1,lng2*-1) *-1),
          r.nextDouble(360), 5+r.nextInt(10)));
        }
        //(r.nextDouble(lng1*-1,lng2*-1) *-1)
        //(r.nextDouble(lat1*-1,lat2*-1) *-1)
      } else if(plate != null){
        list.add(new CarAround(0.0, 0.0, 0, 0));
        // list.add(repository.findByPlate(plate));}
      }else if(plate == null || plate.equals("")){
        for(int i=0; i<5; i++){
          list.add(new CarAround(0.0, 0.0, 0, 0));
        }
        // list.addAll(repository.findAll());
      }
      return new ResponseEntity<>(list, HttpStatus.OK);
    }catch (Exception e){
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(apiLoc+"/all")
  public ResponseEntity<?> getAllAdmin(@RequestHeader (name="Authorization") String token){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          List<Car> list = new ArrayList<Car>();
          list.addAll(repository.findAll());
          return new ResponseEntity<>(list, HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }catch (Exception e){
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @GetMapping(apiLoc+"/{id}")
  public ResponseEntity<Car> getByID(@PathVariable("id") String id){
    Optional<Car> data = repository.findById(id);
    return data.map(Car -> new ResponseEntity<>(Car, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }
  @PostMapping(apiLoc)
  public ResponseEntity<Car> create(@RequestHeader (name="Authorization") String token, @RequestParam(required = false) String id, @RequestBody Car data){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("driver") || userq.isPresent() && userq.get().getRoles().equals("admin")){
          Car _data = repository.save(new Car((userq.get().getRoles().equals("admin")) ? id: userq.get().getId(), data.getPlate(), data.getRideClass()));
          return new ResponseEntity<>(_data, HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }catch (Exception e){
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @PutMapping(apiLoc+"/{id}/verification")
  public ResponseEntity<?> verification(@RequestHeader (name="Authorization") String token, @PathVariable("id") String id, @RequestBody boolean verif){
    jwtTokenUtil = new JwtTokenUtil();
    if (token.startsWith("Bearer ")) {
      String jwtToken = token.substring(7);
      Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
      if(userq.isPresent() && userq.get().getRoles().equals("admin")){
        Optional<Car> data = repository.findById(id);
        if(data.isPresent()){
          Car _data = data.get();
          _data.setVerified(verif);
          return new ResponseEntity<>(repository.save(_data), HttpStatus.OK);
        }
      }
    }
    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
  }
  @PutMapping(apiLoc+"/{id}")
  public ResponseEntity<Car> update(@RequestHeader (name="Authorization") String token, @PathVariable("id") String id, @RequestBody Car xdata){
    jwtTokenUtil = new JwtTokenUtil();
    if (token.startsWith("Bearer ")) {
      String jwtToken = token.substring(7);
      Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
      if(userq.isPresent()){
        Optional<Car> data = repository.findById(id);
        if(data.isPresent()){
          Car _data = data.get();
          if(userq.get().getRoles().equals("admin")){
            if(xdata.getPlate() != null) _data.setPlate(xdata.getPlate());
            if(xdata.getRideClass() != null) _data.setRideClass(xdata.getRideClass());
          }
          _data.setLat(xdata.getLat());
          _data.setLng(xdata.getLng());
          _data.setRotationDeg(xdata.getRotationDeg());
          return new ResponseEntity<>(repository.save(_data), HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }else{
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
  @DeleteMapping(apiLoc+"/{id}")
  public ResponseEntity<Car> delete(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          repository.deleteById(id);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @GetMapping(apiLoc+"/files/{filename:.+}")
  @ResponseBody
  public ResponseEntity<?> getCarImage(@RequestHeader (name="Authorization") String token, @PathVariable String filename) {
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent()){
          if(userq.get().getRoles().equals("driver")){
            Optional<Car> c = repository.findBydriver(userq.get().getId());
            if(c.isPresent()){
              if(!c.get().getPhoto().isEmpty()){
                if(c.get().getPhoto().contains(filename)){
                  Resource file = storageService.load(filename);
                  return ResponseEntity.ok()
                  .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
                }
              }
            }
          }else if(userq.get().getRoles().equals("admin")){
            Resource file = storageService.load(filename);
            return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
          }
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }catch(Exception e){
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }
}
