package com.example.demo.model.Conveter;

import org.springframework.data.annotation.Id;

public class DashboardDate {
    @Id
    private String id;
    private int month;
    private int year;
    private int count;
    private int dayOfMonth;
    
    public DashboardDate(String id, int month, int year, int count, int dayOfMonth) {
        this.id = id;
        this.month = month;
        this.year = year;
        this.count = count;
        this.dayOfMonth = dayOfMonth;
    }
    
    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
