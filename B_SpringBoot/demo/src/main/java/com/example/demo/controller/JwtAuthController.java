package com.example.demo.controller;

// import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.helper.JwtTokenUtil;
// import com.example.demo.helper.MessageResponse;
import com.example.demo.helper.MessageResponse;
import com.example.demo.model.User;
import com.example.demo.model.Driver;
import com.example.demo.model.PaymentMethod;
import com.example.demo.model.RideClass;
import com.example.demo.repository.TaxiRepository;
import com.example.demo.repository.PaymentMethodRepository;
import com.example.demo.repository.RideClassRepository;
import com.example.demo.helper.Settings;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true") //angular
@RestController
@RequestMapping(Settings.ApiMap)
public class JwtAuthController {
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;

	@Value("${jwt.secret}")
	private String secret;


	@Autowired
	PaymentMethodRepository PaymentMethodRepository;
	@Autowired
	RideClassRepository RideClassRepository;

	@Autowired
	TaxiRepository repo;
	//create post mapping to check wheter jwt token is still valid or not api?/splash
	@GetMapping("/splash")
	public ResponseEntity<?> splash(@RequestHeader (name="Authorization") String token){
		try {
			jwtTokenUtil = new JwtTokenUtil();
			if (token.startsWith("Bearer ")) {
				String jwtToken = token.substring(7);
				if(jwtTokenUtil.isExpiredToken(secret, jwtToken) == false) return new ResponseEntity<>(new MessageResponse("valid"), HttpStatus.OK);
			}
			return new ResponseEntity<>(new MessageResponse("expired"), HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/web/signin", method = RequestMethod.POST)
	public ResponseEntity<?> webSignIn(@RequestBody User authenticationRequest) throws Exception {
		Optional<User> u = repo.findByEmailIgnoringCase(authenticationRequest.getEmail());
		if(!u.isPresent()){
			return new ResponseEntity<>(new MessageResponse("User is not registered!"), HttpStatus.BAD_REQUEST);
		}else{
			BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
			if(enc.matches(authenticationRequest.getPassword(), u.get().getPassword())){
				authenticate(u.get().getEmail(), authenticationRequest.getPassword());
				final UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(u.get().getEmail());
				jwtTokenUtil = new JwtTokenUtil();
				String token = jwtTokenUtil.generateToken(userDetails, secret, u.get().getId());

				User a =  u.get();
				HashMap<String, Object> b = new HashMap<String, Object>();
				HashMap<String, Object> profile = new HashMap<String, Object>();
				profile.put("roles", a.getRoles());
				profile.put("name", a.getName());
				profile.put("email", a.getEmail());
				b.put("profile", profile);
				b.put("token", token);
				return ResponseEntity.ok(new MessageResponse(b));
			}else{
				return new ResponseEntity<>(new MessageResponse("Email or password is wrong!"), HttpStatus.BAD_REQUEST);
			}
		}
	}
	@RequestMapping(value = "/user/signin", method = RequestMethod.POST)
	public ResponseEntity<?> userSignIn(@RequestBody User authenticationRequest) throws Exception {
		Optional<User> u = repo.findByEmailIgnoringCase(authenticationRequest.getEmail());
		if(!u.isPresent()){
			return new ResponseEntity<>(new MessageResponse("User is not registered!"), HttpStatus.BAD_REQUEST);
		}else{
			BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
			if(enc.matches(authenticationRequest.getPassword(), u.get().getPassword())){
				authenticate(u.get().getEmail(), authenticationRequest.getPassword());
				final UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(u.get().getEmail());
				jwtTokenUtil = new JwtTokenUtil();
				String token = jwtTokenUtil.generateToken(userDetails, secret, u.get().getId());
				return ResponseEntity.ok(new MessageResponse(token));
			}else{
				return new ResponseEntity<>(new MessageResponse("Email or password is wrong!"), HttpStatus.BAD_REQUEST);
			}
		}
	}

	@RequestMapping(value = "/driver/signin", method = RequestMethod.POST)
	public ResponseEntity<?> driverSignIn(@RequestBody User authenticationRequest) throws Exception {
		Optional<User> u = repo.findByEmailIgnoringCase(authenticationRequest.getEmail());
		if(!u.isPresent()){
			return new ResponseEntity<>(new MessageResponse("Driver is not registered!"), HttpStatus.BAD_REQUEST);
		}else{
			BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
			if(enc.matches(authenticationRequest.getPassword(), u.get().getPassword())){
				authenticate(u.get().getEmail(), authenticationRequest.getPassword());
				final UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(u.get().getEmail());
				jwtTokenUtil = new JwtTokenUtil();
				String token = jwtTokenUtil.generateToken(userDetails, secret, u.get().getId());
				Driver a = (Driver) u.get();
				HashMap<String, Object> b = new HashMap<String, Object>();
				HashMap<String, Object> profile = new HashMap<String, Object>();
				profile.put("name", a.getName());
				profile.put("email", a.getEmail());
				// profile.put("phone", )
				b.put("profile", profile);
				b.put("token", token);
				b.put("profileComplete", a.isSignupComplete());
				b.put("offline", a.isOffline());
				b.put("verified", a.isVerified());
				b.put("takingJob", a.isTakingJob());
				return ResponseEntity.ok(new MessageResponse(b));
			}else{
				return new ResponseEntity<>(new MessageResponse("Email or password is wrong!"), HttpStatus.BAD_REQUEST);
			}
		}
	}
	@RequestMapping(value="init")
	public ResponseEntity<?> initTaxiDB() throws Exception{
		//init the payment method, rideclass
		RideClassRepository.save(new RideClass("Premium", "12000", "10000", 4));
		PaymentMethodRepository.save(new PaymentMethod("Cash"));
		return new ResponseEntity<>(new MessageResponse("Init Success"), HttpStatus.OK);
	}
	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			// System.out.println("auth process exit");
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
