package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.nio.charset.StandardCharsets;
import com.example.demo.helper.MessageResponse;
import com.example.demo.helper.Settings;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.TaxiRepository;
import com.example.demo.repository.WithdrawalRepository;
import com.example.demo.model.Car;
import com.example.demo.model.Driver;
import com.example.demo.model.EmergencyContact;
import com.example.demo.model.User;
import com.example.demo.model.Withdrawal;
import com.example.demo.model.Parsing.CustomDriver;
import com.example.demo.helper.FilesStorageService;
import org.springframework.web.multipart.MultipartFile;
import com.example.demo.helper.JwtTokenUtil;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials = "true")
@RestController
@RequestMapping(Settings.ApiMap)
public class DriverController {
  @Value("${jwt.secret}")
  private String secret;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  TaxiRepository repo;
  @Autowired
  CarRepository carRepository;
  @Autowired
  FilesStorageService storageService;
  @Autowired
  WithdrawalRepository withdrawalRepository;

  @GetMapping(Settings.DriverAPI)
  public ResponseEntity<?> getAllDriver(@RequestHeader(name = "Authorization") String token,
      @RequestParam(required = false) String username) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("admin")) {
          if (username != null && !username.equals("")) {
            return new ResponseEntity<>(repo.findByEmailIgnoringCase(username), HttpStatus.OK);
          } else {
            List<CustomDriver> data = new ArrayList<CustomDriver>();
            List<User> list = repo.findByRoles("driver");
            data.addAll(list.stream().map(u -> new CustomDriver((Driver) u)).toList());
            return new ResponseEntity<>(data, HttpStatus.OK);
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
@GetMapping(Settings.DriverAPI+"/{id}")
  public ResponseEntity<?> getDriverByID(@PathVariable("id") String id, @RequestHeader(name = "Authorization") String token,
      @RequestParam(required = false) String username) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("admin")) {
            Optional<User> userx = repo.findById(id);
            if(userx.isPresent()){
              CustomDriver tmp = new CustomDriver((Driver) userx.get());
              List<Withdrawal> withdrawal = withdrawalRepository.findWithdrawalByUserID(userx.get().getId());
              tmp.setWithdrawals(withdrawal);
              return new ResponseEntity<>(tmp, HttpStatus.OK);
            }            
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(Settings.DriverAPI + "/refresh")
  public ResponseEntity<?> refreshDriverData(@RequestHeader(name = "Authorization") String token) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver a = (Driver) userq.get();
          HashMap<String, Object> b = new HashMap<String, Object>();
          HashMap<String, Object> profile = new HashMap<String, Object>();
          profile.put("name", a.getName());
          profile.put("email", a.getEmail());
          // profile.put("phone", )
          b.put("takingJob", a.isTakingJob());
          b.put("profile", profile);
          b.put("token", jwtToken);
          b.put("profileComplete", a.isSignupComplete());
          b.put("offline", a.isOffline());
          b.put("verified", a.isVerified());
          return ResponseEntity.ok(new MessageResponse(b));
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/acceptAndsubmit")
  public ResponseEntity<?> submitAndAcceptAgreement(@RequestHeader(name = "Authorization") String token,
      @RequestBody boolean accepted) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          Optional<Car> car = carRepository.findBydriver(driver.getId());
          if (driver.getStatusSIM() == 0 || driver.getStatusSIM() == -1) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new MessageResponse("Please upload valid SIM"));
          } else if (driver.getStatusSTNK() == 0 || driver.getStatusSTNK() == -1) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new MessageResponse("Please upload valid STNK"));
          } else if (driver.getStatusPhotoProfile() == 0 || driver.getStatusPhotoProfile() == -1) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new MessageResponse("Please upload valid photo profile"));
          } else if (car.isPresent() == false) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new MessageResponse("Please upload car photo first"));
          } else if (car.isPresent() && car.get().getPhoto().size() == 0) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new MessageResponse("Please upload car photo first"));
          } else if (driver.isSignupComplete() == false && accepted == true) {
            driver.setSignupComplete(true);
            repo.save(driver);
            return ResponseEntity.ok(new MessageResponse("Submitted"));
          } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
          }
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/signup")
  public ResponseEntity<?> createDriver(@RequestBody User data) {
    try {
      if (!repo.findByEmailIgnoringCase(data.getEmail()).isPresent()) {
        BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
        // trim space
        repo.save(new Driver(data.getName(), data.getEmail().toLowerCase(), enc.encode(data.getPassword()), "driver"));
        return ResponseEntity.ok(new MessageResponse("Success"));
      } else {
        return ResponseEntity.badRequest().body(new MessageResponse("Username alreay exist"));
      }
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @RequestMapping(Settings.DriverAPI + "/signupStatus")
  public ResponseEntity<?> uploadPhotoProfile(@RequestHeader(name = "Authorization") String token) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          HashMap<String, Object> information = new HashMap<String, Object>();
          ArrayList<String> carImage = new ArrayList<String>();
          Optional<Car> car = carRepository.findBydriver(driver.getId());
          int statusCar = 0;
          if (car.isPresent() && car.get().getPhoto().size() > 0) {
            carImage.addAll(car.get().getPhoto());
            statusCar = 1;
          }
          if (car.isPresent() && car.get().isVerified()) {
            statusCar = 2;
          }
          information.put("complete", driver.isSignupComplete());
          information.put("emergencyContact", driver.getEmergencyDetail());
          information.put("photoProfile", driver.getProfilePicture());
          information.put("STNK", driver.getSTNK());
          information.put("SIM", driver.getSIM());
          information.put("CAR", carImage);
          information.put("statusCAR", statusCar);
          information.put("statusPP", driver.getStatusPhotoProfile());
          information.put("statusSIM", driver.getStatusSIM());
          information.put("statusSTNK", driver.getStatusSTNK());
          return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(information));
        }
      }
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/photoProfile")
  public ResponseEntity<?> uploadPhotoProfile(@RequestHeader(name = "Authorization") String token,
      @RequestParam("file") MultipartFile file) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          if (driver.getStatusPhotoProfile() != 2) {
            uploadImageFile(file, "profile-" + driver.getId() + ".png");
            driver.setProfilePicture("profile-" + driver.getId() + ".png");
            driver.setStatusPhotoProfile(1);
            repo.save(driver);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Upload sucessfully"));
          }
        }
      }
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/SIM")
  public ResponseEntity<?> uploadSIM(@RequestHeader(name = "Authorization") String token,
      @RequestParam("file") MultipartFile file) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          if (driver.getStatusSIM() != 2) {
            uploadImageFile(file, "sim-" + driver.getId() + ".png");
            driver.setSIM("sim-" + driver.getId() + ".png");
            driver.setStatusSIM(1);
            repo.save(driver);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Upload sucessfully"));
          }
        }
      }
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/STNK")
  public ResponseEntity<?> uploadSTNK(@RequestHeader(name = "Authorization") String token,
      @RequestParam("file") ArrayList<MultipartFile> files) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          if (driver.getStatusSTNK() != 2) {
            for (MultipartFile file : files) {
              if (file.getOriginalFilename().endsWith("-1")) {
                uploadImageFile(file, "stnk-" + driver.getId() + "-" + 1 + ".png");
                driver.changeSTNK("stnk-" + driver.getId() + "-" + 1 + ".png", 1);
              } else if (file.getOriginalFilename().endsWith("-2")) {
                uploadImageFile(file, "stnk-" + driver.getId() + "-" + 2 + ".png");
                driver.changeSTNK("stnk-" + driver.getId() + "-" + 2 + ".png", 2);
              }
            }
            driver.setStatusSTNK(1);
            repo.save(driver);
            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Upload sucessfully"));
          }
        }
      }
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Errorx"));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error" + e));
    }
  }

  @PostMapping(Settings.DriverAPI + "/Car")
  public ResponseEntity<?> uploadCarPict(@RequestHeader(name = "Authorization") String token,
      @RequestParam("file") ArrayList<MultipartFile> files) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          Optional<Car> car = carRepository.findBydriver(driver.getId());
          int num = 0;
          if (car.isPresent()) {
            num = car.get().getPhoto().size();
            for (MultipartFile file : files) {
              uploadImageFile(file, "car-" + driver.getId() + "-" + num + ".png");
              car.get().addPhoto("car-" + driver.getId() + "-" + num + ".png");
              num++;
            }
            carRepository.save(car.get());
          } else {
            carRepository.save(new Car(driver.getId(), "", ""));
            car = carRepository.findBydriver(driver.getId());
            for (MultipartFile file : files) {
              uploadImageFile(file, "car-" + driver.getId() + "-" + num + ".png");
              car.get().addPhoto("car-" + driver.getId() + "-" + num + ".png");
              num++;
            }
            carRepository.save(car.get());
          }
          return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Upload sucessfully"));
        }
      }
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse("Error"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/emergencyProfile")
  public ResponseEntity<MessageResponse> emergencyProfile(@RequestHeader(name = "Authorization") String token,
      @RequestBody EmergencyContact emergencyContact) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          driver.setEmergencyDetail(emergencyContact);
          repo.save(driver);
          return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Emergency contact updated"));
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/enabled")
  public ResponseEntity<MessageResponse> setenabled(@RequestHeader(name = "Authorization") String token,
      @RequestBody boolean set) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Driver driver = (Driver) userq.get();
          driver.setOffline(set);
          repo.save(driver);
          return ResponseEntity.ok(new MessageResponse(set));
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @GetMapping(Settings.DriverAPI + "/withdrawal")
  public ResponseEntity<MessageResponse> getWithdrawal(@RequestHeader(name = "Authorization") String token) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          List<Withdrawal> withdrawal = withdrawalRepository.findWithdrawalByUserID(userq.get().getId());
          return ResponseEntity.ok(new MessageResponse(withdrawal));
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @GetMapping(Settings.DriverAPI + "/withdrawalCancel")
  public ResponseEntity<MessageResponse> cancelWithdrawal(@RequestHeader(name = "Authorization") String token,
      @RequestBody String id) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          Optional<Withdrawal> w = withdrawalRepository.findById(id);
          if (w.isPresent()) {
            w.get().setStatus(-2);
            withdrawalRepository.save(w.get());
            return ResponseEntity.ok(new MessageResponse("Canceled"));
          }
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  @PostMapping(Settings.DriverAPI + "/withdrawal")
  public ResponseEntity<MessageResponse> makeWithdrawal(@RequestHeader(name = "Authorization") String token,
      @RequestBody String balance) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent() && userq.get().getRoles().equals("driver")) {
          withdrawalRepository.save(new Withdrawal(userq.get().getId(), Float.valueOf(balance)));
          return ResponseEntity.ok(new MessageResponse("Success Requesting"));
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }

  private void uploadImageFile(MultipartFile file, String filename) {
    // Check if the bytes contain backdoor code or malicious code
    try {
      byte[] imageBytes = file.getBytes();
      // Check for malicious code
      String imageString = new String(imageBytes, StandardCharsets.UTF_8);
      if (!file.getContentType().startsWith("image")) {
        System.out.println("Image file type is invalid");
      } else if (imageString.contains("<?php") || imageString.contains("<java>") || imageString.contains("class.module")
          || imageString.contains("java.")) {
        System.out.println("Image contain malicious code");
      } else {
        storageService.saveAs(file, filename);
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }
  /*
   * 
   * byte[] bytes=multipart.getBytes();
   * BufferedOutputStream stream= new BufferedOutputStream(new
   * FileOutputStream("src/main/resources/static/image/book/"+fileName));;
   * 
   * stream.write(bytes);
   * stream.close();
   */

  // @GetMapping("/files")
  // public ResponseEntity<List<FileInfo>> getListFiles() {
  // List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
  // String filename = path.getFileName().toString();
  // String url = MvcUriComponentsBuilder
  // .fromMethodName(FilesController.class, "getFile",
  // path.getFileName().toString()).build().toString();

  // return new FileInfo(filename, url);
  // }).collect(Collectors.toList());

  // return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
  // }

  @GetMapping(Settings.DriverAPI + "/files/{filename:.+}")
  @ResponseBody
  public ResponseEntity<?> getFile(@RequestHeader(name = "Authorization") String token, @PathVariable String filename) {
    try {
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if (userq.isPresent()) {
          if(userq.get().getRoles().equals("driver")){
            Driver driver = (Driver) userq.get();
            ArrayList<String> imagus = new ArrayList<>();
            imagus.add(driver.getSIM());
            imagus.addAll(driver.getSTNK());
            imagus.add(driver.getProfilePicture());
            for (String im : imagus) {
              if (im.equals(filename)) {
                Resource file = storageService.load(filename);
                return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
              }
            }
          }else if(userq.get().getRoles().equals("admin")){
            Resource file = storageService.load(filename);
            return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
          }
        }
      }
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }
}
