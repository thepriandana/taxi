package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
@Document(collection = "Notification")
public class Notification implements Serializable{
    @Id
    private String id;
    private String title;
    private String subtitle;
    private String htmlString;
    public Notification(String title, String subtitle, String htmlString) {
        this.title = title;
        this.subtitle = subtitle;
        this.htmlString = htmlString;
    }
    public String getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getSubtitle() {
        return subtitle;
    }
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
    public String getHtmlString(){
        return htmlString;
    }
    public void setHtmlString(String htmlString){
        this.htmlString = htmlString;
    }
}
