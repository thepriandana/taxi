 package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
// import com.fasterxml.jackson.annotation.JsonProperty.Access;
@Document(collection = "Booking")
public class Booking implements Serializable{
    @Id
    private String id;
    private String customer;
    private String driver;
    // private PaymentMethod paymentMethod;
    @JsonProperty(value="payment")
    private String payment;
    private int status; //0=request, 1 active, 2 cancel, 3 finish,
    /*
      91 = otw, driver

    */

    @JsonProperty("startLoc")
    private LatLng startLoc;
    private String startLocName;
    private LocalDateTime startTime;

    @JsonProperty("endLoc")
    private LatLng endLoc;
    private String endLocName;
    private LocalDateTime endTime;

    private String note;
    private int rating;

    @JsonProperty("driverLoc")
    private LatLng driverLoc;

    private String rideClass;
    private double totalKM;
    private ArrayList<String> actions;

    public Booking(String payment, LatLng startLoc, String startLocName,
    LatLng endLoc, String endLocName, String note) {
        this.payment = payment;
        this.startLoc = startLoc;
        this.startLocName = startLocName;
        this.startTime = LocalDateTime.now();
        this.endLoc = endLoc;
        this.endLocName = endLocName;
        this.note = note;
        this.actions = new ArrayList<>();
    }
    public void setID(String id){
        this.id = id;
    }
    public String getId() {
        return id;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public LatLng getStartLoc() {
        return startLoc;
    }
    public void setStartLoc(LatLng startLoc) {
        this.startLoc = startLoc;
    }
    public String getStartLocName() {
        return startLocName;
    }
    public void setStartLocName(String startLocName) {
        this.startLocName = startLocName;
    }
    public LocalDateTime getStartTime() {
        return startTime;
    }
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
    public LatLng getEndLoc() {
        return endLoc;
    }
    public void setEndLoc(LatLng endLoc) {
        this.endLoc = endLoc;
    }
    public LocalDateTime getEndTime() {
        return endTime;
    }
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public String getEndLocName() {
        return endLocName;
    }
    public void setEndLocName(String endLocName) {
        this.endLocName = endLocName;
    }
    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
    public LatLng getDriverLoc() {
        return driverLoc;
    }
    public void setDriverLoc(LatLng driverLoc) {
        this.driverLoc = driverLoc;
    }
    public double getTotalKM() {
        return totalKM;
    }
    public void setTotalKM(double totalKM) {
        this.totalKM = totalKM;
    }
    public ArrayList<String> getAction() {
        return actions;
    }
    public void addAction(String actionString){
        this.actions.add(actionString);
    }
    public void setAction(ArrayList<String> actions) {
        this.actions = actions;
    }
    public String getcustomer() {
        return customer;
    }
    public void setcustomer(String customer) {
        this.customer = customer;
    }
    public String getdriver() {
        return driver;
    }
    public void setdriver(String driver) {
        this.driver = driver;
    }
    public String getPayment() {
        return payment;
    }
    public void setPayment(String payment) {
        this.payment = payment;
    }
    public String getRideClass() {
        return rideClass;
    }
    public void setRideClass(String rideClass) {
        this.rideClass = rideClass;
    }

}
