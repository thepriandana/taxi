package com.example.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Notification;

public interface NotificationRepository extends MongoRepository<Notification, String>{
    
}
