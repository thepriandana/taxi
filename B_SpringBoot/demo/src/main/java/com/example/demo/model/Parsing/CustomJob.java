package com.example.demo.model.Parsing;

import java.time.LocalDateTime;

import com.example.demo.model.Booking;
import com.example.demo.model.LatLng;

public class CustomJob{
    private String id;
    private LatLng startLoc;
    private String startLocName;
    private LocalDateTime startTime;
    
    private LatLng endLoc;
    private String endLocName;
    private LocalDateTime endTime;
    public CustomJob(Booking b){
        this.id = b.getId();
        this.startLoc = b.getStartLoc();
        this.startLocName = b.getStartLocName();
        this.startTime = b.getStartTime();

        this.endLoc = b.getEndLoc();
        this.endLocName = b.getEndLocName();
        this.endTime = b.getEndTime();
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public LatLng getStartLoc() {
        return startLoc;
    }
    public void setStartLoc(LatLng startLoc) {
        this.startLoc = startLoc;
    }
    public String getStartLocName() {
        return startLocName;
    }
    public void setStartLocName(String startLocName) {
        this.startLocName = startLocName;
    }
    public LocalDateTime getStartTime() {
        return startTime;
    }
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
    public LatLng getEndLoc() {
        return endLoc;
    }
    public void setEndLoc(LatLng endLoc) {
        this.endLoc = endLoc;
    }
    public String getEndLocName() {
        return endLocName;
    }
    public void setEndLocName(String endLocName) {
        this.endLocName = endLocName;
    }
    public LocalDateTime getEndTime() {
        return endTime;
    }
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
