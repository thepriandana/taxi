package com.example.demo.model.Parsing;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.model.Booking;
import com.example.demo.model.Driver;
import com.example.demo.model.EmergencyContact;
import com.example.demo.model.ImageVerification;
import com.example.demo.model.Withdrawal;
public class CustomDriver {
  private String id;
  private String name;
  private boolean offline;
  private boolean verified;
  private boolean takingJob;
  private EmergencyContact emergencyDetail;
  private String SIM;
  private ArrayList<String> STNK;
  private String profile;
  private Float balance;
  //status -1 denied, 0 not yet, 1 uploaded, 2 accepted
  // private ArrayList<ImageVerification> imageVerifications;
  private List<Withdrawal> withdrawals;
  
    //status -1 denied, 0 not yet, 1 uploaded, 2 accepted
    private int statusSIM;
    private int statusSTNK;
    private int statusPhotoProfile;
  public int getStatusSIM() {
      return statusSIM;
    }
    public int getStatusSTNK() {
      return statusSTNK;
    }
    public int getStatusPhotoProfile() {
      return statusPhotoProfile;
    }
  public Float getBalance() {
    return balance;
  }
  public void setBalance(Float balance) {
    this.balance = balance;
  }
  public CustomDriver(Driver d){
    this.id = d.getId();
    this.offline = d.isOffline();
    this.verified = d.isVerified();
    this.takingJob = d.isTakingJob();
    this.emergencyDetail = d.getEmergencyDetail();
    this.SIM = d.getSIM();
    this.STNK = d.getSTNK();
    this.profile = d.getProfilePicture();
    this.balance = d.getBalance();
    this.name = d.getName();
    this.withdrawals = new ArrayList<>();
    this.profile = d.getProfilePicture();
    this.statusPhotoProfile = d.getStatusPhotoProfile();
    this.statusSIM = d.getStatusSIM();
    this.statusSTNK = d.getStatusSTNK();
  }
  
  public List<Withdrawal> getWithdrawals() {
    return withdrawals;
  }
  public void setWithdrawals(List<Withdrawal> withdrawals) {
    this.withdrawals = withdrawals;
  }
  public String getName(){
    return name;
  }
  public String getId() {
    return id;
  }
  public boolean isOffline() {
    return offline;
  }
  public boolean isVerified() {
    return verified;
  }
  public boolean isTakingJob() {
    return takingJob;
  }
  public EmergencyContact getEmergencyDetail() {
    return emergencyDetail;
  }
  public String getSIM() {
    return SIM;
  }
  public ArrayList<String> getSTNK() {
    return STNK;
  }
  public String getProfile() {
    return profile;
  }
  // public ArrayList<ImageVerification> getImageVerifications() {
  //   return imageVerifications;
  // }
  
}
