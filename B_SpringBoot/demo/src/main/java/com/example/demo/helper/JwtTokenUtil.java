package com.example.demo.helper;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.jsonwebtoken.*;
@Component
public class JwtTokenUtil{
	public static final int jwtExpirationMs = 5*60*60;
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);
	public String generateToken(UserDetails userDetails, String secret, String uid) {
		Map<String, Object> claims = new HashMap<>();
		return Jwts.builder().setClaims(claims)
		.setId(uid)
		.setSubject(userDetails.getUsername())
		.setIssuedAt(new Date(System.currentTimeMillis()))
		.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationMs * 1000))
		.signWith(SignatureAlgorithm.HS512, secret.getBytes(Charset.forName("UTF-8"))).compact();
	  }

	  public String getUsernameFromToken(String token, String secret) {
		return Jwts.parser().setSigningKey(secret.getBytes(Charset.forName("UTF-8"))).parseClaimsJws(token.replace("{", "").replace("}","")).getBody().getSubject();
	  }
	  public String getUIDFromToken(String token, String secret) {
		return Jwts.parser().setSigningKey(secret.getBytes(Charset.forName("UTF-8"))).parseClaimsJws(token.replace("{", "").replace("}","")).getBody().getId();
	  }
	  public boolean validateToken(String secret, String authToken, UserDetails userDetails) {
		try {
			Jwts.parser().setSigningKey( secret.getBytes(Charset.forName("UTF-8"))).parseClaimsJws(authToken);
		  return true;
		} catch (SignatureException e) {
		  logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
		  logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
		  logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
		  logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
		  logger.error("JWT claims string is empty: {}", e.getMessage());
		}
		return false;
	  }
	  public boolean isExpiredToken(String secret, String authToken) {
		try {
			Jwts.parser().setSigningKey( secret.getBytes(Charset.forName("UTF-8"))).parseClaimsJws(authToken);
			return false;
		} catch (ExpiredJwtException e) {
			return true;
		} catch (Exception e){
			return true;
		}
	}
}
