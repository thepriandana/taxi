package com.example.demo.model.Parsing;
public class CarAround{
    private Double lat;
    private Double lng;
    private double rotation;
    private int duration;
    public Double getLat() {
        return lat;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public Double getLng() {
        return lng;
    }
    public void setLng(Double lng) {
        this.lng = lng;
    }
    public double getRotation() {
        return rotation;
    }
    public void setRotation(Double rotation) {
        this.rotation = rotation;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public CarAround(Double lat, Double lng, double rotation, int duration) {
        this.lat = lat;
        this.lng = lng;
        this.rotation = rotation;
        this.duration = duration;
    }
}