package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.springframework.data.annotation.Id;
@Document(collection = "Payment")
public class Payment implements Serializable{
    @Id
    private String id;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String userID;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String paymentMethod;
    private String promo;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String paymentRetrieve;
    @JsonProperty(access = Access.WRITE_ONLY)
    private EnumPayment paymentType; //for making sure whether it is from top up/ on demand payment/ etc
    private Float total;
    private ArrayList<ActionLog> actions;
    private Date dateCreate;
    private int status;
    
    public Date getDateCreate() {
        return dateCreate;
    }
    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public Float getTotal() {
        return total;
    }
    public void setTotal(Float total) {
        this.total = total;
    }
    public ArrayList<ActionLog> getActions() {
        return actions;
    }
    public void setActions(ArrayList<ActionLog> actions) {
        this.actions = actions;
    }
    public void addAction(ActionLog log){
        this.actions.add(log);
    }    
    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID) {
        this.userID = userID;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getPaymentMethod() {
        return paymentMethod;
    }
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    public String getPromo() {
        return promo;
    }
    public void setPromo(String promo) {
        this.promo = promo;
    }
    public String getPaymentRetrieve() {
        return paymentRetrieve;
    }
    public void setPaymentRetrieve(String paymentRetrieve) {
        this.paymentRetrieve = paymentRetrieve;
    }
    public EnumPayment getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(EnumPayment paymentType) {
        this.paymentType = paymentType;
    }
    public Payment(String userID, String paymentMethod, String promo, EnumPayment paymentType, Float total) {
        this.userID = userID;
        this.paymentMethod = paymentMethod;
        this.promo = promo;
        this.paymentType = paymentType;
        this.total = total;
        actions = new ArrayList<>();
        this.status = 0;
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0); // same for minutes and seconds
        this.dateCreate = today.getTime();
    }
}
