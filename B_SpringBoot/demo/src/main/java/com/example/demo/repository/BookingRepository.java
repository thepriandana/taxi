package com.example.demo.repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Booking;
import com.example.demo.model.Conveter.DashboardDate;

public interface BookingRepository extends MongoRepository<Booking, String>{
    // @Query("FROM Booking b WHERE _id = ObjectId(:id)")
    // Optional<Booking> findById(@Param("id") String id);
    List<Booking> findBookingByCustomer(@Param("customer") String customer);
    List<Booking> findBookingByDriver(@Param("driver") String driver);
    Optional<List<Booking>> findBookingByDriverOrderByIdDesc(@Param("driver") String driver, Pageable pageable);
    Optional<List<Booking>> findByCustomerOrderByIdDesc(@Param("customer") String customer, Pageable pageable);
    Optional<List<Booking>> findAllByDriverOrderByIdDesc(@Param("driver") String driver, Pageable pageable);
    @Query("FROM Booking b WHERE b.startLoc.lat >= :lat1 AND b.startLoc.lat <= :lat2 AND b.startLoc.lng >= :lng1 AND b.startLoc.lng <= :lng2 AND b.status = 0")
    List<Booking> findBookingByStartLoc(@Param("lat1") Double lat1, @Param("lng1") Double lng1, @Param("lat2") Double lat2, @Param("lng2") Double lng2);
    List<Booking> findByStatus(int status);

    //, count: { $sum: 1 }
    // @Aggregation("{$group : { _id : {month : {$month :  $startTime}, year : {$year :  $startTime}}}}, {$count: 'Total'}")
    // @Aggregation("{$project : { _id: {year: { $year: $startTime }, month: { $month: $startTime }, dayOfMonth: { $dayOfMonth: $startTime },},count: { $sum: 1 },}}")

    @Aggregation("{$project : {year: { $year: $startTime }, month: { $month: $startTime }, dayOfMonth: { $dayOfMonth: $startTime },count: { $sum: 1 }}}")
    List<DashboardDate> groupByStartTime();
    // created_at: {
    //     $gte: ISODate("2010-04-29T00:00:00.000Z"),
    //     $lt: ISODate("2010-05-01T00:00:00.000Z")
    // }
    // @Aggregation("{$project : {}}")
    List<Booking> findByStartTimeBetween(Date startDate, Date endDate);
}
