package com.example.demo.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class ImageVerification {
    private String id;
    private String tagName;
    private String url;
    private LocalDateTime dateAdd;
    private EnumAction status;
    private ArrayList<ActionLog> actions;
    private ArrayList<String> msgLog;
    
    public ImageVerification(String tagName, String url) {
        this.tagName = tagName;
        this.url = url;
        this.msgLog = new ArrayList<>();
        this.actions = new ArrayList<>();
        this.status = EnumAction.IMAGE_CREATED;
        this.dateAdd = LocalDateTime.now();
        this.msgLog.add("ADD IMAGE");
        this.actions.add(new ActionLog(EnumAction.IMAGE_CREATED));
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public LocalDateTime getDateAdd() {
        return dateAdd;
    }
    public void setDateAdd(LocalDateTime dateAdd) {
        this.dateAdd = dateAdd;
    }
    public EnumAction getStatus() {
        return status;
    }
    public void setStatus(EnumAction status) {
        this.status = status;
    }
    public ArrayList<ActionLog> getActions() {
        return actions;
    }
    public void setActions(ArrayList<ActionLog> actions) {
        this.actions = actions;
    }
    public ArrayList<String> getMsgLog() {
        return msgLog;
    }
    public void setMsgLog(ArrayList<String> msgLog) {
        this.msgLog = msgLog;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
    
}
