package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.Settings;
import com.example.demo.model.Promo;
import com.example.demo.model.User;
import com.example.demo.repository.PromoRepository;
import com.example.demo.repository.TaxiRepository;

@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true")
@RestController
@RequestMapping(Settings.ApiMap)
public class PromoController {
  final String apiLoc = Settings.PromoAPI;
  @Autowired
  PromoRepository repository;
  @Value("${jwt.secret}")
  private String secret;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  TaxiRepository userRepo;
  @GetMapping(apiLoc)
  public ResponseEntity<List<Promo>> getAll(){
    try {
      List<Promo> list = new ArrayList<Promo>();
      list.addAll(repository.findAll());
      return new ResponseEntity<>(list, HttpStatus.OK);
    }catch (Exception e){
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  String randomString(int n) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < n; i++)
    text += possible.charAt((int)Math.floor(Math.random() * possible.length()));
    return text;
  }

  @GetMapping(apiLoc+"/{id}")
  public ResponseEntity<Promo> getByID(@PathVariable("id") String id){
    Optional<Promo> data = repository.findById(id);
    return data.map(Promo -> new ResponseEntity<>(Promo, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }
  @PostMapping(apiLoc)
  public ResponseEntity<Promo> create(@RequestHeader(name="Authorization") String token, @RequestBody Promo data){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          Promo _data = repository.save(new Promo(data.getTitle(), data.getDateStart(), data.getDateEnd(),
          data.getCode().isEmpty() ? randomString(6) : data.getCode(), data.getDiscount(), data.getBody()));
          return new ResponseEntity<>(_data, HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }catch (Exception e){
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @PutMapping(apiLoc+"/{id}")
  public ResponseEntity<Promo> update(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id, @RequestBody Promo xdata){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          Optional<Promo> data = repository.findById(id);
          if(data.isPresent()){
            Promo _data = data.get();
            _data.setTitle(xdata.getTitle());
            _data.setCode(xdata.getCode());
            _data.setDiscount(xdata.getDiscount());
            _data.setDateStart(xdata.getDateStart());
            _data.setDateEnd(xdata.getDateEnd());
            _data.setBody(xdata.getBody());
            return new ResponseEntity<>(repository.save(_data), HttpStatus.OK);
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @DeleteMapping(apiLoc+"/{id}")
  public ResponseEntity<Promo> delete(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = userRepo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          repository.deleteById(id);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
