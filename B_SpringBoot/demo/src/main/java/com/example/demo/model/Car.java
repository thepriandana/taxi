package com.example.demo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "Car")
public class Car implements Serializable{
    @Id
    private String id;
    private String driver;
    private String rideClass;
    private boolean verified;
    private List<String> photo;
    private String plate;
    private Double lat;
    private Double lng;
    private Double rotationDeg;
    public Double getRotationDeg() {
        return rotationDeg;
    }
    public void setRotationDeg(Double rotationDeg) {
        this.rotationDeg = rotationDeg;
    }
    public void addPhoto(String pHOTO){
        photo.add(pHOTO);
    }
    public String getDriver() {
        return driver;
    }
    public void setDriver(String driver) {
        this.driver = driver;
    }
    public String getRideClass() {
        return rideClass;
    }
    public void setRideClass(String rideClass) {
        this.rideClass = rideClass;
    }
    public Car(String driver, String plate, String rideClass) {
        this.driver = driver;
        this.plate = plate;
        this.rideClass = rideClass;
        this.photo = new ArrayList<String>();
        verified = false;
    }
    public boolean isVerified() {
        return verified;
    }
    public void setVerified(boolean verified) {
        this.verified = verified;
    }
    public List<String> getPhoto() {
        return photo;
    }
    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }
    public String getId() {
        return id;
    }
    public String getPlate() {
        return plate;
    }
    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Double getLat() {
        return lat;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public Double getLng() {
        return lng;
    }
    public void setLng(Double lng) {
        this.lng = lng;
    }

}
