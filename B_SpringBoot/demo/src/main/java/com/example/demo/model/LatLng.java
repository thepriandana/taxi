package com.example.demo.model;

import java.io.Serializable;

public class LatLng implements Serializable{
    private String lat, lng;
    public LatLng(String lat, String lng){
        this.lat = lat; this.lng = lng;
    }
    public String getLat() {
        return lat;
    }
    public void setLat(String lat) {
        this.lat = lat;
    }
    public String getLng() {
        return lng;
    }
    public void setLng(String lng) {
        this.lng = lng;
    }
}