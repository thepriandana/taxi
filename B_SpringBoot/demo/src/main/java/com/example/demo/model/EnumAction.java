package com.example.demo.model;

public enum EnumAction {
    user_sign_in,
    user_sign_out,
    user_order_requested,
    user_order_canceled,
    user_order_finished,
    user_rating_created,
    user_payment_created,
    user_payment_accepted,
    driver_picture_empty,
    driver_picture_request,
    driver_picture_denied,
    driver_picture_accepted,
    driver_accepted,
    driver_on_the_way,
    driver_finish_order,
    driver_withdrawal_created,
    admin_halt_payment,
    admin_declined_payment,
    IMAGE_ACCEPTED,
    IMAGE_VERIFIED,
    IMAGE_DELETED,
    IMAGE_CREATED,
    IMAGE_DECLINED
}
