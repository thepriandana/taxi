package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.example.demo.model.Parsing.CustomUser;
import com.example.demo.helper.JwtTokenUtil;
import com.example.demo.helper.MessageResponse;
import com.example.demo.helper.Settings;
import com.example.demo.model.User;
import com.example.demo.repository.TaxiRepository;
@CrossOrigin(origins = Settings.CrossOrigin, allowCredentials="true") //angular
@RestController
@RequestMapping(Settings.ApiMap)
public class UserController {
  @Autowired
  TaxiRepository repo;
  @Value("${jwt.secret}")
  private String secret;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @GetMapping(Settings.UserAPI)
  public ResponseEntity<?> getAllUser(@RequestHeader(name="Authorization") String token, @RequestParam(required = false) String username){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          if(username != null && !username.equals("")){
            return new ResponseEntity<>(repo.findByEmailIgnoringCase(username), HttpStatus.OK);
          }else{
            List<CustomUser> data= new ArrayList<CustomUser>();
            List<User> list = repo.findByRoles("client");
            data.addAll(list.stream().map(u -> new CustomUser(u)).toList());
            return new ResponseEntity<>(data, HttpStatus.OK);

          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @GetMapping(Settings.UserAPI+"/{id}")
  public ResponseEntity<?> getUserByID(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          Optional<User> data = repo.findById(id);
          if(data.isPresent()) return ResponseEntity.ok(new CustomUser(data.get()));
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @PostMapping(Settings.UserAPI+"/signup")
  public ResponseEntity<?> createClient(@RequestBody User data){
    try{
      if(repo.findByEmailIgnoringCase(data.getEmail()).isEmpty()){
        BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
        //trim space
        repo.save(new User(data.getName(), data.getEmail().toLowerCase(), enc.encode(data.getPassword()), (data.getRoles().isEmpty() ? "client" : data.getRoles()) ));
        return ResponseEntity.ok(new MessageResponse("Success"));
      }else{
        return ResponseEntity.badRequest().body(new MessageResponse("Username alreay exist"));
      }
    }catch (Exception e){
      return ResponseEntity.badRequest().body(new MessageResponse("Something wrong"));
    }
  }
  @PutMapping(Settings.UserAPI+"/{id}")
  public ResponseEntity<User> updateUser(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id, @RequestBody User xuser){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          Optional<User> data = repo.findById(id);
          if(data.isPresent()){
            User _data = data.get();
            _data.setPassword(xuser.getPassword());
            return new ResponseEntity<>(repo.save(_data), HttpStatus.OK);
          }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @DeleteMapping(Settings.UserAPI+"/{id}")
  public ResponseEntity<User> deleteUser(@RequestHeader(name="Authorization") String token, @PathVariable("id") String id){
    try{
      jwtTokenUtil = new JwtTokenUtil();
      if (token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        Optional<User> userq = repo.findById(jwtTokenUtil.getUIDFromToken(jwtToken, secret));
        if(userq.isPresent() && userq.get().getRoles().equals("admin")){
          repo.deleteById(id);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
