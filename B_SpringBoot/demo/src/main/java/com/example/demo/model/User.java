package com.example.demo.model;

import java.io.Serializable;
import java.util.HashMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "Users")
// @JsonTypeInfo(use = DEDUCTION) // Intended usage
@JsonSubTypes({@Type(Driver.class)})
public class User implements Serializable{
    @Id
    private String id;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String name;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String email;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;
    @JsonProperty(access = Access.WRITE_ONLY)
    private float balance;
    @JsonIgnore
    private HashMap<String, Notification> notif;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String roles;

    public User() {
        //no instance
    }
    public User(String u, String e, String p, String r){
        email = e;
        name = u;
        password = p;
        notif = new HashMap<>();
        roles = r;
        balance = 0;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getId() {
        return id;
    }
    public void addNotification(Notification _notif){
        notif.put(_notif.getId(), _notif);
    }
    public HashMap<String, Notification> getNotif() {
        return notif;
    }
    public void setNotif(HashMap<String, Notification> notif) {
        this.notif = notif;
    }
    @Override
    public String toString() {
        return "User id [id=" + id + "]";
    }
    public String getRoles() {
        return roles;
    }
    public void setRoles(String roles) {
        this.roles = roles;
    }
    public void removeNotifByID(String id){
        notif.remove(id);
    }
    public void addBalance(Float balance){
      this.balance += balance;
    }
    public Float getBalance(){
        return balance;
    }
}
