# Taxi App

This is a taxi app that includes mobile apps for both drivers and users. The driver and user mobile apps are built using the Flutter framework. The backend of the app is developed using the Spring Boot framework, and the admin web interface is created using Angular.

## Features

- Mobile apps for both drivers and users developed using Flutter.
- Backend developed using the Spring Boot framework.
- Admin web interface built with Angular.
- Secure JWT used for authentication and authorization.
- Driver verification process includes uploading photos and license cards.
- Double-checking of bookings using latitude and longitude to ensure accurate location and calculate the price.
- Emergency contact feature for drivers in case of any incidents.
- Admin can cancel active orders and view dashboard statistics including revenue, active/pending/finished bookings, and completion rate.
- Goal setting based on previous performance (daily, monthly, or yearly).
- Users can add ratings after completing a booking.
- Drivers need to upload car images for verification.
- Admin filters and assigns drivers to chosen ride classes.
- Drivers can withdraw money by creating a ticket.

## Screenshots
![Admin view](admin_preview.png)
![Driver App](driver_preview.png)
![User App](user_preview.png)

## Setup Instructions

1. Clone the repository.
2. Install the necessary dependencies for the Flutter mobile apps, Spring Boot backend, and Angular admin web interface.
3. Configure the database settings in the backend.
4. Build and run the Flutter mobile apps on the desired devices.
5. Start the Spring Boot backend server.
6. Run the Angular admin web interface locally or deploy it to a web server.
7. Access the admin web interface and perform necessary verifications and management tasks.

## License
BY CLONING THIS REPOSITORY, YOU AGREE TO THE FOLLOWING TERMS AND CONDITIONS:

1. The software and its accompanying source code and documentation (hereinafter referred to as "the Software") provided in this repository are made available solely for the purpose of demonstration and showcasing the author's knowledge and skills.

2. You are expressly prohibited from distributing, modifying, or creating derivative works of the Software, including its source code.

3. You may not sublicense, rent, lease, sell, or otherwise transfer any rights granted to you under this license.

4. The author of the Software (hereinafter referred to as "the Author") retains all intellectual property rights to the Software, including its source code. This license does not grant you any ownership or proprietary rights to the Software or its source code.

5. The Software, including its source code, is provided "as is," without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and non-infringement. In no event shall the Author be liable for any claim, damages, or other liability arising out of or in connection with the Software or its use.

6. This license shall be governed by and construed in accordance with the laws of Indonesian Goverment.

BY CLONING THIS REPOSITORY, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND AGREE TO BE BOUND BY THE TERMS AND CONDITIONS OF THIS LICENSE.
