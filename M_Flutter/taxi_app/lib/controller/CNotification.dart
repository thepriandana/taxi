import 'dart:convert';
import 'package:get/get.dart';

import '../GlobalSettings.dart';
import '../model/Notification.dart';
import '../repository/RUser.dart';

class NotificationController extends GetxController {
  final notification = <UserNotification>[].obs;
  void getNotification() async {
    String url = "${GS.base_url}/notif";
    await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<UserNotification> preList = objJson.map((a) => UserNotification.fromJSON(a)).toList();
      notification(preList);
      update();
    });
  }
}