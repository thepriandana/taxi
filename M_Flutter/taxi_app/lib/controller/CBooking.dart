import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:taxi_app/model/RideClass.dart';

import '../GlobalSettings.dart';
import '../model/Booking.dart';
import '../model/CustomeLatLng.dart';
import '../model/Payment.dart';
import '../repository/RUser.dart';

class BookingController extends GetxController{
  final payment = <Payment>[].obs;
  final rideClass = <RideClass>[].obs;
  final selectedRideClass = RideClass().obs;
  final bookings = <Booking>[].obs;
  final tmpBooking = Booking().obs;
  final hideFD = false.obs;
  final distanceText = "".obs;
  final distance = 0.0.obs;
  final durationText = "".obs;
  final duration = 0.0.obs;
  static BookingController get to => Get.find(); // add this line
  BookingController() {
    // tmpBooking.update((val){
    // val?.endLoc(CustomeLatLng(lat:-8.717704050668829, lng: 115.16855005204272));
    //     val?.endLocName = "Pantai Kuta";
    //     val?.startLoc(CustomeLatLng(lat:-8.746719796770327,lng: 115.16678631305693));
    // val?.startLocName = "Bandar Udara Internasional Ngurah Rai";
    // });

    // Get.offNamed("/pages/0");
  }
  Future<void> getBookingAll() async {
    String url = "${GS.base_url}/booking";
    await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<Booking> preList = objJson.map((a) => Booking.fromJSON(a)).toList();
      bookings(preList);
      update();
    });
  }
  Future<void> cancelLastBooking() async{
    String url = "${GS.base_url}/booking/cancel";
    await RUser().sendJWTCURL(url).then((value) {
      if(jsonDecode(value)["message"] != "Nothing"){
        //success
        hideFD(false);
      }else{
        //nothing found
      }
    });
  }
  Future<void> sendRating(Booking b) async{
    String url = "${GS.base_url}/booking/rating";
    await RUser().sendJWTCURL(url, b).then((value){

    });
  }
  Future<Booking> getBookingByID(String uid) async {
    String url = "${GS.base_url}/booking/$uid";
    Booking p = await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value);
      return Booking.fromJSON(objJson);
    });
    return p;
  }
  Future<void> refreshPaymentMethod() async {
    String url = "${GS.base_url}/paymentM";
    await RUser().sendCURL(url).then((value) async{
      var objJson = jsonDecode(value) as List;
      List<Payment> preList = objJson.map((a) => Payment.fromJSON(a)).toList();
      payment(preList);
      print(payment);
    });
  }

  Future<void> refreshRideClassMethod() async {
    String url = "${GS.base_url}/rideclass";
    await RUser().sendCURL(url).then((value) async{
      var json = jsonDecode(value)  as List;
        List<RideClass> preList = json.map((a) => RideClass.fromJSON(a)).toList();
        rideClass.clear();
        rideClass.addAll(preList);
        print(rideClass);
    });
  }

  Future<bool> sendBookingRequest() async{
    String url = "${GS.base_url}/booking";
    //validate tmpBooking first
    // print("Send Booking now: $tmpBooking");
    if(tmpBooking.value.startLocName.toString() != "" && tmpBooking.value.startLocName!.isNotEmpty &&
        tmpBooking.value.endLocName.toString() != "" && tmpBooking.value.endLocName!.isNotEmpty){
      if(tmpBooking.value.payment.toString() == ""){
        tmpBooking.update((val) {
          val?.payment = payment.elementAt(0).id;
        });
      }

      if(tmpBooking.value.rideClass.toString() == ""){
        tmpBooking.update((val) {
          val?.rideClass = rideClass.elementAt(0).id;
        });
      }
      String isSend = "";
      await RUser().sendJWTCURL(url, tmpBooking.value.toMap()).then((value){
        isSend = "Success";
        print(value.toString());
      });
      if(isSend == "Success") {
          Get.back();
          Get.toNamed("/findingDriver");
      }
      return isSend == "Success";
    }else{
      return false;
    }
  }
  Future<void> findLastActiveBooking() async{
    String url = "${GS.base_url}/booking/active";
    await RUser().sendJWTCURL(url).then((value) {
      if(jsonDecode(value)["message"] != "Nothing"){
        Map<String, dynamic> map = jsonDecode(value)["message"];
        Booking tmp = Booking.fromJSON(map);
        tmpBooking(tmp);
        print(tmpBooking);
      }else{
        tmpBooking(Booking());
      }
    });
  }
}