
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:dio/dio.dart';
import 'package:latlong2/latlong.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:taxi_app/model/CustomeLatLng.dart';
import 'package:taxi_app/repository/RUser.dart';
import 'package:http/http.dart' as http;

import '../GlobalSettings.dart';
import '../element/CustomeMarkers.dart';
import '../model/Car.dart';
import '../model/PlaceX.dart';
import 'CBooking.dart';

class MapsController extends GetxController {
  final markersX = <Marker>[].obs;
  final position = CustomeLatLng().obs;
  final centerPosition = MapOptions().obs;
  final suggestionPlace = <PlacesX>[].obs;
  final bookingController = Get.put(BookingController());
  //if near current location gps then set it become true
  final isCurrentLocationUsed = true.obs;
  final isCentered = true.obs;
  final sname = "".obs;
  final mapC = MapController().obs;
  final polylineDriver = <LatLng>[].obs;
  final polylineOriginDestination = <LatLng>[].obs;
  // static List<Polyline> polyline = <Polyline>[];
  MapsController({CustomeLatLng? latLng}){
    position(latLng);
    bookingController.tmpBooking.update((val) {
      val?.startLoc(position.value);
    });
    suggestionPlace.add(PlacesX()); //add first
    scaffoldKey = new GlobalKey<ScaffoldState>();
    debounce(sname, (_) async{
      if(sname.value.length>0) {
        suggestionPlace([]);
        suggestionPlace.add(PlacesX()); //add first
        Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input=$sname&location=${position.value.lat},${position
                  .value.lng}&radius=50&key=" + GS.aizaKey);
          final client = new http.Client();
          Map<String, String> requestHeaders = {
            'Content-type': 'application/json',
            'Accept': 'application/json'
          };
          print("$uri");
          var response = await client.get(uri,headers: requestHeaders);
          if (response.statusCode == 200) {
            var objJson = await jsonDecode(response.body.replaceFirst("yes", ""))["predictions"] as List;
            Map x = objJson.asMap();
            x.forEach((key, value) {
              if(value["structured_formatting"]["main_text"] != null && value["types"] != null) {
                suggestionPlace.add(PlacesX.fromX(value["place_id"], value["structured_formatting"]["main_text"]));
              }
            });
          }
      }
    },  time: Duration(seconds: 1));
    debounce(bookingController.tmpBooking, (_){

    });
  }
  void updateFirstLocation(CustomeLatLng latLng){
    position(latLng);
  }
  late GlobalKey<ScaffoldState> scaffoldKey;
  Future<void> getLocation() async {



    late LocationSettings locationSettings;
    if (defaultTargetPlatform == TargetPlatform.android) {
      locationSettings = AndroidSettings(
          accuracy: LocationAccuracy.high,
          distanceFilter: 1,
          // forceLocationManager: true,
          intervalDuration: const Duration(seconds: 5),
      );
    } else if (defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.macOS) {
      locationSettings = AppleSettings(
        accuracy: LocationAccuracy.high,
        activityType: ActivityType.fitness,
        distanceFilter: 1,
        pauseLocationUpdatesAutomatically: true,
        // Only set to true if our app will be started up in the background.
        showBackgroundLocationIndicator: false,
      );
    } else {
      locationSettings = LocationSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 100,
      );
    }
    Geolocator.getPositionStream(locationSettings: locationSettings).listen((Position? _position) async {
     if(_position != null) {
       position(CustomeLatLng(lat:_position.latitude, lng:_position.longitude));
     }
     //updateMapX();
   });
  }
  Future<void> updateMapX() async {
    late List<Marker> tmp = [];
    if (bookingController.tmpBooking.value.status == -1){
    List<Car> carAround = await getCarAround(LatLng(position.value.lat.toDouble(), position.value.lng.toDouble()));
      for (var c in carAround)
        tmp.add(new Marker(
            point: LatLng(c.lat!.toDouble(),c.lng!.toDouble()),
            width: 30,
            height: 30,
            builder: (context) => Transform.rotate(
                angle:  c.rotation!,child: Image(
              width: 30,
              height: 30,
              image: AssetImage("lib/asset/car.png"),
              fit: BoxFit.contain,
            ))
        ));
    } else if (bookingController.tmpBooking.value.status == 1) {
      tmp.add(new Marker(
          point: LatLng(bookingController.tmpBooking.value.driverCar.value.lat!, bookingController.tmpBooking.value.driverCar.value.lng!),
          width: 30,
          height: 30,
          builder: (context) => Transform.rotate(
              angle:  bookingController.tmpBooking.value.driverCar.value.rotation!,child: Image(
            width: 30,
            height: 30,
            image: AssetImage("lib/asset/car.png"),
            fit: BoxFit.contain,
          ))
      ));
    }
    if(bookingController.tmpBooking.value.endLocName.toString() != "") {
      if(polylineOriginDestination.isEmpty){
        await getPolyline(LatLng(bookingController.tmpBooking.value.startLoc.value.lat.toDouble(), bookingController.tmpBooking.value.startLoc.value.lng.toDouble()),
            LatLng(bookingController.tmpBooking.value.endLoc.value.lat.toDouble(), bookingController.tmpBooking.value.endLoc.value.lng.toDouble())
        ).then((value) => polylineOriginDestination(value));

    }
      tmp.add(new Marker(
          point: LatLng(bookingController.tmpBooking.value.endLoc.value.lat.toDouble(),
              bookingController.tmpBooking.value.endLoc.value.lng.toDouble()),
          width: 550,
          height: 100,
          builder: (context) => CustomeMarkers(placeName: "${bookingController.tmpBooking.value.endLocName}")));
    }
    if(bookingController.tmpBooking.value.startLocName.toString() != "") tmp.add(new Marker(
        point: LatLng(bookingController.tmpBooking.value.startLoc.value.lat.toDouble(),bookingController.tmpBooking.value.startLoc.value.lng.toDouble()),
        width: 550,
        height: 100,
        builder: (context) => CustomeMarkers(isDestination: true, placeName: "${bookingController.tmpBooking.value.startLocName}")));
    markersX(tmp);
    moveCameraAfter(tmp);
    mapC.refresh();
  }
  void updateSuggestions(String placeName) async {
    sname(placeName);
  }
  double calculateD(LatLng ax, LatLng bx) {
    var R = 6371;
    var dLat = (ax.latitude - bx.latitude) * pi / 180;
    var dLon = (ax.longitude - bx.longitude) * pi / 180;
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(ax.latitude*  pi / 180) * cos(bx.latitude * pi / 180) *
            sin(dLon / 2) * sin(dLon / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    return R * c;
  }
  double calculateZoom(double widthPixel,double ratio,double lat,double length){
    length = length *1000;
    double k = widthPixel * 156543.03392 * cos(lat * pi / 180);
    double myZoom = ( log( (ratio * k)/(length*100) )/ln2 );
    return(myZoom);
  }
  Future<void> getLastLocation() async{
    var box = await Hive.openBox('taxi_app');
    String lastLocation = box.get("last_location").toString();
    if(lastLocation.isNotEmpty && lastLocation != "[]" && lastLocation != "null"){
      var json = jsonDecode(lastLocation);
      LatLng location = LatLng(json[0], json[1]);
      // cameraPosition = CameraPosition(target: location, zoom: 14.4746);
    }else{
      // cameraPosition = CameraPosition(target: LatLng(0,0));
    }
  }
  void moveCameraAfter(List<Marker> m) async {
    double latMin = 0, latMax=0, lngMax = 0, lngMin = 0;
    if (m.length > 1) {
      for (var i = 0; i < m.length; i++) {
        if(i==0){
          latMin = m.elementAt(i).point.latitude;
          latMax = m.elementAt(i).point.latitude;
          lngMin = m.elementAt(i).point.longitude;
          lngMax = m.elementAt(i).point.longitude;
        }else{
          if(latMin>m.elementAt(i).point.latitude){
            latMin = m.elementAt(i).point.latitude;
          }
          if(latMax<m.elementAt(i).point.latitude){
            latMax = m.elementAt(i).point.latitude;
          }
          if(lngMin>m.elementAt(i).point.longitude){
            lngMin = m.elementAt(i).point.longitude;
          }
          if(lngMax<m.elementAt(i).point.longitude){
            lngMax = m.elementAt(i).point.longitude;
          }
        }
      }
      double latX= (latMin + latMax) / 2, lngX= (lngMin+lngMax)/2,
          dis = calculateD(new LatLng(latMin, lngMin), new LatLng(latMax, lngMax)),
          jom = calculateZoom(Get.width, 90, latX, dis);
      // final GoogleMapController controller = await mController.future;
      centerPosition(MapOptions(
          center: LatLng((latMin+latMax)/2,(lngMin+lngMax)/2),
          zoom: jom,
          onPositionChanged: (MapPosition position, hasGesture){
            if(hasGesture) isCentered(false);
          }));
      if(isCentered.value) mapC.value.moveAndRotate(LatLng((latMin+latMax)/2,(lngMin+lngMax)/2), jom, 0);
    }
  }
  Future<List<Car>> getCarAround(LatLng loc) async {
    if(loc.longitude == 0.0 && loc.latitude == 0.0){
      return [];
    }
    String url = "${GS.base_url}/car?lat=${loc.latitude}&lng=${loc.longitude}";
    List<Car> tmp = await RUser().sendCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<Car> carList = objJson.map((parseX) => Car.fromJSON(parseX)).toList();
      return carList;
    });
    return tmp;
  }
  Future<List<LatLng>> getPolyline(LatLng ori, LatLng des) async{
    // check if polly line is empty
    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(GS.aizaKey,
      PointLatLng(ori.latitude, ori.longitude),
      PointLatLng(des.latitude, des.longitude),
      travelMode: TravelMode.driving,
    ).then((value){
      print("Polyline requested");
      return value;
    });

    List<LatLng> polylineCoordinates = [];
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      updateDistanceDuration();
    }
    // Polyline _polyline = Polyline(color: Colors.blue, points: polylineCoordinates, strokeWidth: 3);
    return polylineCoordinates;
  }
  Future<void> selectLocation({String? placeID, CustomeLatLng? latLng, required bool isOrigin}) async {
    String q = "";
    if(placeID != null){
      q = "&place_id=$placeID";
    }else if(latLng!=null){
      q = "&latlng=${latLng.lat},${latLng.lng}";
    }
    Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/geocode/json?key=" + GS.aizaKey + "$q");
    final client = new http.Client();
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json'
    };
    print("$uri");
    var response = await client.get(uri,headers: requestHeaders);
    if (response.statusCode == 200) {
      var objJson = await jsonDecode(response.body);
      String name = "";
      String ftype= objJson["results"][0]["address_components"][0]["types"][0].toString();
      if(ftype != "street_number"){
        //use num 0
        name = objJson["results"][0]["address_components"][0]["long_name"];
      }else{
        if(objJson["results"][0]["address_components"][1]["types"][0].toString() == "plus_code"){
          //use formatted text
          name = objJson["results"][0]["formatted_address"];
        }else{
          name = objJson["results"][0]["address_components"][1]["long_name"];
        }
        //check if num 1 is plus_code then use formated text rather than num 1
      }
      var lat = objJson["results"][0]["geometry"]["location"]["lat"];
      var lng = objJson["results"][0]["geometry"]["location"]["lng"];
      CustomeLatLng c = CustomeLatLng(lat: lat, lng: lng);
      bookingController.tmpBooking.update((val) {
        print("we x $name, $c");
        if(isOrigin){
          val?.startLocName = name;
          val?.startLoc(c);
        }else{
          val?.endLocName = name;
          val?.endLoc(c);
        }
      });
      bookingController.update();
      suggestionPlace([]);
      suggestionPlace.add(PlacesX()); //add first
    }
  }
  Future<void> updateDistanceDuration() async{
    Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/distancematrix/json?destinations="+
      bookingController.tmpBooking.value.startLoc.value.lat.toString()+","+bookingController.tmpBooking.value.startLoc.value.lng.toString()
      +"&origins="+ bookingController.tmpBooking.value.endLoc.value.lat.toString()+","+bookingController.tmpBooking.value.endLoc.value.lng.toString() + "&key="+GS.aizaKey);
    final client = new http.Client();
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json'
    };
    print("$uri");
    var response = await client.get(uri,headers: requestHeaders);
    if (response.statusCode == 200) {
      var objJson = await jsonDecode(response.body);
      int distance = objJson["rows"][0]["elements"][0]["distance"]["value"];
      String durationText = objJson["rows"][0]["elements"][0]["duration"]["text"].toString();
      int duration = objJson["rows"][0]["elements"][0]["duration"]["value"];
      bookingController.distanceText("${distance / 1000}+km");
      bookingController.distance(distance/1000);
      bookingController.durationText(durationText);
      bookingController.duration(duration/60);
      // bookingController.kmprice(int.parse(bookingController.selectedRideClass.value.price!)*(distance/100));
    }
  }
  // void change(){
  //
  // }

//   List<Marker> _buildMarkersOnMap() {
//     List<Marker> markers = <Marker>[];
//     var marker = new Marker(
//         point: LatLng(lastLocation.lat.toDouble(),lastLocation.lng.toDouble()),
//         width: 550,
//         height: 100,
//         builder: (context) => CustomeMarkers(placeName: "JL. Akasia XV/11A..",)
//     );
//     markers.add(marker);
//     markers.add(new Marker(
//         point: LatLng(lastLocation.lat.toDouble()-0.003,lastLocation.lng.toDouble()),
//         width: 550,
//         height: 100,
//         builder: (context) => CustomeMarkers(isDestination: true, placeName: "Bandara Ngurah..")
//     ));
//     return markers;
//   }
// }
}