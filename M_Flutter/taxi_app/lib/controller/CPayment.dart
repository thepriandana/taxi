import 'dart:convert';
import 'package:get/get.dart';

import '../GlobalSettings.dart';
import '../model/Payment.dart';
import '../repository/RUser.dart';

class PaymentController extends GetxController {
  final notification = <Payment>[].obs;
  void getPaymentM() async {
    String url = "${GS.base_url}/paymentM";
    await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<Payment> preList = objJson.map((a) => Payment.fromJSON(a)).toList();
      notification(preList);
    });
  }
}