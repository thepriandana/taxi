
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:taxi_app/repository/RUser.dart';

import '../GlobalSettings.dart';
import '../Helper.dart';
import '../model/User.dart';
import 'CBooking.dart';

class UserController extends GetxController{
  // final BookingController bookingController = Get.find();
  final statusLogin = 0.obs;  //-1 Error, 0 Not login, 1 Login
  final user = User().obs;
  final hidePassword = true.obs;
  final loading = false.obs;
  OverlayEntry? loader;
  UserController(){
    loginFormKey = new GlobalKey<FormState>();
    registerFormKey = new GlobalKey<FormState>();
  }
  late GlobalKey<FormState> registerFormKey;
  late GlobalKey<FormState> loginFormKey;
  late GlobalKey<ScaffoldState> scaffoldKey;
  Future<void> checkTokenSplash() async {
    String url = "${GS.base_url}/splash";
    await RUser().sendJWTCURL(url).then((value){
      String tokenStatus = jsonDecode(value)["message"].toString();
      if(tokenStatus == "valid"){
        statusLogin(1);
        update();
        print("WX ${statusLogin.value}");
      }else{
        logout();
      }
    }).onError((error, stackTrace){
      statusLogin(-1);
    });
  }
  Future<bool> checkLogin() async{
    var box = await Hive.openBox('taxi_app');
    String status = box.get("current_user").toString();
    return (status.isNotEmpty && status != "[]" && status != "null");
  }
  void logout() async{
    print("Logout called");
    statusLogin(0);
    hidePassword(true);
    user(User());
    var box = await Hive.openBox('taxi_app');
    await box.delete("current_user");
  }
  void register() async{
    if (loginFormKey.currentState!.validate()) {
      loginFormKey.currentState!.save();
      RUser().userDo("post", "signup", user.value.toMap(), true).then((value){
        // Map<String, dynamic> json = jsonDecode(value.toString());
        Get.snackbar("", "signUpComplete".tr);
        login();
      }).catchError((e) {
        Helper.hideLoader(loader!);
        // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
        Get.snackbar("",e.toString());
      }).whenComplete(() {
        Helper.hideLoader(loader!);
      });
    }
  }
  Future<void> login() async{
    if (loginFormKey.currentState!.validate()) {
      loginFormKey.currentState!.save();
      RUser().userDo("post", "signin", user.value.toMap(), true).then((value) async{
        var box = await Hive.openBox('taxi_app');
        await box.put("current_user", value.toString());
        statusLogin(1);
        Get.snackbar("","welcome".tr);
        Get.toNamed("/pages/0");
      }).catchError((e) {
        // Helper.hideLoader(loader!);
        Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
        Get.snackbar("",json["message"].toString());
      }).whenComplete(() {
        // Helper.hideLoader(loader!);
      });
    }
  }
}