
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../GlobalSettings.dart';
import '../model/Promo.dart';
import '../repository/RUser.dart';

class CouponController extends GetxController{
  final promo = <Promo>[].obs;
  late GlobalKey<ScaffoldState> scaffoldKey;
  void getPromo() async {
    String url = "${GS.base_url}/promo";
    await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<Promo> preList = objJson.map((a) => Promo.fromJSON(a)).toList();
      promo(preList);
    });
  }
}