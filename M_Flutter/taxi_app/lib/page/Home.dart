import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:taxi_app/controller/CMap.dart';
import 'package:taxi_app/element/BottomOrderDetailWidget.dart';
import 'package:taxi_app/element/BottomRequestRideWidget.dart';
import 'package:taxi_app/model/CustomeLatLng.dart';

import '../controller/CBooking.dart';
import '../controller/CUser.dart';
import '../model/Booking.dart';
import 'package:get/get.dart';

class Home extends StatelessWidget with WidgetsBindingObserver{
  Home({required this.lastLocation});
  late CustomeLatLng lastLocation;
  AppLifecycleState appLifecycleState = AppLifecycleState.detached;
  final BookingController _bookingController = Get.find();
  final UserController _con = Get.find();
  final MapsController _mapsController = Get.find();
  // late GlobalKey<AutoCompleteTextFieldState<String>> keyOrigin;
  final useMap = false.obs;
  final isTimerMarching = false.obs;
  late Timer timer;
  @override
  Widget build(BuildContext context) {
    print("$lastLocation XOO");
    _mapsController.updateFirstLocation(lastLocation);
    init();
    return Scaffold(
        body: Container(
            child:
            Stack(children: [
              Positioned(child:
                Obx(() => FlutterMap(
                  mapController: _mapsController.mapC.value,
                  options: _mapsController.centerPosition.value,
                  nonRotatedChildren: [
                    AttributionWidget.defaultWidget(
                      source: 'Google',
                      onSourceTapped: null,
                    ),
                  ],
                  children: [
                    TileLayer(
                      urlTemplate: "https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga",
                      userAgentPackageName: "Google",
                      tileFadeInStartWhenOverride: 0.1,
                      // urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                      // userAgentPackageName: 'com.example.app',
                    ),
                    PolylineLayer(
                      polylineCulling: true,
                      polylines: [
                        Polyline(
                          points: (_mapsController.polylineDriver.isEmpty) ? [] : _mapsController.polylineDriver,
                          color: Colors.orangeAccent
                        ),
                        Polyline(
                            points: (_mapsController.polylineOriginDestination.isEmpty) ? [] : _mapsController.polylineOriginDestination,
                            color: Colors.blue
                        )
                      ],
                    ),
                    new MarkerLayer(markers: _mapsController.markersX),
                  ],
                )
              )),
              Positioned(child:
              Row(crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(padding: EdgeInsets.only(left: 10, top: 35), child: SizedBox(
                      height: 50,
                      child: FloatingActionButton(
                        heroTag:"Z",
                        onPressed: (){
                          Scaffold.of(context).openDrawer();
                        },
                        child: Icon(Icons.menu, color: Colors.black87),
                        backgroundColor: Colors.white,
                      ),
                    )),
                    Expanded(child: Obx(() => (_bookingController.tmpBooking.value.status != -1 ) ? SizedBox():
                    Flex(direction: Axis.vertical,children: [ Container(
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5), //color of shadow
                                  spreadRadius: 5, //spread radius
                                  blurRadius: 7, // blur radius
                                  offset: Offset(0, 2))],
                            border: Border.all(color: Colors.transparent, width: 1),
                            borderRadius: BorderRadius.circular(7), color: Colors.white
                        ),
                        padding: EdgeInsets.only(top:0, bottom: 0, left: 5, right: 5),
                        margin: EdgeInsets.only(top:35, left: 10, right: 10),
                        child:  Stack(children: [
                          Positioned(child: Image(
                            width: 100,
                            image: AssetImage("lib/asset/dot.png"),
                            // fit: BoxFit.contain,
                          ), left: -35, height: 55,top: 25),
                          Positioned(child: Icon(CupertinoIcons.circle_fill, size: 10, color: Colors.blue), top: 20, left: 10),
                          Positioned(child: Icon(CupertinoIcons.square_fill, size: 10), bottom: 20, left: 10),
                          Positioned(child: Padding(padding: EdgeInsets.only(left: 30, right: 10), child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Expanded(flex: 12,child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 8, bottom:5),
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(3),
                                        color: Colors.grey.shade50),
                                    child:
                                    TextFormField(onTap: (){
                                      Get.toNamed("/locationFinder", arguments: "origin");
                                    }, decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: (_bookingController.tmpBooking.value.startLocName == "") ? 'Current Location' : _bookingController.tmpBooking.value.startLocName,
                                        isDense: true,
                                      ),
                                      readOnly: true,
                                    ),
                                  ),
                                  Divider(height: 1,),

                                  Container(
                                    margin: EdgeInsets.only(top: 5, bottom: 8),
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3),
                                    color: Colors.grey.shade200),
                                    child:
                                      TextFormField(onTap: (){
                                        Get.toNamed("/locationFinder", arguments: "destination");
                                      },
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                            hintText: (_bookingController.tmpBooking.value.endLocName == "") ? 'Where to go?' : _bookingController.tmpBooking.value.endLocName,
                                          isDense: true
                                        ),
                                        readOnly: true,
                                      )
                                  ),
                                ], mainAxisSize: MainAxisSize.min,
                              ))
                            ])
                          ))
                        ])
                      ),
                      Align(alignment: Alignment.bottomRight,
                        child: Container(margin: EdgeInsets.only(right: 0, top: 10),
                            child: SizedBox(
                                height: 35,
                                child: FloatingActionButton(
                                  onPressed: (){
                                    _mapsController.isCentered(true);
                                    },
                                  child: Icon(CupertinoIcons.compass, color: Colors.blueAccent),
                                  backgroundColor: Colors.white,
                                )
                            ))
                      )
                    ])
                    )),
                  ])
              ),
              Align(alignment: Alignment.bottomCenter,
                  child: Obx(() => (_bookingController.tmpBooking.value.status != -1) ? BottomOrderDetailWidget() : BottomHomeWidget()))
            ])
        )
      // bottomNavigationBar:  (hideRequestForm.value == true) ? SizedBox() :BottomHomeWidget()
    );
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    appLifecycleState = state;
  }
  void init() async {
    _mapsController.getLocation();
    WidgetsBinding.instance?.addObserver(this);
    if(useMap.value == true){
      _mapsController.getLocation();
    }
    //check if there is last error on splash, show dialog and retry
    void start() {
      isTimerMarching(true);
      timer = Timer.periodic(const Duration(seconds: 5), (timer) async {
        if (_con.statusLogin.value == 1)
          if (_bookingController.tmpBooking.value.status == 0 || _bookingController.tmpBooking.value.status == 1) {
            await _bookingController.findLastActiveBooking();
          }
        if (appLifecycleState != AppLifecycleState.paused) {
          //update map if app not in background
          if(isTimerMarching.value) _mapsController.updateMapX();
        }
      });
    }
    ever(_bookingController.tmpBooking, (_) async{
      print("We called ${_bookingController.tmpBooking.value.status}");
      if(_bookingController.tmpBooking.value.status == 0 && _bookingController.hideFD.value == false){
        _bookingController.hideFD(true);
        Get.toNamed("/findingDriver");
      }else if(_bookingController.tmpBooking.value.status == 1){
        if(_mapsController.polylineDriver.isEmpty){
          await _mapsController.getPolyline(LatLng(_bookingController.tmpBooking.value.driverCar.value.lat!, _bookingController.tmpBooking.value.driverCar.value.lng!),
              LatLng(_bookingController.tmpBooking.value.startLoc.value.lat.toDouble(), _bookingController.tmpBooking.value.startLoc.value.lng.toDouble())
          ).then((value) => _mapsController.polylineDriver(value));
        }
      }else if(_bookingController.tmpBooking.value.status == 3){
        if(_bookingController.hideFD.value == true){
          Get.toNamed("/orderDetail", arguments: _bookingController.tmpBooking.value);
          _bookingController.hideFD(false);
          print("there is exist before");
        }
        _bookingController.tmpBooking(Booking());
        //remove poly line
        _mapsController.polylineOriginDestination([]);
        _mapsController.polylineDriver([]);
      }
    });
    if(isTimerMarching.value == false) start();
    if(_con.statusLogin.value == 1){
      _bookingController.findLastActiveBooking();
    }
  }
  ///
}
