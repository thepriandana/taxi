import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CBooking.dart';
import 'package:taxi_app/element/ItemOrderWidget.dart';
class History extends StatelessWidget{
  final currentTab = 0.obs;
  @override
  Widget build(BuildContext context) {
    final BookingController bookingController = Get.find();
    return Scaffold(body: SafeArea(
        child: GetBuilder<BookingController>(
          initState: (_) => bookingController.getBookingAll(), // INIT IT ONLY THE FIRST TIME
          builder: (_) => (bookingController.bookings.length == 0) ?
          Container(
              child:Center(
                child: Column(mainAxisSize: MainAxisSize.min,
                  children: [
                    Image(image: AssetImage("lib/asset/empty.png"), height: 150,),
                    Center(child: Text("emptyOrder".tr, style: TextStyle(color: Colors.black38),)),
                    SizedBox(height: 200,)
                  ],
                ),)
          ) :
          Column(children: [
            Row(
              children: [
                Expanded(child: InkWell(
                    onTap: (){
                      currentTab(0);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Text("Active", textAlign: TextAlign.center),
                    ))
                ),
                Expanded(
                  child: InkWell(
                      onTap: () {
                        currentTab(1);
                      },
                      child: Padding(
                        padding: EdgeInsets.all(15),child: Text("History", textAlign: TextAlign.center),
                      )),
                )
              ],
            ),
            Container(
                child: Expanded(
                    child: Obx(() => ListView.builder(
                      itemCount: bookingController.bookings.length,
                      itemBuilder: (context, index){
                        return InkWell(child: ItemOrderWidget(booking: bookingController.bookings.elementAt(index)), onTap: (){
                          // print( bookingController.bookings.elementAt(index));
                          Get.toNamed("/orderDetail", arguments: bookingController.bookings.elementAt(index));
                        });
                      },
                    ))
                )
            )
          ]),
        )));
  }
}