import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CBooking.dart';

import '../model/Booking.dart';

class OrderDetail extends StatelessWidget{
  final BookingController bc = Get.find();
  final Booking booking = Get.arguments;
  final status = "".obs;
  void init() {
    // // TODO: implement initState
    switch(booking.status){
      case 0: status("Requested"); break;
      case 1: status("Active"); break;
      case 2: status("Canceled"); break;
      case 3: status("Finished"); break;
      default: status(""); break;
    }
  }
  @override
  Widget build(BuildContext context) {
    init();
    return Scaffold(
        body: SafeArea(
          child:
          GetBuilder<BookingController>(
              initState: (_) => init(),
              builder: (_) =>
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Card(margin: EdgeInsets.all(15),child: Padding(padding: EdgeInsets.all(15), child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(child:
                                Column(children: [
                                  Text("DK 55 AA", style: TextStyle(fontWeight: FontWeight.bold)),
                                  Text("${booking.driverName}")
                                ], crossAxisAlignment: CrossAxisAlignment.start,
                                )),
                                Text("$status")
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Fare"),
                                    SizedBox(height: 5),
                                    Text("Booking ID"),
                                    SizedBox(height: 5),
                                    Text("Class")
                                  ],
                                ),
                                  flex: 0,
                                ),
                                Expanded(child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                                      Text("${booking.payment}"),
                                      SizedBox(width: 5),
                                      Text("12K", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green))
                                    ]),
                                    SizedBox(height: 5),
                                    Text("${booking.id}", style: TextStyle(fontWeight: FontWeight.bold)),
                                    SizedBox(height: 5),
                                    Text("Economy", style: TextStyle(fontWeight: FontWeight.bold))
                                  ],
                                ))
                              ],
                            ),
                            //Key point
                            SizedBox(height: 15),
                            Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Flex(
                                    direction: Axis.horizontal,
                                    children: [
                                      Icon(Icons.check_circle_sharp, color: Colors.grey.shade200,),
                                      SizedBox(width: 5),
                                      Text("Driver accept the order.")
                                    ]),
                                SizedBox(height: 5),
                                Flex(direction: Axis.horizontal,
                                    children: [
                                      Icon(Icons.arrow_circle_right_rounded, color: Colors.yellow.shade700),
                                      SizedBox(width: 5),
                                      Text("Driver on the way to your location.")
                                    ]),
                              ],
                            ),
                            SizedBox(height: 15,),
                            Text("Driver Note", style: TextStyle(color: Theme.of(context).primaryColorDark)),
                            SizedBox(height: 5,),
                            Text("${booking.note??"Empty."}")
                          ],
                        ),
                        ),
                        ),
                        (booking.status! == 3) ?
                        Card(margin: EdgeInsets.only(left: 15, bottom: 15, right: 15),
                          child: Padding(padding: EdgeInsets.all(15), child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Center(child: Text("How are your trip", style: TextStyle(fontWeight: FontWeight.bold),)),
                              SizedBox(height: 5),
                              Center(child:
                              RatingBar.builder(
                                glow: false,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: false,
                                itemCount: 5,
                                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  sendRating(rating.toInt());
                                },
                              ))
                            ],
                          ),),
                        ) : (booking.status == 0) ? Padding(padding: EdgeInsets.only(left: 15, right: 15),child:Material(
                          color: Theme.of(context).primaryColor,
                          child: InkWell(
                              onTap: () {
                                //alert dialog first
                                cancelOrder();
                              },
                              child: SizedBox(
                                height: kToolbarHeight,
                                width: double.infinity,
                                child: Center(
                                  child: Text(
                                    'Cancel',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              )),
                        )) : SizedBox()
                      ]
                  )
          ),
        )
    );
  }
  cancelOrder(){
    if(booking.status == 0){
      Get.dialog(AlertDialog(
          title: const Text('Cancel Order'),
          content: const Text('Confirm cancellation'),
          actions: [
            TextButton(
              onPressed: () {
                Get.back();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                bc.cancelLastBooking().then((value) => Get.back());
              },
              child: const Text('Confirm'),
            ),
          ],
        ),
      );
    }
  }
  sendRating(int index) async {
    if(booking.rating == null){
      Get.dialog(AlertDialog(
          title: const Text('Submit Rating'),
          content: const Text('Confirm submission'),
          actions: [
            TextButton(
              onPressed: () {
                Get.back();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                Booking tmp = booking;
                tmp.rating = index;
                bc.sendRating(tmp).then((value) => Get.back());
              },
              child: const Text('Send'),
            ),
          ],
        ),
      );
    }
  }
}