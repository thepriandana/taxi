import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CCoupon.dart';

import '../element/ItemCouponWidget.dart';

class Coupon extends StatelessWidget{
  final CouponController _con = Get.put(CouponController());
  final isComplete = false.obs;
  int selectedIndex = 0;
  String code = "";
  String total = "";
  @override
  Widget build(BuildContext context) {
    _con.getPromo();
    return Scaffold(body: SafeArea(
        child: (_con.promo.isEmpty) ?
        CircularProgressIndicator()
            :
        Column(
          children: [
            Expanded(child: Column(children: [
              Text("Select promo"),
              Divider()
            ],), flex: 0),
            Expanded(child: ListView.builder(
                itemCount: _con.promo.length,
                itemBuilder: (context, index){
                  return InkWell(
                    onTap: (){
                        selectedIndex = index;
                        code = _con.promo.elementAt(selectedIndex).code!;
                    },
                    child: ItemCouponWidget(isSelected: selectedIndex == index, promo:_con.promo.elementAt(index)),
                  );
                }
            )),
            Expanded(child: Container(padding: EdgeInsets.only(left: 15, right: 15,  bottom: 10, top: 10),
                decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [BoxShadow(
                  color: Colors.grey.withOpacity(0.5), //color of shadow
                  spreadRadius: 5, //spread radius
                  blurRadius: 7, // blur radius
                  offset: Offset(0, 2))
            ]),
                  child: Row(
              children: [
                Expanded(child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("You save", style: TextStyle(color: Colors.black38),),
                    Text("Rp50,000", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                  ],
                ),),
                Expanded(flex:0, child:  ElevatedButton(onPressed: (){}, child: Text("Continue"))),
              ]
            )
            ), flex: 0),
          ],
        )
    ),
    );
  }
}