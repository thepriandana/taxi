import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controller/CUser.dart';

class Login extends StatelessWidget{
  final UserController _con = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Container(
              child:
              Center(
                  child: Padding(padding: EdgeInsets.only(left: 35, right: 35), child: Form(
                      key: _con.loginFormKey,
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(Icons.local_taxi, size: 50),
                            SizedBox(height: 15),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              onSaved: (input) => _con.user.update((val){ val?.email = input!;}),
                              validator: (input) => !input!.contains('@') ? "v_email".tr : null,
                              decoration: InputDecoration(
                                labelText: "email".tr,
                                labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                                contentPadding: EdgeInsets.all(12),
                                hintText: 'Please insert your email',
                                hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                                prefixIcon: Icon(Icons.alternate_email, color: Theme.of(context).colorScheme.secondary),
                              ),
                            ),
                            TextFormField(
                                keyboardType: TextInputType.text,
                                obscureText: _con.hidePassword.value,
                                onSaved: (input) => _con.user.update((val){val?.password = input!;}),
                                validator: (input) => input!.length < 3 ? "e_moreCharacters".trParams({'num':'3'}) : null,
                                decoration: InputDecoration(
                                  labelText: "password".tr,
                                  labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: '******',
                                  hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                                  prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).colorScheme.secondary),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                        _con.hidePassword(!_con.hidePassword.value);
                                    },
                                    color: Theme.of(context).focusColor,
                                    icon: Icon(_con.hidePassword.value ? Icons.visibility_off_outlined: Icons.visibility_outlined),
                                  ),
                                )
                            ),
                            SizedBox(height: 15),
                            ElevatedButton(onPressed: (){_con.login();}, child: Text("Login")),
                            SizedBox(height: 15),
                            TextButton(onPressed: (){
                              Get.back();
                              Get.toNamed("/register");
                            }, child: Text("Create new account")),
                            SizedBox(height: 3),
                            Row(children: [Expanded(child: Divider()), SizedBox(width: 8), Text("or"), SizedBox(width: 8), Expanded(child: Divider())],),
                            SizedBox(height: 3),
                            TextButton(onPressed: (){}, child: Text("Reset password"))
                          ]))
                  )
              )
          ),
        ));
  }
}