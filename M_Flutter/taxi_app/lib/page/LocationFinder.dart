import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CBooking.dart';
import 'package:taxi_app/controller/CMap.dart';
import 'package:latlong2/latlong.dart';
import 'package:taxi_app/element/AppButton.dart';
import 'package:taxi_app/model/CustomeLatLng.dart';

import '../model/PlaceX.dart';

class LocationFinder extends StatelessWidget {
  final MapsController mapsController = Get.find();
  final BookingController bookingController = Get.find();
  final isUsingPin = false.obs;
  final isOrigin = (Get.arguments == "origin").obs;
  final pinLocation = CustomeLatLng().obs;
  final origin = TextEditingController(text: "");
  final destination = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    pinLocation(CustomeLatLng(lat: mapsController.position.value.lat.toDouble(), lng:mapsController.position.value.lng.toDouble()));
    return WillPopScope(onWillPop: (){
      if(isUsingPin.value == false){
        //check
        return Future.value(true);
      }else{
        isUsingPin(false);
        return Future.value(false);
      }

    }, child:Scaffold(
      backgroundColor: Color(0xffd9d9d9),
      body: Stack(children: [
        Positioned(child:
        Obx(() => (isUsingPin.value == true) ? FlutterMap(
            options: MapOptions(
              onPositionChanged: (MapPosition position, hasGesture){
                pinLocation(CustomeLatLng(lat: position.center?.latitude.toDouble(), lng:position.center?.longitude.toDouble()));
              },
              center: checkCenter(),
              zoom: 16,
            ),
            nonRotatedChildren: [
              AttributionWidget.defaultWidget(
                source: 'Google',
                onSourceTapped: null,
              ),
            ],
            children: [
              TileLayer(
                urlTemplate: "https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga",
                userAgentPackageName: "GoogleX",
              ),
            ]) : SizedBox()
        )
        ),
        Column(
            mainAxisSize: MainAxisSize.min,
            children:[
              Obx(() => Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      (isUsingPin.value == true) ?
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.5), //color of shadow
                          spreadRadius: 5, //spread radius
                          blurRadius: 7, // blur radius
                          offset: Offset(0, 2)) :
                      BoxShadow()],
                    border: Border.all(color: Colors.transparent, width: 1),
                    borderRadius: BorderRadius.circular(7),
                  ),
                  padding: EdgeInsets.only(top: 0, bottom: 0, left: 5, right: 5),
                  margin: EdgeInsets.only(left: 10, right: 10, bottom: 0, top: 35),
                  child: Stack(children: [
                    Positioned(child: Image(
                      width: 100,
                      image: AssetImage("lib/asset/dot.png"),
                      // fit: BoxFit.contain,
                    ), left: -35, height: 55,top: 25),
                    Positioned(child: Icon(CupertinoIcons.circle_fill, size: 10, color: Colors.blue), top: 20, left: 10),
                    Positioned(child: Icon(CupertinoIcons.square_fill, size: 10), bottom: 20, left: 10),
                    Positioned(child: Container(padding: EdgeInsets.only(left: 30, right: 10), child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(child: Column(
                            children: <Widget>[
                              TextFormField(
                                onTap: (){
                                  isOrigin(true);
                                },
                                onChanged: (val){
                                  if(val.length == 0){
                                    mapsController.suggestionPlace([]);
                                    mapsController.suggestionPlace.add(PlacesX()); //add first
                                  }
                                mapsController.updateSuggestions(val);
                              },autofocus: (isOrigin.value) ? true : false,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                  hintText: (bookingController.tmpBooking.value.startLocName == "") ? 'Current Location' : bookingController.tmpBooking.value.startLocName),
                                readOnly: false,
                                controller: origin,
                              ),
                              Divider(height: 1,),
                              TextFormField(
                                onTap: (){
                                  isOrigin(false);
                                },
                                onChanged: (val){
                                  if(val.length == 0){
                                    mapsController.suggestionPlace([]);
                                    mapsController.suggestionPlace.add(PlacesX());
                                  }
                                  mapsController.updateSuggestions(val);
                                  }, autofocus: (!isOrigin.value) ? true : false,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: (bookingController.tmpBooking.value.endLocName == "") ? 'Where to go?' : bookingController.tmpBooking.value.endLocName,
                                ),
                                readOnly: false,
                                  controller: destination
                              ),
                            ], mainAxisSize: MainAxisSize.min,
                          ))
                        ]),
                    ))
                  ])
              )),
              Obx(()=> (isUsingPin.value == true) ? SizedBox() : Flexible(child: Container(
                margin: EdgeInsets.only(left:10, right:10),
                child: ListView.builder(
                        itemCount: mapsController.suggestionPlace.length,
                        itemBuilder: (context, index){
                          return InkWell(
                              onTap: (){
                                if(index == 0){
                                  mapsController.selectLocation(isOrigin: isOrigin.value, latLng: mapsController.position.value);
                                  origin.clear();
                                }else{
                                  mapsController.selectLocation(isOrigin: isOrigin.value, placeID: mapsController.suggestionPlace.elementAt(index).placeId);
                                  destination.clear();
                                }
                                FocusScope.of(context).unfocus();
                              },
                              child: Container(
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    // border: Border(top: BorderSide(color: Colors.grey.shade200, width: 1)),
                                    borderRadius: BorderRadius.circular(3),
                                    color: Colors.white,
                                  ),
                                  margin: EdgeInsets.only(top: .8),
                                  child: Row(children: [
                                    (index == 0) ? CircleAvatar(child: Icon(CupertinoIcons.location_solid, color: Colors.white)) : CircleAvatar(child: Icon(CupertinoIcons.location, color: Colors.white), backgroundColor: Colors.black54,),
                                    SizedBox(width: 10),
                                    Flexible(child: Text((index == 0) ? "Use current location": mapsController.suggestionPlace.elementAt(index).name.toString(),
                                      overflow: TextOverflow.ellipsis))
                                  ])
                              )
                          );
                        }),
                      )
                    ),
              ),
              Obx(() => (isUsingPin.value == true) ?  SizedBox(): InkWell(child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3),
                  ),
                  padding: EdgeInsets.all(10),
                  child: Row(children: [
                    CircleAvatar(child: Icon(CupertinoIcons.location_solid, color: Colors.white), backgroundColor: Colors.grey,),
                    SizedBox(width: 10), Text("Set location on map")])
              ), onTap: (){
                FocusScope.of(context).unfocus();
                isUsingPin(true);
              })
              ),
            ]),

        Obx(() => (isUsingPin.value == true) ? Positioned(child: Icon(CupertinoIcons.map_pin_ellipse, size: 30,), left: Get.width/2-15, top: Get.height/2-22,): SizedBox()),
        Obx(() => (isUsingPin.value == true) ? Align(alignment: Alignment.bottomCenter,
          child: Container(child: AppButton(text: "Select", textAlign: TextAlign.center, expanded: true, onPressed: updateUsingPin,)),
        ) : SizedBox())
      ]),
    )
    );
  }
  updateUsingPin(){
    mapsController.selectLocation(isOrigin: isOrigin.value, latLng: mapsController.position.value).then((_){
      isUsingPin(false);
    }).onError((error, stackTrace){isUsingPin(false);});
  }
  LatLng? checkCenter(){
    LatLng tmp = LatLng(mapsController.position.value.lat.toDouble(), mapsController.position.value.lng.toDouble());
    CustomeLatLng ck;
    if(isOrigin.value){
      ck =  bookingController.tmpBooking.value.startLoc.value;
    }else{
      ck =  bookingController.tmpBooking.value.endLoc.value;
    }
    if(ck.lat.value != 0 && ck.lng.value != 0) return LatLng(ck.lat.value, ck.lng.value);
    return tmp;
  }
}
