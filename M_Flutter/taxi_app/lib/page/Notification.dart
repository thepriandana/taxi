import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CNotification.dart';

class Notifications extends StatelessWidget{
  final NotificationController _con = Get.put(NotificationController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(
      child: GetBuilder<NotificationController>(
        initState: (_) => NotificationController().getNotification(),
        builder: (_) => (_con.notification.length == 0) ? Container(
            child:Center(
              child: Column(mainAxisSize: MainAxisSize.min,
                children: [
                  Image(image: AssetImage("lib/asset/empty.png"), height: 150,),
                  Center(child: Text("emptyNotification".tr, style: TextStyle(color: Colors.black38),)),
                  SizedBox(height: 200,)
                ],
              ),)
        ):
        ListView.builder(itemBuilder: (context, index){
          return Container(padding: EdgeInsets.all(10),
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey.shade200))),
              child:
              Row(children: [
                CircleAvatar(child: Icon(Icons.notifications)),
                SizedBox(width: 10,),
                Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                  Text("${_con.notification.elementAt(index).title}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                  Text("${_con.notification.elementAt(index).subtitle}")
                ])),
              ],)
          );
        }, itemCount: _con.notification.length,
        ),
      ),
    ));
  }
}