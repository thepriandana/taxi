import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CBooking.dart';
import 'package:taxi_app/element/AppButton.dart';
import 'package:taxi_app/element/LoadingWidget.dart';

class FindingDriver extends StatelessWidget{
  final BookingController _bookingController = Get.find();

  //make them unable to use back key first
  @override
  Widget build(BuildContext context) {
    hidesec();
    // ever(_bookingController.statusFinder, (_) => Get.back());
    return WillPopScope(onWillPop: () => Future.value(false), child: Scaffold(
      body: Container(
        color: Color(0xff4221ee),
          child: Stack(
            children: [
              Align(alignment: Alignment.topCenter,  child: Padding(padding: EdgeInsets.only(top: 100),child:Text("Finding Driver", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)))),
              Align(alignment: Alignment.center, child: Icon(Icons.local_taxi, size: 50, color: Colors.white)),
              Positioned(top: (Get.height/2)-(Get.width/4), left: (Get.width/2)-(Get.width/4),child: SizedBox(
                child: CircularProgressIndicator(color: Colors.white,),
                height: Get.width/2,
                width: Get.width/2,
              )),
              Align(alignment: Alignment.bottomCenter, child: Padding(padding: EdgeInsets.all(15), child:
                  Flex(direction: Axis.horizontal,children:[
                    Flexible(child: AppButton(text: "cancel".tr, expanded: true, onPressed: confirmCancelation, textAlign: TextAlign.center))
                  ])
              ))
        ],
      )),
    )
    );
  }
  void hidesec(){
      Timer.periodic(const Duration(seconds: 5), (timer) async {
        timer.cancel();
        _bookingController.hideFD(true);
        _bookingController.tmpBooking.update((val) {val?.status = 0 ;});
        Get.offNamed("/pages/0");
      });
  }
  void confirmCancelation(){
    bool isConfirm = false;
    Get.dialog(AlertDialog(
              title: const Text('Cancel Confirmation'),
              content: (isConfirm == false) ? Text('Do you want to process the cancellation?') : Center(child: LoadingWidget(height: 150)),
              actions: [
                TextButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text('Cancel'),
                ),
                TextButton(
                  onPressed: () {
                    Get.back(closeOverlays: true);
                    // isConfirm = true;
                    // _bookingController.cancelLastBooking().then((value) => Get.back());
                  },
                  child: const Text('Confirm'),
                ),
              ],
            )
    );
  }
}