import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hive/hive.dart';
import 'package:taxi_app/ErrorVici.dart';
import 'package:taxi_app/controller/CBooking.dart';
import 'package:taxi_app/controller/CMap.dart';
import 'package:taxi_app/model/CustomeLatLng.dart';

import '../controller/CUser.dart';

class Splash extends StatefulWidget{
  _Splash createState() => _Splash();
}
class _Splash extends State<Splash>{
  final UserController uc = Get.put(UserController(), permanent: true);
  final BookingController bc = Get.put(BookingController(), permanent: true);
  final MapsController _mapsController = Get.put(MapsController(), permanent: true);
  @override
  void initState() {
    super.initState();
    mustPass();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied || permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {

        Get.snackbar("e_".tr, "e_locationReq".tr);
        _determinePosition();
        return Future.error('e_locationDenied'.tr);
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      await Geolocator.openAppSettings();
      Get.snackbar("", "e_locationReq".tr);
      _determinePosition();
      return Future.error('e_locationDeniedPerm'.tr);
    }

    // When we reach here, permissions are granted an d we can
    // continue accessing the position of the device.
    Position tmp = await Geolocator.getCurrentPosition();
    CustomeLatLng last = CustomeLatLng(lat: tmp.latitude, lng: tmp.longitude);
    var box = await Hive.openBox('taxi_app');
    // await box.put("last_location", jsonEncode(last).toString());
    return tmp;
  }
  void mustPass(){
    bc.refreshPaymentMethod().then((_){
      bc.refreshRideClassMethod().then((_){
        uc.checkLogin().then((value){
          if(value != false){
            uc.checkTokenSplash().then((_){
              _determinePosition().then((value){
              Get.toNamed("/pages/0");
              });
            });
          }else{
            _determinePosition().then((value){
              Get.toNamed("/pages/0");
            });
          }
        });
      }).onError((error, stackTrace) {
        ErrorVici().splashTimeout(mustPass);
      });
    }).onError((error, stackTrace) {
      ErrorVici().splashTimeout(mustPass);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xff4221ee),
        child: Center(child: Column(
          children: [
            Expanded(child: Column(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 15),
                Icon(Icons.local_taxi, size: 50, color: Colors.white),
                SizedBox(height: 10),
                Text("app_name".tr, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25))
              ])),
            Text("C 2022", style: TextStyle(color: Colors.white)),
            SizedBox(height: 16.8)
          ],
        ),
      )),
    );
  }
}