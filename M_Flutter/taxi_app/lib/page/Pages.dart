import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:taxi_app/element/LeftDrawerWidget.dart';
import 'package:taxi_app/model/CustomeLatLng.dart';
import 'package:taxi_app/page/History.dart';
import 'package:taxi_app/page/Notification.dart';
import 'package:taxi_app/page/Home.dart';
import 'package:taxi_app/page/Coupon.dart';
import 'package:taxi_app/page/SignUp.dart';

import '../controller/CUser.dart';

// ignore: must_be_immutable
class Pages extends StatefulWidget{
  dynamic currentTab;
  late Widget currentPage = SignUp() ;
  Pages(){
    currentTab = int.parse(Get.parameters['tab']!);
  }
  @override
  State createState() {
    return _Page();
  }
}
class _Page extends State<Pages>{
  final history = <int>[].obs;
  final UserController _con = Get.find();
  void _selectTab(int tabItem) async{
    Position? position = await Geolocator.getLastKnownPosition();
    print(position);
    setState(() {
      history.add(tabItem);
      widget.currentTab = tabItem;
      switch(widget.currentTab){
        case 0:
          widget.currentPage = Home(lastLocation: CustomeLatLng(lat: position!.latitude, lng: position.longitude));
          break;
        case 1:
          widget.currentPage = Notifications();
          break;
        case 2:
          widget.currentPage = History();
          break;
        case 3:
          widget.currentPage = Coupon();
          break;
        case 99:
          // Navigator.of(context).pushReplacementNamed("/Pages", arguments: 0);
          break;
      }
    });
  }
  @override
  void initState() {
    super.initState();
    _selectTab(widget.currentTab);
  }

  @override
  void didUpdateWidget(Pages oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(onWillPop: onWillPop, child: Scaffold(
      drawer: LeftDrawerWidget(),
      body: widget.currentPage,
      // body: Home(),
    ));

  }

  late DateTime currentBackPressTime;
  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if( history.length >1){
      print("ALL PAGE ${history.length}");
    }
    return Future.value(false);
    //   widget.pagesHistory.removeLast();
    //   _selectTab(widget.pagesHistory.last);
    //   widget.pagesHistory.removeLast();
    //   return Future.value(false);
    // }else if (now.difference(currentBackPressTime) > Duration(seconds: 2)) {
    //   currentBackPressTime = now;
    //   Get.snackbar("", "tapToLeave".tr);
    //   return Future.value(false);
    // }
    // SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    // return Future.value(true);
  }
}
