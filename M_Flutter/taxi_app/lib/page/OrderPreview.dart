import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:taxi_app/controller/CBooking.dart';
import 'package:taxi_app/controller/CUser.dart';
import 'package:taxi_app/element/AppButton.dart';

import '../model/Booking.dart';

class OrderPreview extends StatelessWidget{
  final UserController uc = Get.find();
  final BookingController bc = Get.find();

  final currencyFormatter = NumberFormat('#,##0.00', 'ID');
  @override
  Widget build(BuildContext context) {
    return Scaffold(body:
    CustomScrollView(
        slivers: [
          SliverToBoxAdapter(child: Container(margin: EdgeInsets.only(left: 25, right: 25, bottom: 25, top: 35),
              child: Obx(() => Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(onTap: () => Get.back(), child:Icon(Icons.arrow_back_sharp, size: 30,)),
                    SizedBox(height: 11),
                    Text("TOTAL PRICE"),
                    Text("${currencyFormatter.format((bc.distance.value)*int.parse(bc.rideClass.first.price.toString()).ceilToDouble())}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 50)),
                    Row(children: [
                      Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                        Text("Promo"),
                        Text("ABC ${bc.tmpBooking.value.promoCode}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25)),
                      ])),
                      TextButton(onPressed: (){Get.toNamed("/coupon");}, child: Text("Change"))
                    ]),
                    SizedBox(height: 10),
                    Row(children: [
                      Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                        Text("Payment"),
                        Text("Cash ${bc.tmpBooking.value.promoCode}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25)),
                      ])),
                      TextButton(onPressed: (){}, child: Text("Change"))
                    ]),
                    Divider(),
                    Wrap(
                        children: [
                          Row(children: [
                            Icon(Icons.circle),
                            SizedBox(width: 15),
                            Expanded(
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children:[
                                      Text("ORIGIN", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w500)),
                                      Text("${bc.tmpBooking.value.startLocName}", style: TextStyle(fontSize: 20))
                                    ])
                            )
                          ])
                        ]),
                    SizedBox(height: 11),
                    Wrap(
                        children: [
                          Row(children: [
                            Icon(Icons.circle),
                            SizedBox(width: 15),
                            Expanded(
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children:[
                                      Text("DESTINATION", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w500)),
                                      Text("${bc.tmpBooking.value.endLocName.toString()}", style: TextStyle(fontSize: 20))
                                    ])
                            )
                          ])
                        ]),
                    Divider(),
                    Text("Note"),
                    SizedBox(height: 10),
                    Container(decoration: BoxDecoration(color: Colors.black54.withAlpha(11), borderRadius: BorderRadius.all(Radius.circular(10))),
                        padding: EdgeInsets.all(10),
                        child: TextField(maxLines: 3,
                            decoration: InputDecoration.collapsed(hintText: "${bc.tmpBooking.value.note??"empty"}"),
                            keyboardType: TextInputType.text,
                            onChanged: (input){bc.tmpBooking.update((val){ val?.note = input;});
                          },
                        )),
                  ])
          )),
          ),
        ]), bottomNavigationBar: Padding(padding: EdgeInsets.only(bottom: 15, left: 15, right: 15),
      child: Row(
          children: [
            InkWell(onTap: (){
              Get.dialog(AlertDialog(
                title: const Text('Clear Booking'),
                content: const Text('Are you sure you want to reset the booking details?'),
                actions: [
                  TextButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: const Text('Cancel'),
                  ),
                  TextButton(
                    onPressed: () {
                      bc.tmpBooking(Booking());
                      Get.back();
                      Get.back();
                    },
                    child: const Text('Reset'),
                  ),
                ],
              ),
              );
            }, child: Icon(CupertinoIcons.trash)),
            Expanded(child: SizedBox()),
            AppButton(text: "Find Driver", expanded: false, icon: Icon(Icons.chevron_right), margin: EdgeInsets.only(bottom: 5),
              padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),onPressed: bc.sendBookingRequest,
            )
          ]),
    )
    );
  }
}