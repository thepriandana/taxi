class UserNotification{
  String? id;
  String? title;
  String? subtitle;
  UserNotification({this.id = '', this.title = '', this.subtitle = ''});
  UserNotification.fromJSON(Map<String, dynamic> map){
    try{
      id = map["id"].toString();
      title = map["title"].toString();
      subtitle = map["subtitle"].toString();
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["title"] = title;
    map["subtitle"] = subtitle;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}