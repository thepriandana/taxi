class Promo{
  String? id;
  String? title;
  String? date;
  String? code;
  double? discount;
  Promo({this.id = '', this.title = '', this.date = '', this.code = '', this.discount = 0});
  Promo.fromJSON(Map<String, dynamic> map){
    try{
      id = map["id"].toString();
      title = map["title"].toString();
      date = map["date"].toString();
      code = map["code"].toString();
      discount = double.parse(map["discount"].toString());
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["title"] = title;
    map["date"] = date;
    map["code"] = code;
    map["discount"] = discount;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}