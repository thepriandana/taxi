class User{
  String? id;
  String? name;
  String? email;
  String? password;
  String? jwtToken;
  User({this.id = '', this.name = '', this.email = '', this.password = '', this.jwtToken = ''});
  User.fromJSON(Map<String, dynamic> json){
    try{
      id = json['id'].toString();
      name = json['name'].toString();
      email = json['email'].toString();
      jwtToken = json['jwtToken'].toString();
    } catch(e){
      print(e);
    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["email"] = email;
    map["password"] = password;
    if(jwtToken != null) map["jwtToken"] = jwtToken;
    return map;
  }

}