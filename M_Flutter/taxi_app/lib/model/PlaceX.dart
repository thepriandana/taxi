class PlacesX {
  String? placeId;
  String? name;
  PlacesX({this.name = '', this.placeId = ''});
  PlacesX.fromJSON(Map<String, dynamic> map){
    try {
      placeId = map["place_id"].toString();
      name = map["description"].toString();
    } catch (e) {

    }
  }
  PlacesX.fromX(String p, String n){
    placeId = p;
    name = n;
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["place_id"] = placeId;
    map["description"] = name;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}
