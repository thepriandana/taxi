
class Car{
   String? id;
   String? idRideClass;
   double? lat;
   double? lng;
   int? duration; //duration from the car to the user
   double? rotation;
   Car({this.id = '', this.idRideClass = '', this.lat = 0, this.lng = 0, this.duration = 0, this.rotation = 0});
   Car.fromJSON(Map<String, dynamic> map){
     try{
       id = map["id"] == null ? map["id"] : "";
       idRideClass = map["idRideClass"] == null ? map["idRideClass"] : "";
       lat = map["lat"];
       lng = map["lng"];
       duration = map["duration"];
       rotation = map["rotation"];
     } catch(e){

     }
   }
   Map toMap(){
     var map = new Map<String, dynamic>();
     map["id"] = id;
     map["idRideClass"] = idRideClass;
     map["lat"] = lat;
     map["lng"] = lng;
     map["duration"] = duration;
     map["rotation"] = rotation;
     return map;
   }
  @override
  String toString() {
    return toMap().toString();
  }
}