class Payment{
  String? id;
  String? name;
  String? url;
  int? fee;
  Payment({this.id='', this.name='', this.url = '', this.fee = 0});
  Payment.fromJSON(Map<String, dynamic> map){
    try{
      id = map["id"].toString();
      name = map["name"].toString();
      url = map["url"].toString();
      fee = map["fee"];
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id ?? "";
    map["name"] = name ?? "";
    map["url"] = url??"";
    map["fee"] = fee??0;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}