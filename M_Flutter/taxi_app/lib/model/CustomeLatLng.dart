import 'package:get/get.dart';

class CustomeLatLng{
  final lat=0.0.obs;
  final lng=0.0.obs;
  CustomeLatLng({double? lat, double? lng}){
    this.lat(lat);
    this.lng(lng);
  }

  CustomeLatLng.fromJSON(Map<String, dynamic> map){
    try{
      lat(map["lat"]);
      lng(map["lng"]);
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["lat"] = lat.value;
    map["lng"] = lng.value;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}