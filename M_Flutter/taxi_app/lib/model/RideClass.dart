class RideClass{
  String? id;
  String? className;
  String? price; //price /km
  String? pax; //maximum pax on this ride class
  RideClass({this.id = '', this.className = '', this.price = '0', this.pax = '0'});
  RideClass.fromJSON(Map<String, dynamic> map){
    try{
      id = map["id"].toString();
      className = map["className"].toString();
      price = map["price"].toString();
      pax = map["pax"].toString();
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["class_name"] = className;
    map["price"] = price;
    map["pax"] = pax;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}