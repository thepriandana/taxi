
import 'package:get/get.dart';
import 'package:taxi_app/model/CustomeLatLng.dart';

import 'Car.dart';
import 'Payment.dart';

class Booking{
  String? id;
  String? driverName;
  String? driverID;
  int? status; //1 active, 2 cancel, 3 finish
  final startLoc = CustomeLatLng().obs;
  final endLoc = CustomeLatLng().obs;
  String? startLocName;
  String? endLocName;
  final startTime = DateTime(0).obs;
  final endTime = DateTime(0).obs;
  String? note;
  String? promoCode;
  String? payment;
  final driverCar = Car().obs;
  int? rating;
  String? rideClass;
  Booking({
    this.id = '', this.driverName = '', this.driverID = '', this.status = -1,
    this.startLocName = '', this.endLocName = '',
    this.note = '', this.promoCode = '', this.payment = '', this.rating = 0,
    this.rideClass = ''
  });
  Booking.fromJSON(Map<String, dynamic> map){
    try{
      status = map["status"];
      payment = map["payment"]["type"].toString();
      id = map["id"].toString();
      driverName = map["driverName"].toString();
      startLoc(CustomeLatLng(lat: double.parse(map["startLoc"]["lat"].toString()), lng: double.parse(map["startLoc"]["lng"].toString())));
      endLoc(CustomeLatLng(lat: double.parse(map["endLoc"]["lat"].toString()), lng: double.parse(map["endLoc"]["lng"].toString())));
      startLocName = map["startLocName"].toString();
      endLocName = map["endLocName"].toString();
      startTime(DateTime.parse(map["startTime"].toString()));
      endTime(DateTime.parse(map["endTime"].toString()));
      note = map["note"] ?? "";
      // promoCode = map["promoID"];
      // driverCar = Car.fromJSON(map["car"]);
      rating = map["rating"];

    } catch(e){

    }
  }
  Map<String, dynamic> toJson() {
    return {
      'startLoc': startLoc.value.toMap(),
      'endLoc': endLoc.value.toMap(),
      'startLocName': startLocName,
      'endLocName': endLocName,
      'note': note,
      'payment': payment
    };
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["startLoc"] = startLoc.value.toMap();
    map["endLoc"] = endLoc.value.toMap();
    map["startLocName"] = startLocName;
    map["endLocName"] = endLocName;
    map["note"] = note;
    map["payment"] = payment;
    map["rideClass"] = rideClass;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}