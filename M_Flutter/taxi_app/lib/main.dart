import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:taxi_app/page/Coupon.dart';
import 'package:taxi_app/page/FindingDriver.dart';
import 'package:taxi_app/page/History.dart';
import 'package:taxi_app/page/LocationFinder.dart';
import 'package:taxi_app/page/Login.dart';
import 'package:taxi_app/page/Notification.dart';
import 'package:taxi_app/page/OrderDetail.dart';
import 'package:taxi_app/page/OrderPreview.dart';
import 'package:taxi_app/page/Pages.dart';
import 'package:taxi_app/page/SignUp.dart';
import 'package:taxi_app/page/Splash.dart';

import 'Messages.dart';

Future<void> main() async{
  //no change, all clean code no blob <3
  WidgetsFlutterBinding.ensureInitialized();
  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;
  Hive.init(appDocPath);
  runApp(
      GetMaterialApp(
          translations: Messages(),
          locale: Locale('en', 'US'), // translations will be displayed in that locale
          fallbackLocale: Locale('en', 'US'),
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          getPages: [
            GetPage(name: '/', page: () => Splash(), transition: Transition.cupertino),
            GetPage(name: '/pages/:tab', page: () => Pages()),
            GetPage(name: '/history', page: () => History()),
            GetPage(name: '/coupon', page: () => Coupon()),
            GetPage(name: '/notification', page: () => Notifications(), transition: Transition.cupertino),
            GetPage(name: '/locationFinder', page: () => LocationFinder()),
            GetPage(name: '/findingDriver', page: () => FindingDriver()),
            GetPage(name: '/orderPreview', page: () => OrderPreview()),
            GetPage(name: '/orderDetail', page: () => OrderDetail()),
            GetPage(name: '/signIn', page: () => Login()),
            GetPage(name: '/register', page: () => SignUp()),
          ])
  );
}
