import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ErrorVici{
  void splashTimeout(Function _function){
    Get.dialog(AlertDialog(
              title: const Text('Request Timeout'),
              content: const Text('Please check your connection and retry again'),
              actions: [
                // TextButton(
                //   onPressed: () {
                //     Navigator.pop(context);
                //   },
                //   child: const Text('Exit'),
                // ),
                TextButton(
                  onPressed: () {
                    _function();
                    Get.back();
                  },
                  child: const Text('Retry'),
                ),
              ],
            )
    );
  }
}