import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:taxi_app/model/User.dart';

import '../GlobalSettings.dart';
class RUser{
  bool isLog = true;
  int timeout = 15;
  ValueNotifier<User> currentUser = new ValueNotifier(User());
  Future<Object> userDo(String CT, String url, [Object? obj, bool? setPreference]) async{
    try{
      Uri uri = Uri.parse("${GS.base_url}/user/$url");
      final client = new http.Client();
      var response;
      if(isLog) print("RUSER: $CT => $uri [$obj] ");
      if(CT == "post"){
        response = await client.post(
          uri,
          headers: {HttpHeaders.contentTypeHeader: 'application/json'},
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
      }else if(CT == "get"){
        response = await client.get(
          uri,
          headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        ).timeout(Duration(seconds: timeout));
      }else if(CT == "put"){
        response = await client.put(
          uri,
          headers: {HttpHeaders.contentTypeHeader: 'application/json'},
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
      }else if(CT == "del"){
        response = await client.delete(
          uri,
          headers: {HttpHeaders.contentTypeHeader: 'application/json'},
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
      }
      if(isLog) print(response.body);
      if (response.statusCode == 200) {
        if(setPreference == true) {
          var box = await Hive.openBox('taxi_app');
          await box.put("current_user", json.encode(response.body));
        }
        return response.body;
      } else {
        throw new Exception(response.body);
      }
    } on TimeoutException catch (e) {
      if(isLog == true) print('Timeout Error: $e');
      // Fluttertoast.showToast(msg: S.errno_timeout,);
      throw new Exception(e);
    } on SocketException catch (e) {
      if(isLog == true) print('Socket Error: $e');
      throw new Exception(e);
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e');
      throw new Exception(e);
    }
  }

  Future<String> sendJWTCURL(String url, [Object? obj]) async{
    try {
      Uri uri = Uri.parse("$url");
      String token = await getTokenJWT();
      final client = new http.Client();
      Map<String, String> requestHeaders = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': "Bearer "+token.trim().replaceAll("\ufffd", "")
      };
      if(isLog == true) print(uri.toString());
      var response;
      if(obj!=null){
        response = await client.post(
          uri,
          headers: requestHeaders,
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
        if(isLog == true) print(json.encode(obj));
      }else{
        response = await client.get(
          uri,
          headers: requestHeaders,
        ).timeout(Duration(seconds: timeout));
      }
      if(isLog == true) print(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return response.body;
      }
    } on TimeoutException catch (e) {
      if(isLog == true) print('Timeout Error: $e');
      // Fluttertoast.showToast(msg: S.errno_timeout);
      throw new Exception(e);
    } on SocketException catch (e) {
      if(isLog == true) print('Socket Error: $e');
      throw new Exception(e);
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e');
      throw new Exception(e);
    }
  }
  Future<String> sendCURL(String url, [Object? obj]) async{
    try{
      Uri uri = Uri.parse("$url");
      final client = new http.Client();
      Map<String, String> requestHeaders = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
      if(isLog == true) print(uri.toString());
      var response;
      if(obj!=null){
        response = await client.post(
          uri,
          headers: requestHeaders,
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
        if(isLog == true) print(json.encode(obj));
      }else{
        response = await client.get(
          uri,
          headers: requestHeaders,
        ).timeout(Duration(seconds: timeout));
      }
      if(isLog == true) print(response.body);
      if (response.statusCode == 200) {
        if(isLog == true) print("200 Body");
        return response.body;
      } else {
        if(isLog == true) print("${response.statusCode} Body");
        return response.body;
      }
    } on TimeoutException catch (e) {
      if(isLog == true) print('Timeout Error: $e');
      // Fluttertoast.showToast(msg: S.errno_timeout);
      throw new Exception(e);
    } on SocketException catch (e) {
      if(isLog == true) print('Socket Error: $e');
      throw new Exception(e);
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e');
      throw new Exception(e);
    }
  }
  Future<String> getTokenJWT() async{
    var box = await Hive.openBox('taxi_app');
    String j= await box.get("current_user");
    return  json.decode(j)["message"].toString();
  }
  Future<String> getUserID() async{
    String j = await getTokenJWT();
    String token = j.split(".")[1];
    return  token;
  }
}
