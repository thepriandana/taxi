import 'package:flutter/material.dart';
import 'package:taxi_app/model/Promo.dart';

class ItemCouponWidget extends StatelessWidget {
  ItemCouponWidget({required this.isSelected, required this.promo});
  late bool isSelected;
  late Promo promo;
  @override
  Widget build(BuildContext context) {
    return Container(
        child:Padding(padding: EdgeInsets.only(top: 5 , left: 7, right: 7), child: Card(
        child: Container(decoration: BoxDecoration(
            border: Border.all(color: ((isSelected) ? Colors.green : Colors.grey.shade200), width: 1),
            borderRadius: BorderRadius.all(Radius.circular(5)),
          color: ((isSelected) ? Colors.green.withOpacity(0.1) : Colors.white),
        ),
            child: Row(
          children: [
            // Expanded(flex: 1, child: Icon(Icons.discount)),
            Expanded(flex: 8,
                child:
                Padding(padding: EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${promo.title}", style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(height: 5),
                    Text("${promo.date}", style: TextStyle(color: Colors.black54, fontSize: 13)),
                    SizedBox(height: 5),
                    Container(child: Padding(padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5), child: Text("${promo.code}", style: TextStyle(color: Colors.white))), decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(5)),),
                  ],
                ))
            )
          ],)
        ),
      ))
    );
  }
}