import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/element/AppButton.dart';
import 'package:taxi_app/model/Booking.dart';

import '../controller/CBooking.dart';
import 'LoadingWidget.dart';

class BottomOrderDetailWidget extends StatelessWidget{
  final BookingController bookingController = Get.find();
  @override
  Widget build(BuildContext context) {
    Booking booking = bookingController.tmpBooking.value;
    return DraggableScrollableSheet(
      initialChildSize: 0.16,
      minChildSize: 0.16,
      maxChildSize: 0.8,
      builder: (BuildContext c, ScrollController s){
        return Container(
            decoration: BoxDecoration(color:Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
            margin: EdgeInsets.only(left: 7, right: 7),
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
            child: CustomScrollView(
                controller: s,
                slivers: <Widget>[
                  SliverList(delegate:
                  SliverChildListDelegate([
                    Padding(padding: EdgeInsets.only(left:5, right: 5, top: 20),
                      child:
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Obx(()=> (bookingController.tmpBooking.value.status == 0) ?
                            Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisSize: MainAxisSize.min,children: [
                              Padding(padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),child: Text("requestDriver".tr, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
                              AppButton(text: "cancel".tr, expanded: true, textAlign: TextAlign.center, onPressed: confirmCancelation)
                            ]) :
                            Container(padding: EdgeInsets.only(left: 10), child: Row(children: [
                              Expanded(child: Text("", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),)),
                              IconButton(onPressed: (){}, icon: Icon(Icons.call)),
                              IconButton(onPressed: (){}, icon: Icon(Icons.message)),
                            ]))),
                            Card(child:
                            Padding(padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                                child:
                                Column(
                                  children: [
                                    Row(
                                        children: [
                                          Icon(Icons.arrow_upward),
                                          SizedBox(width: 10),
                                          Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Text("pickUp".tr, style: TextStyle(fontSize:13, color: Colors.black38, fontWeight: FontWeight.bold)),
                                                Text("${booking.startLocName}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17))
                                              ]),
                                        ]
                                    ),
                                    SizedBox(height: 15),
                                    Row(
                                        children: [
                                          Icon(Icons.stop_circle_rounded),
                                          SizedBox(width: 10),
                                          Container(
                                            width:MediaQuery.of(context).size.width*0.7  ,
                                            child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Text("dropOff".tr, style: TextStyle(fontSize:13, color: Colors.black38, fontWeight: FontWeight.bold)),
                                                  Text("${booking.endLocName}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17))
                                                ]),)
                                        ]
                                    ),
                                  ],))
                            ),
                            Card(child: Padding(
                                padding: EdgeInsets.all(10),
                                child:Row(children: [
                                  Icon(Icons.account_balance_wallet_outlined),
                                  SizedBox(width: 5,),
                                  Expanded(child: Text("${booking.payment}", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))),
                                  Text("Rp12.000", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
                                ],
                                )
                            )),
                            Obx(()=> (bookingController.tmpBooking.value.status == 0) ? SizedBox():
                            Container(child: Column(children: [
                              Text("Driver")
                            ]))
                            ),
                          ]),
                    )
                  ])
                  )
                ])
        );
      },
    );
  }
  void confirmCancelation(){
    bool isConfirm = false;
    Get.dialog(AlertDialog(
      title: const Text('Cancel Confirmation'),
      content: (isConfirm == false) ? Text('Do you want to process the cancellation?') : Center(child: LoadingWidget(height: 150)),
      actions: [
        TextButton(
          onPressed: () {
            Get.back();
          },
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () {
            isConfirm = true;
            bookingController.tmpBooking.update((val) => val?.status = -1);
            Get.back();
            // _bookingController.cancelLastBooking().then((value) => Get.back());
          },
          child: const Text('Confirm'),
        ),
      ],
    )
    );
  }
}

