import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/CBooking.dart';
import '../model/Booking.dart';
import '../model/RideClass.dart';

class CarOption extends StatelessWidget {
  CarOption({required this.rideClass, this.isSelected = false});
  late RideClass rideClass;
  late bool isSelected;
  final BookingController bookingController = Get.find();
  int priceme(){
    if(bookingController.distance.value > 0){
      return (bookingController.distance.value*int.parse(rideClass.price.toString())/1000).ceil();
    }
    return (int.parse(rideClass.price.toString())/1000).ceil();
  }
  @override
  Widget build(BuildContext context) {
    Booking booking = bookingController.tmpBooking.value;

    return Container(padding: EdgeInsets.all(10),
        child: Obx(()=>  Row(
          children: [
            (isSelected) ? Padding(padding: EdgeInsets.only(right: 5),
                child: Icon(CupertinoIcons.circle_fill, size: 15, color: Colors.blue)) : SizedBox(width: 20),
            Column(crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${rideClass.className}", style: TextStyle(fontWeight: FontWeight.bold,  fontSize: 17)),
                  Text("1-${rideClass.pax} pax", style: TextStyle(fontSize: 12)),]),
            Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("IDR ${priceme()}k", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                  (bookingController.durationText.value.isNotEmpty) ? Text("~${bookingController.durationText}", style: TextStyle(fontSize: 12)) : SizedBox()
                ])
            ),
          ],
        ))
    );
  }
}
