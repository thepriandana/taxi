
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/CBooking.dart';

class CustomeMarkers extends StatelessWidget {
  CustomeMarkers({this.isDestination = false, this.placeName = "", this.minutes = 1});
  bool isDestination;
  String placeName;

  final BookingController bookingController = Get.find();
  double minutes;
  @override
  Widget build(BuildContext context) {
    if(bookingController.duration.value!=0){
      minutes = bookingController.duration.value;
    }
    return Stack(fit: StackFit.passthrough,
        children: [
          Align(alignment: Alignment.topLeft,
              child: Container(
                  margin: EdgeInsets.only(left: 290),
                  decoration: BoxDecoration(color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      boxShadow: [BoxShadow(
                          color: Colors.grey.withOpacity(0.5), //color of shadow
                          spreadRadius: 3, //spread radius
                          blurRadius: 2, // blur radius
                          offset: Offset(0, 2))]),
                  child: Row(mainAxisSize: MainAxisSize.min,
                      children: [
                        (isDestination) ? SizedBox(): Container(
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5))),
                            padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                            child: Column(mainAxisSize: MainAxisSize.min,children: [
                              Text("${minutes.ceil()}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 13)),
                              Text("mins", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w100, fontSize: 10))
                            ])
                        ),
                        Padding(padding: EdgeInsets.all((isDestination) ? 10: 5),child: 
                          Text("${(placeName.length>20) ? placeName.substring(0, 20)+"...": placeName}", style: TextStyle(fontWeight: FontWeight.bold))
                        ),
                        Icon(CupertinoIcons.chevron_right)])
              )),
          Icon((isDestination) ?
            CupertinoIcons.arrowtriangle_down_square_fill : CupertinoIcons.arrowtriangle_down_circle_fill, size: 20,),
        ]);
  }
}

