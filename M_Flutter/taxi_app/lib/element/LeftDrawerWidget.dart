import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CBooking.dart';
import 'package:taxi_app/controller/CUser.dart';

import '../model/Booking.dart';


class LeftDrawerWidget  extends StatelessWidget{
  final UserController userController = Get.find();
  final BookingController bookingController = Get.find();
  @override
  Widget build(BuildContext context) {
    userController.checkLogin().then((value){
      if(value){
        userController.statusLogin(1);
      }
    });
    TextStyle textoStylu = TextStyle(fontSize: Get.height/39, fontWeight: FontWeight.bold, color: Colors.black87);
    return Drawer(
        backgroundColor: Colors.transparent,
        child: Container(
          padding: EdgeInsets.only(left: 0, right: 0, bottom: 30, top: 30),
          height: Get.height,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), topRight: Radius.circular(25))
          ),
          child:  IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // Obx(() => (userController.statusLogin.value == 1) ? SizedBox() : Wrap(
                  //     spacing: 5,
                  //     children: <Widget>[
                  //       MaterialButton(onPressed: (){ Get.toNamed("/signIn");},
                  //         elevation: 0,
                  //         color: Theme.of(context).colorScheme.secondary,
                  //         height: 40,
                  //         child: Wrap(
                  //           runAlignment: WrapAlignment.center,
                  //           crossAxisAlignment: WrapCrossAlignment.center,
                  //           spacing: 9,
                  //           children: [
                  //             Text("_signIn".tr, style: Theme.of(context).textTheme.subtitle2?.merge(TextStyle(color: Colors.white)))
                  //           ],
                  //         ),
                  //         shape: StadiumBorder(),
                  //       ),
                  //       MaterialButton(onPressed: (){ Get.toNamed("/register");},
                  //         elevation: 0,
                  //         color: Theme.of(context).colorScheme.secondary,
                  //         height: 40,
                  //         child: Wrap(
                  //           runAlignment: WrapAlignment.center,
                  //           crossAxisAlignment: WrapCrossAlignment.center,
                  //           spacing: 9,
                  //           children: [
                  //             Text("_signUp".tr, style: Theme.of(context).textTheme.subtitle2?.merge(TextStyle(color: Colors.white)),)
                  //           ],
                  //         ),
                  //         shape: StadiumBorder(),
                  //       ),
                  //     ])
                  // ),
                  Wrap(children: [
                      ListTile(
                        onTap: () {
                          Scaffold.of(context).openEndDrawer();
                          Get.offNamed("/pages/0");
                        },
                        title: Text("_home".tr, style:textoStylu)
                      ),
                      ListTile(
                        onTap: () {
                          Scaffold.of(context).openEndDrawer();
                          (userController.statusLogin.value == 1) ? Get.offNamed("/pages/2") : sendLoginMessage();
                        },
                        title: Text("_history".tr, style:textoStylu)
                      ),
                      ListTile(
                        onTap: () {
                          Scaffold.of(context).openEndDrawer();
                          (userController.statusLogin.value == 1) ? Get.offNamed("/pages/1") : sendLoginMessage();
                        },
                        title: Text("_notification".tr, style: textoStylu)
                      ),
                      ListTile(
                        onTap: () {
                          Scaffold.of(context).openEndDrawer();
                          (userController.statusLogin.value == 1) ? Get.offNamed("/pages/3") : sendLoginMessage();
                        },
                        title: Text("_promo".tr,style:textoStylu)
                      ),
                      ListTile(
                        onTap: () { },
                        title: Text("_support".tr, style:textoStylu)
                      ),
                      ListTile(onTap: () {},
                        title: Text("_settings".tr, style:textoStylu)
                      )
                  ]),
                  ListTile(
                    onTap: () {
                      if(userController.statusLogin.value == 1){
                        //send bye bye message
                        userController.logout();
                        bookingController.tmpBooking(Booking());
                        bookingController.bookings([]);
                        bookingController.hideFD(false);
                        Get.back();
                        Get.offNamed("/pages/0");
                        Get.snackbar("", "bye".tr);
                      }else{
                        //go to login
                        sendLoginMessage();
                      }
                    },
                    title: Text("_signOut".tr, style:textoStylu.merge(TextStyle(color: Colors.grey)))
                  )
                ])
          )
          // IntrinsicHeight(
          //   child:
            // Column(
            //     mainAxisSize: MainAxisSize.max,
            //     crossAxisAlignment: CrossAxisAlignment.stretch,
            //     children: [
            //       Text("welcome".tr, style: Theme.of(context).textTheme.headline4?.merge(TextStyle(color: Theme.of(context).colorScheme.secondary))),
            //       SizedBox(height: 5),
            //       // (widget.isLogin) ? SizedBox() : Text(S.loginorsignup, style: Theme.of(context).textTheme.bodyText2),
            //       SizedBox(height: 5),
            //

        )
    );
  }
  void sendLoginMessage(){
    Get.rawSnackbar(messageText:
    Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(child: Text("e_notSign".tr, style: TextStyle(color: Colors.white))),
        Expanded(child: TextButton(onPressed: (){
          Get.toNamed("/signIn");
        }, child: Text("_signIn".tr, style: TextStyle(color: Colors.blue))), flex: 0)
      ],
    ),
        duration: Duration(seconds: 15),
        padding: EdgeInsets.only(left: 15, right: 15),
        margin: EdgeInsets.all(5));
  }
}