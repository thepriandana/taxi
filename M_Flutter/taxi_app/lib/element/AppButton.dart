import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppButton extends StatelessWidget {
  AppButton({required this.text, this.onPressed,
    this.icon, this.fontSize = 15, this.expanded = false,
    this.textAlign = TextAlign.start,
    this.padding = const EdgeInsets.all(15), this.margin = const EdgeInsets.only(bottom: 15, left: 10, right: 10),
    this.color = Colors.white, this.bgColor = const Color(0xff4545eb), this.borderRadius = const BorderRadius.all(Radius.circular(10))});
  Color? color;
  Color? bgColor;
  TextAlign? textAlign;
  BorderRadius? borderRadius;
  String text;
  VoidCallback? onPressed;
  Icon? icon;
  double? fontSize;
  bool? expanded;
  EdgeInsets? padding, margin;
  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: () {if(onPressed != null){ onPressed!();}},
          child: Container(
              padding: padding,
              margin: margin,
              decoration: BoxDecoration(
                color: bgColor,
                borderRadius: borderRadius,
              ),
              child: Flex(direction: Axis.horizontal, children: [
              (expanded! == true)?
              Flexible(child: Row(
                children: [
                  Expanded(child: Text("$text", style: TextStyle(fontWeight: FontWeight.bold, color: this.color, fontSize: fontSize), textAlign: textAlign)),
                  (icon == null)? SizedBox(): Icon(Icons.arrow_forward, color: Colors.white)
                ],
              )) :
              Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                alignment: WrapAlignment.spaceAround,
                children: [
                  Text("$text", style: TextStyle(fontWeight: FontWeight.bold, color: this.color, fontSize: fontSize), textAlign: textAlign),
                  (icon == null)? SizedBox(): Icon(Icons.arrow_forward, color: Colors.white,)
                ],
              )
              ])
    ));
  }
}
