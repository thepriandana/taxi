import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_app/controller/CBooking.dart';

import 'package:taxi_app/controller/CUser.dart';

import '../model/RideClass.dart';
import 'AppButton.dart';
import 'CarOption.dart';

class BottomHomeWidget extends StatelessWidget {
  final UserController userController = Get.find();
  final payment = "".obs;
  final BookingController bookingController = Get.find();
  final seeCar = false.obs;
  @override
  Widget build(BuildContext context) {
    // RideClass rc = bookingController.rideClass.firstWhere((element) => bookingController.tmpBooking.value.)
    return Container(
      width: Get.width,
        margin: const EdgeInsets.only(top: 5.0),
        decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(10)),
            color: Colors.white),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Obx(() => (seeCar.value) ? Container(height: Get.height/4, child: Column(children: [
              Flexible(child: TextButton(onPressed: (){
                seeCar(!seeCar.value);
              }, child: Text("Close")), flex: 0),
              Flexible(child: ListView.builder(
                  itemCount: 3,
                  itemBuilder: (context, index){
                    if(index == 1) return InkWell(child: CarOption(rideClass: bookingController.rideClass.first, isSelected: true,));
                    return InkWell(child: CarOption(rideClass: bookingController.rideClass.first));
                  })
                )
              ]))
                : Row(
                children: [
                  Flexible(child: CarOption(rideClass: bookingController.rideClass.first)),
                  Flexible(flex:0, child: InkWell(onTap: () => seeCar(!seeCar.value), child: Padding(padding: EdgeInsets.only(left: 5, right: 10),
                      child: Icon(CupertinoIcons.chevron_down)))
                  )
                ])
            ),
            Flexible(child: AppButton(text: "requestRide".tr,
              margin: EdgeInsets.all(15),
              icon: Icon(Icons.arrow_forward), padding: EdgeInsets.only(bottom: 10, top: 10, left: 15, right:15),
              expanded: true, onPressed: continueOrder,
            ))
          ],
        ));
  }
  continueOrder(){
    userController.checkLogin().then((value){
      if(value){
        Get.toNamed("/orderPreview");
      }else{
        Get.dialog(AlertDialog(
          title: Text('beforeContinue'.tr),
          content: Text('e_notSign'.tr),
          actions: [
            TextButton(
              onPressed: () {
                Get.back();
              },
              child: Text("later".tr),
            ),
            TextButton(
              onPressed: () {
                Get.toNamed("/signIn");
              },
              child: Text('Login'),
            ),
          ],
        ),
        );
      }
    });
    //print('called on tap');
  }
}
