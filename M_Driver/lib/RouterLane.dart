import 'package:flutter/material.dart';
import 'package:taxi_driver/pages/SignUp.dart';
import 'package:taxi_driver/pages/SignUpAdvances.dart';
import 'package:taxi_driver/pages/Splash.dart';
import 'package:taxi_driver/pages/Login.dart';

class RouterLane {
  static Route<dynamic> autoRoute(RouteSettings settings){
    final args = settings.arguments;
    switch(settings.name){
      case '/Splash':
        return MaterialPageRoute(builder: (_) => Splash());
      case '/Login':
        return MaterialPageRoute(builder: (_) => Login());
      case '/SignUp':
        return MaterialPageRoute(builder: (_) => SignUp());
      case '/SignUpCar':
        return MaterialPageRoute(builder: (_) => SignUpAdvances());
      // case '/notification-page':
      //   return MaterialPageRoute(builder: (context) {
      //     final ReceivedAction receivedAction = settings
      //         .arguments as ReceivedAction;
      //     return MyNotificationPage(receivedAction: receivedAction);
      //   });
      default:
        return MaterialPageRoute(builder: (_) => const Scaffold(body: SafeArea(child: Text("Default Route"))));
    }
  }
}