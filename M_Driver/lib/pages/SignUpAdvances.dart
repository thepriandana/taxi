import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:asuka/asuka.dart';
import 'package:taxi_driver/controller/CUser.dart';
import 'package:taxi_driver/pages/DialogEmergencyContact.dart';
import 'package:taxi_driver/pages/SUPickPhotoProfile.dart';
import 'package:taxi_driver/pages/SUPickSIM.dart';
import 'package:taxi_driver/pages/SUPickSTNK.dart';

import '../Strings.dart';
import '../controller/CSignUpCar.dart';
import 'SUPickCar.dart';

class SignUpAdvances extends StatefulWidget {
  @override
  _SignUpAdvancesState createState() => _SignUpAdvancesState();
}

class _SignUpAdvancesState extends StateMVC<SignUpAdvances> {
  _SignUpAdvancesState() : super(SignUpCarController()) {
    _con = controller as SignUpCarController;
  }
  late SignUpCarController _con;
  late UserController _ucon = UserController();
  late String _current;
  late int statusSIM =0 , statusSTNK=0, statusCAR=0, statusPP=0;
  late String? sim = "", pp="";
  late List<String> stnk, car;
  late bool acceptPrivacyPolicy = false;
  late List<CameraDescription> _cameras = [];
  late bool complete;
  @override
  void initState() {
    super.initState();
    checkCamera();
    doInit();
  }
  bool isInit = false;
  void checkCamera() async {
    _cameras = await availableCameras();
    print("camera available "+_cameras.toString());
  }
  void doInit() async{
   await _con.getSignupStatus().then((value){
      var obj = jsonDecode(value);
      statusSIM = obj["message"]["statusSIM"];
      statusSTNK = obj["message"]["statusSTNK"];
      statusPP = obj["message"]["statusPP"];
      statusCAR = obj["message"]["statusCAR"];
      sim = obj["message"]["SIM"];
      stnk = obj["message"]["STNK"].cast<String>();
      car = obj["message"]["CAR"].cast<String>();
      pp = obj["message"]["photoProfile"];
      complete = obj["message"]["complete"];
     });
    await _ucon.changeUsertoPreference();
    setState(() {
      isInit = true;
    });
  }
  Widget documentStatus(int status){
    if(status == 0) {
      return const Text("Empty", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange));
    }else if(status == 1){
      return const Text("Uploaded", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange));
    }else if(status == 2){
      return const Text("Accepted", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green));
    }else{
      return const Text("Denied", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange));
    }
  }
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        appBar: AppBar(title: const Text("Document Status", style: TextStyle(color: Colors.black)), backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          leading: InkWell(onTap: (){
            backLogout();
            Navigator.of(context).pop();
            }, child: Icon(CupertinoIcons.arrow_left))),
        resizeToAvoidBottomInset: false,
        body: SafeArea(
            child: Center(
                child: (!isInit) ? CircularProgressIndicator() :Column(
                    children: [
                      Card(margin: EdgeInsets.all(15), child: Row(children: [
                        Padding(padding: EdgeInsets.all(15), child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${_ucon.user.name}", style: const TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                              const SizedBox(height: 7),
                              Row(children: [
                                const Icon(Icons.email),
                                const SizedBox(width: 10),
                                Text("${_ucon.user.email}")
                              ],),
                              const SizedBox(height: 7),
                              Row(children: [
                                const Icon(Icons.phone),
                                const SizedBox(width: 10),
                                Text("+62 xxx xxxxxxx")
                              ],)
                            ]),)
                      ]),
                      ),
                      Card(
                          margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                          child: InkWell(onTap: (){
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DialogEmergencyContact())).then((val)=>doInit());
                          }, child: Padding(
                              padding: EdgeInsets.all(15),
                              child: Column(children: [
                                Row(
                                  children: [
                                    Expanded(child: Text("Emergency Contact Detail")),
                                    Icon(CupertinoIcons.arrow_right)
                                  ],
                                )
                              ]))
                          )),
                      Card(margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InkWell(child: Padding(padding: EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 5),child: Row(children: [
                                Icon(CupertinoIcons.photo),
                                const SizedBox(width: 10),
                                Expanded(child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Photo Profile", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                                      documentStatus(statusPP)
                                    ])),
                                Icon(CupertinoIcons.arrow_right)
                              ])), onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SUPickPhotoProfile(cameras: _cameras, url: pp, status: statusPP))).then((val)=>doInit());
                              }),
                              const Divider(),
                              InkWell(child: Padding(padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),child: Row(children: [
                                Icon(CupertinoIcons.photo),
                                const SizedBox(width: 10),
                                Expanded(child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("SIM", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                                      documentStatus(statusSIM)
                                    ])),
                                Icon(CupertinoIcons.arrow_right)
                              ])), onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SUPickSIM(cameras: _cameras, url: sim, status: statusSIM))).then((val)=>doInit());
                              }),
                              const Divider(),
                              InkWell(child: Padding(padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),child: Row(children: [
                                Icon(CupertinoIcons.photo),
                                const SizedBox(width: 10),
                                Expanded(child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("STNK", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                                      documentStatus(statusSTNK)
                                    ])),
                                Icon(CupertinoIcons.arrow_right)
                              ])), onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SUPickSTNK(cameras: _cameras, url: stnk, status: statusSTNK))).then((val)=>doInit());
                              }),
                              const Divider(),
                              InkWell(child: Padding(padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 15),child: Row(children: [
                                Icon(CupertinoIcons.photo),
                                const SizedBox(width: 10),
                                Expanded(child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      const Text("Car", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                                      documentStatus(statusCAR)
                                    ])),
                                Icon(CupertinoIcons.arrow_right)
                              ])), onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SUPickCar(cameras: _cameras, url: car, status: statusCAR))).then((val)=>doInit());
                              }),
                            ],
                          )

                      ),
                      (complete) ? Container(padding: EdgeInsets.all(15),child: Text("All document has been submitted.\nPlease wait for admin to verified your account", textAlign: TextAlign.center,)) : Center(child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Checkbox(value: acceptPrivacyPolicy, onChanged: (bool? v){
                            setState(() {
                              acceptPrivacyPolicy = v!;
                            });
                          }),
                          TextButton(child: Text("Agree privacy and policy"), onPressed: (){
                             setState(() {
                                acceptPrivacyPolicy = !acceptPrivacyPolicy;
                             });
                          },)
                        ],)),
                      (complete)? const SizedBox() : Center(child: ElevatedButton(child: Text("Submit"), onPressed: (){
                        if(acceptPrivacyPolicy){
                          _con.sendAccept();
                        }else{
                          Asuka.showDialog(
                              builder: (context) => AlertDialog(content: Material(
                                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                                  child: Container(
                                    child: Text("Please accept our privacy and policy to continue")
                                  )
                              ), actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text('Return'),
                                )
                              ],)
                          );
                        }
                      },))
                    ])
            )

        )
    );
  }
  void changedDropDownItem(String? _selected) {
    setState(() {
      _current = _selected!;
    });
  }
  void backLogout(){
    Asuka.showDialog(
      builder: (context) => AlertDialog(
        title: const Text('Confirmation'),
        content: const Text('Do you want to logout?'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
              _ucon.logout();
            },
            child: const Text('Log Out'),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel'),
          ),
        ],
      ),
    );
  }
}


// Text("Plate ID"),
// TextFormField(
// keyboardType: TextInputType.text,
// onSaved: (input) => {},
// ),
// SizedBox(height: 15,),
// Text("Type Car"),
// // DropdownButtonFormField(
// //     value: _current,
// // items: _dropDownMenuItems,
// // onChanged: changedDropDownItem),
// Text("Photo"),
// // GridView.count(
// //   crossAxisCount: 4,
// //   // children: fileListThumb,
// // ),
// ElevatedButton(child: Text("Pick Photo"), onPressed: (){
// _con.pickFiles();
// setState(() {
// // fileListThumb = _con.fileListThumb;
// });
// },)