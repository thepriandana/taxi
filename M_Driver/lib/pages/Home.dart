import 'dart:async';
import 'dart:convert';

import 'package:asuka/asuka.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:expandable/expandable.dart';
import 'package:hive/hive.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/controller/CBooking.dart';
import 'package:taxi_driver/pages/DialogJobOffer.dart';

import '../controller/CMap.dart';
import '../controller/CUser.dart';
import '../element/HomeJobBottom.dart';
import '../main.dart';
import '../model/Booking.dart';
import 'Profiles.dart';
class Dashboard extends StatefulWidget{
  Dashboard({Key? key, required this.lastLocation}) : super(key: key);
  late LatLng lastLocation;
  @override
  _Dashboard createState() => _Dashboard();
}
class _Dashboard extends StateMVC<Dashboard>{
  _Dashboard() : super(UserController()) {
    _con = controller as UserController;
  }
  late UserController _con;
  late bool isOffline = false;
  late bool isTakingJob = false;
  late MapsController _mapsController = new MapsController(context, widget.lastLocation, refreshMaps);
  late CameraPosition _cameraPosition = CameraPosition(target: widget.lastLocation);
  List<Marker> _marker = <Marker>[];
  static String lastID = "";
  static List<Polyline> _polyline = <Polyline>[];
  Completer<GoogleMapController> googleMapController = Completer();
  @override
  void initState() {
    lastID = "";
    print(widget.lastLocation);
    // TODO: implement initState
    super.initState();
    checkLastActiveJob();
    _mapsController.getLastLocation().then((value) =>  refreshMaps());
    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(48, 48)), 'lib/asset/car.png')
        .then((onValue) {
      setState(() {
        _mapsController.carIcon = onValue;
      });
    });

    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(48, 48)), 'lib/asset/pin_map_destination.png')
        .then((onValue) {
      _mapsController.destinationIcon = onValue;
      setState(() {
        _mapsController.destinationIcon = onValue;
      });
    });

    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(48, 48)), 'lib/asset/pin_map_origin.png')
        .then((onValue) {
      _mapsController.originIcon = onValue;
      setState(() {
        _mapsController.originIcon = onValue;
      });
    });
    if(useMap == true){
      _mapsController.getLocation().then((value){
        _marker = _mapsController.customMarkers;
      });
    }
    displayNotification();
    isOffline = _con.getOfflineOnline();
    Timer.periodic(const Duration(seconds: 2), (timer) async{
      getOffer();
      isTakingJob = UserController().getTakingJob();
    });
  }
  Future<void> checkLastActiveJob() async{
    if(_con.getTakingJob()){
      isTakingJob = true;
      Booking bk = BookingController().getTmpBooking();
      print("xxwa $bk");
      await _mapsController.getPolyline(bk.startLoc!,bk.endLoc!).then((value){
        print(value.toJson().toString());
        _polyline.add(value);
      });
    }
  }
  Future<void> getOffer() async{
    var box = Hive.box('taxi_app');
    String offerJob = box.get("offerJob").toString();
    if (offerJob.isNotEmpty && offerJob != "[]" && offerJob != "null" && offerJob != "Full authentication is required to access this resource"){
      var objJson = jsonDecode(offerJob)["message"] as List;
      Map<String, dynamic> map = objJson[0];
      Booking tmp = Booking.fromJSON(map);
      if(lastID != tmp.id){
        lastID = tmp.id!;
        Navigator.of(context).push(MaterialPageRoute(builder: (_)=>JobDialog(jobId: lastID)));
      }
    }
  }
  Future<void> displayNotification() async{
    // FlutterBackgroundService().invoke("stopService");
    const AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails('your channel id', 'your channel name',
        channelDescription: 'your channel description',
        importance: Importance.max,
        priority: Priority.max,
        ticker: 'ticker');
    const NotificationDetails notificationDetails =
    NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
        0, 'Taxi Driver', 'Welcome Back', notificationDetails,
        payload: 'item x');
  }
  bool useMap = true;


  void refreshMaps() async{
    setState(() {
      _marker = _mapsController.customMarkers;
    });
    final GoogleMapController controller = await googleMapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_mapsController.cameraPosition));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(body:
    SafeArea(child: Stack(
      children: [
        Positioned(child:
        Container(
          child:
          (useMap == true) ? GoogleMap(
            mapType: MapType.normal,
            polylines: _polyline.toSet(),
            initialCameraPosition: _cameraPosition,
            myLocationEnabled: false,
            onMapCreated: (GoogleMapController controller) {
              googleMapController.complete(controller);
            },
            onTap: (_) => FocusScope.of(context).unfocus(),
            markers: _marker.toSet(),
          ) :
          Container(color: Colors.green),)
        ),
        (isTakingJob) ? SizedBox() : Positioned(
          top: 35,
          right: 25,
          child: ExpandableNotifier(  // <-- Provides ExpandableController to its children
            child: Expandable(           // <-- Driven by ExpandableController from ExpandableNotifier
              collapsed: ExpandableButton(  // <-- Expands when tapped on the cover photo
                child: Row(children: [
                  Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), color: Colors.white),
                  padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5), child: Row(children: [
                    Icon(CupertinoIcons.circle_fill, color: isOffline == false ? Colors.green : Colors.red),
                    SizedBox(width:5),
                    Text(isOffline == false ? "ACTIVE" : "INACTIVE"),
                    SizedBox(width:5),
                    Icon(CupertinoIcons.chevron_down)
                  ])),
                  SizedBox(width: 10),
                  InkWell(onTap: (){
                    refreshMaps();
                  },
                      child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: Colors.white),
                      padding: EdgeInsets.only(left: 8, right: 8, top: 5, bottom: 5),
                      child: Icon(CupertinoIcons.location_circle_fill)))
              ])
              ),
              expanded:
              Card(child: Padding(padding: EdgeInsets.all(15),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(child: ClipRect(child: ExpandableButton(       // <-- Collapses when tapped on
                          child: Padding(padding: EdgeInsets.only(bottom: 10), child: Row(
                              mainAxisSize: MainAxisSize.min,
                            children: [
                              Flexible(child:ClipRect(child: Text("STATUS: "+(isOffline == false ? "ACTIVE" : "INACTIVE")))),
                              Flexible(child:ClipRect(child: SizedBox(width: 10))),
                              Flexible(child:ClipRect(child: Icon(CupertinoIcons.chevron_up))),
                            ])
                          )
                      ))),
                      Row(mainAxisSize: MainAxisSize.min,
                        children: [
                          Flexible(child:ClipRect(child: Column(children: [ Text("Order In"), Text("1", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20))]))),
                          SizedBox(width: 10),
                          Flexible(child:ClipRect(child: Column(children: [ Text("Order Finish"), Text("1", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20))]) ))
                        ],
                      ),
                      SizedBox(height: 10),
                      InkWell(onTap: (){
                        confirm();
                      },child: Row(mainAxisSize: MainAxisSize.min,
                        children: [
                          Flexible(child:ClipRect(child: Icon(CupertinoIcons.power, color: (isOffline ? Colors.green : Colors.red),))),
                          SizedBox(width: 10),
                          Flexible(child:ClipRect(child: Text("GO "+(isOffline == false ? "OFFLINE" : "ONLINE"))))
                        ],
                      )),
                      SizedBox(height: 5),
                      Container(color: Colors.black, height: 10, child: SizedBox(height: 10,),),
                      SizedBox(height: 5),
                      InkWell(onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (_) => Profile(contextx: context)));
                        }, child: Container(child: Row(mainAxisSize: MainAxisSize.min,
                        children: [
                          Flexible(child:ClipRect(child: Icon(CupertinoIcons.square_grid_2x2))),
                          SizedBox(width: 10),
                          Flexible(child:ClipRect(child: Text("Menu")))
                        ])
                      ))
                    ]),
              ),),
            ),
          ),
        ),
        Positioned(bottom: -3, child:(isTakingJob) ? HomeJobBottom() : SizedBox())
        // Positioned(bottom: 0, child: Container(color: Colors.white,
        //     padding: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 10),
        //     width: MediaQuery.of(context).size.width,
        //     child: Column( children: [
        //     Row( children: [
        //       Expanded(child: Icon(CupertinoIcons.home)),
        //       // Expanded(child: Icon(CupertinoIcons.home)),
        //       Expanded(child: Icon(CupertinoIcons.person)),
        //   ]),
        // ]))),
      ],
    )
    ),
    );
  }
  void confirm(){
    Asuka.showDialog(
      builder: (context) => AlertDialog(
        title: const Text('Confirmation'),
        content: const Text('Click confirm to continue'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () {
              _con.enabledDisabled();
              setState(() { isOffline = _con.getOfflineOnline();});
              Navigator.pop(context);
            },
            child: const Text('Confirm'),
          ),
        ],
      ),
    );
  }
}