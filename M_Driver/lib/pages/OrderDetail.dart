import 'package:asuka/asuka.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../controller/CBooking.dart';
import '../model/Booking.dart';

class OrderDetail extends StatefulWidget{
  OrderDetail({required this.booking});
  late Booking booking;
  _OrderDetail createState() => _OrderDetail();
}
class _OrderDetail extends State<OrderDetail>{
  late String status = "";
  late BookingController bc;
  late String note = "";
  @override
  void initState() {
    bc = BookingController();
    // TODO: implement initState
    switch(widget.booking.status){
      case 0: status = "Requested"; break;
      case 1: status = "Active"; break;
      case 2: status = "Canceled"; break;
      case 3: status = "Finished"; break;
      default: status = ""; break;
    }
    note = (widget.booking.note!.toString() == "") ? "Empty." : widget.booking.note!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child:
            Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Card(margin: EdgeInsets.all(15),child: Padding(padding: EdgeInsets.all(15), child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(child:
                          Column(children: [
                            Text("ISAC", style: TextStyle(fontWeight: FontWeight.bold)),
                          ], crossAxisAlignment: CrossAxisAlignment.start,
                          )),
                          Text("$status")
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Fare"),
                              SizedBox(height: 5),
                              Text("Booking ID"),
                              SizedBox(height: 5),
                              Text("Class")
                            ],
                          ),
                            flex: 0,
                          ),
                          Expanded(child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                                Text("${widget.booking.payment}"),
                                SizedBox(width: 5),
                                Text("12K", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green))
                              ]),
                              SizedBox(height: 5),
                              Text("${widget.booking.id}", style: TextStyle(fontWeight: FontWeight.bold)),
                              SizedBox(height: 5),
                              Text("Economy", style: TextStyle(fontWeight: FontWeight.bold))
                            ],
                          ))
                        ],
                      ),
                      //Key point
                      SizedBox(height: 15),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Flex(
                              direction: Axis.horizontal,
                              children: [
                                Icon(Icons.check_circle_sharp, color: Colors.grey.shade200,),
                                SizedBox(width: 5),
                                Text("Driver accept the order.")
                              ]),
                          SizedBox(height: 5),
                          Flex(direction: Axis.horizontal,
                              children: [
                                Icon(Icons.arrow_circle_right_rounded, color: Colors.yellow.shade700),
                                SizedBox(width: 5),
                                Text("Driver on the way to your location.")
                              ]),
                        ],
                      ),
                      SizedBox(height: 15,),
                      Text("Users Note", style: TextStyle(color: Theme.of(context).primaryColorDark)),
                      SizedBox(height: 5,),
                      Text(note)
                    ],
                  ),
                  ),
                  ),
                  (widget.booking.status == 0) ? Padding(padding: EdgeInsets.only(left: 15, right: 15),child:Material(
                    color: Theme.of(context).primaryColor,
                    child: InkWell(
                        onTap: () {
                          //alert dialog first
                          cancelOrder();
                        },
                        child: SizedBox(
                          height: kToolbarHeight,
                          width: double.infinity,
                          child: Center(
                            child: Text(
                              'Cancel',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        )),
                  )) : SizedBox()
                ]
            )
        )
    );
  }
  cancelOrder(){
    if(widget.booking.status == 0){
      Asuka.showDialog(
        builder: (context) => AlertDialog(
          title: const Text('Cancel Order'),
          content: const Text('Confirm cancellation'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                bc.cancelLastBooking().then((value) => Navigator.pop(context));
              },
              child: const Text('Confirm'),
            ),
          ],
        ),
      );
    }
  }
  sendRating(int index) async {
    if(widget.booking.rating == null){
      Asuka.showDialog(
        builder: (context) => AlertDialog(
          title: const Text('Submit Rating'),
          content: const Text('Confirm submission'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                Booking tmp = widget.booking;
                tmp.rating = index;
                // bc.sendRating(tmp).then((value) => Navigator.pop(context));
              },
              child: const Text('Send'),
            ),
          ],
        ),
      );
    }
  }
}