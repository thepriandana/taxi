import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../Strings.dart';
import '../controller/CUser.dart';

class DialogEmergencyContact extends StatefulWidget {
  @override
  _DialogEmergencyContactState createState() => _DialogEmergencyContactState();
}

class _DialogEmergencyContactState extends StateMVC<DialogEmergencyContact>{
  _DialogEmergencyContactState() : super(UserController()) {
    _con = controller as UserController;
  }
  late UserController _con;
  @override
  Widget build(BuildContext context) {
    //name, phone number, relation
    return Scaffold(body:SafeArea(child:
        Padding(padding: EdgeInsets.all(15),
      child: Form(
          key: _con.loginFormKey,
          child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Emergency Contact", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            SizedBox(height: 10,),
            Text("Please insert carefully the contact below, this contact will be called if there's an emergency situation."),
            SizedBox(height: 10,),
            TextFormField(keyboardType: TextInputType.text,
                onSaved: (input){_con.emergencyContact.name = input;},
                validator: (input) => input!.length < 3 ? S.getText("should_be_more_than_3_letters") : null,
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(12),
                    hintText: "${S.getText("emergency_name")}",
                    hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)))),
            TextFormField(keyboardType: TextInputType.text,
                onSaved: (input){ _con.emergencyContact.relation = input;},
                validator: (input) => input!.length < 3 ? S.getText("should_be_more_than_3_letters") : null,
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(12),
                    hintText: "${S.getText("emergency_relation")}",
                    hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)))),
            TextFormField(keyboardType: TextInputType.phone,
                onSaved: (input){_con.emergencyContact.phone = input;},
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(12),
                    hintText: "${S.getText("emergency_phone")}",
                    hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)))),
            SizedBox(height: 10),
            InkWell(
              onTap: (){_con.registerEmergencyContact();},
                child: Container(
              padding: EdgeInsets.all(10),
                decoration: BoxDecoration(color: Colors.blueAccent, borderRadius: BorderRadius.circular(15)),
              child: Center(child: Text("Save", style: TextStyle(color: Colors.white)))))
          ])
      )),
    ));
  }
}
