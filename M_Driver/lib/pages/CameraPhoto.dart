import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class CameraPhoto extends StatefulWidget {
  CameraPhoto({required this.cameras, required this.path, required this.cameraNum, required this.f});
  late String path;
  late List<CameraDescription> cameras;
  late int cameraNum;
  late Function f;
  @override
  State<CameraPhoto> createState() => _CameraPhotoState();
  String lastPath = "";
}

class _CameraPhotoState extends State<CameraPhoto>
    with WidgetsBindingObserver {
  late CameraController _controller;
  bool _isCameraPermissionGranted = false;

  @override
  initState() {
    // TODO: implement initState
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    _controller = CameraController(widget.cameras[widget.cameraNum], ResolutionPreset.max);
    getPermissionStatus();
    super.initState();
  }

  getPermissionStatus() async {
    await Permission.camera.request();
    var status = await Permission.camera.status;
    if (status.isGranted) {
      setState(() {
        _isCameraPermissionGranted = true;
      });
      onNewCameraSelected(widget.cameras[widget.cameraNum]);
    } else {
      print("Permission Denied");
    }
  }

  @override
  Widget build(BuildContext context) {
    return (!_controller.value.isInitialized)
        ? Container(
      child: const Center(child: CircularProgressIndicator()),
    ) : Scaffold(
        body: Container(
            color: Colors.black,
            child: Center(
                child: _isCameraPermissionGranted
                    ? Stack(children: [
                  Positioned(
                      child: AspectRatio(
                        aspectRatio: 1 / _controller.value.aspectRatio,
                        child: CameraPreview(_controller, child:
                        LayoutBuilder(builder: (BuildContext context,
                            BoxConstraints constraints) {
                          return GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTapDown: (details) =>
                                onViewFinderTap(details, constraints),
                          );
                        })),
                      )),
                  Positioned(
                      left: (MediaQuery.of(context).size.width / 2 -
                          22),
                      bottom: 10,
                      child: FloatingActionButton(
                          child: const Icon(CupertinoIcons.camera_circle),
                          onPressed: () async {
                            try {
                              final image = await _controller.takePicture();
                              if (!mounted) return;
                              changeFileNameOnly(File(image.path), widget.path);
                            } catch (e) {
                              print(e);
                            }
                          })),
                ]): Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(),
                    const Text(
                      'Permission denied',
                      style: TextStyle(color: Colors.white),
                    ),
                    const SizedBox(height: 16),
                    ElevatedButton(
                      onPressed: () {
                        getPermissionStatus();
                      },
                      child: const Text('Give permission'),
                    ),
                  ],
                ))));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController cameraController = _controller;
    // App state changed before we got the chance to initialize.
    if (!cameraController.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      // Free up memory when camera not active
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      // Reinitialize the camera with same properties
      onNewCameraSelected(cameraController.description);
    }
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    final previousCameraController = _controller;
    // Instantiating the camera controller
    final CameraController cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.high,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );
    // Dispose the previous controller
    await previousCameraController.dispose();
    // Replace with the new controller
    if (mounted) {
      setState(() {
        _controller = cameraController;
      });
    }
    // Update UI if controller updated
    cameraController.addListener(() {
      if (mounted) setState(() {});
    });
    // Initialize controller
    try {
      await cameraController.initialize();
    } on CameraException catch (e) {
      print('Error initializing camera: $e');
    }
  }

  void onViewFinderTap(TapDownDetails details, BoxConstraints constraints) {
    // if (_controller == null) {
    //   return;
    // }
    final offset = Offset(
      details.localPosition.dx / constraints.maxWidth,
      details.localPosition.dy / constraints.maxHeight,
    );
    _controller!.setExposurePoint(offset);
    _controller!.setFocusPoint(offset);
  }
  void changeFileNameOnly(File file, String newFileName) {
    var path = file.path;
    var lastSeparator = path.lastIndexOf(Platform.pathSeparator);
    var newPath = path.substring(0, lastSeparator + 1) + newFileName;
    file.rename(newPath);
    widget.lastPath = newPath;
    widget.f();
    Navigator.of(context).pop();

  }
}
