import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/controller/CBooking.dart';
import 'package:taxi_driver/pages/SignUpAdvances.dart';

import '../Strings.dart';
import '../controller/CUser.dart';

class SignUp extends StatefulWidget{
  _SignUp createState()=> _SignUp();
}
class _SignUp extends StateMVC<SignUp>{
  _SignUp() : super(UserController()) {
    _con = controller as UserController;
  }
  late UserController _con;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Center(
          child: Padding(padding: const EdgeInsets.only(left: 35, right: 35), child: Form(
            key: _con.loginFormKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  keyboardType: TextInputType.text,
                  onSaved: (input) => _con.user.name = input,
                  validator: (input) => input!.length < 3 ? S.getText("should_be_more_than_3_letters") : null,
                  decoration: InputDecoration(
                    labelText: S.getText("user_name"),
                    labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                    contentPadding: const EdgeInsets.all(12),
                    hintText: "${S.getText("sign_name")}",
                    hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                    prefixIcon: Icon(Icons.account_circle_outlined, color: Theme.of(context).colorScheme.secondary),
                  ),
                ),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  onSaved: (input) => _con.user.email = input,
                  validator: (input) => !input!.contains('@') ? S.getText("should_be_a_valid_email") : null,
                  decoration: InputDecoration(
                    labelText: S.getText("email"),
                    labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                    contentPadding: const EdgeInsets.all(12),
                    hintText: "${S.getText("sign_email")}",
                    hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                    prefixIcon: Icon(Icons.alternate_email, color: Theme.of(context).colorScheme.secondary),
                  ),
                ),
                TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: _con.hidePassword,
                    onSaved: (input) => _con.user.password = input!,
                    validator: (input) => input!.length < 8 ? S.getText("should_be_more_than_8_characters") : null,
                    decoration: InputDecoration(
                      labelText: S.getText("password"),
                      labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                      contentPadding: const EdgeInsets.all(12),
                      hintText: '******',
                      hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                      prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).colorScheme.secondary),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            _con.hidePassword = !_con.hidePassword;
                          });
                        },
                        color: Theme.of(context).focusColor,
                        icon: Icon(_con.hidePassword ? Icons.visibility_off_outlined: Icons.visibility_outlined),
                      ),
                    )
                ),
                const SizedBox(height: 15),
                ElevatedButton(onPressed: (){
                  _con.register();
                  // Navigator.of(context).pushReplacementNamed("/SignUpCar");
                  }, child: Text("${S.getText("sign_signup")}")),
                const SizedBox(height: 15),
                TextButton(onPressed: (){
                  Navigator.of(context).pushReplacementNamed("/Login");
                }, child: Text("${S.getText("sign_already")}")),
              ],
            ),
          )
          ),
        ),),
    );
  }
}