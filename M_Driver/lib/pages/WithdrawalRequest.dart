import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/controller/CWithdrawal.dart';

import '../Strings.dart';

class WithdrawalRequest extends StatefulWidget {
  @override
  _WithdrawalRequestState createState() => _WithdrawalRequestState();
}

class _WithdrawalRequestState extends StateMVC<WithdrawalRequest>{
  _WithdrawalRequestState() : super(WithdrawalController()) {
    _con = controller as WithdrawalController;
  }
  late WithdrawalController _con;
  int num = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Container(
        padding: EdgeInsets.all(15),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text("Balance", style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text("Rp 100.000", style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              SizedBox(height: 15),
              Text("Withdrawal Request"),
              SizedBox(height: 10),
              TextFormField(
                keyboardType: TextInputType.number,
                onChanged: (value){ num = int.parse(value);},
                onSaved: (value){ num = int.parse(value??"$num");},
                onFieldSubmitted: (value){ num = int.parse(value);} ,
                validator: (input) => int.parse(input.toString())>100000 ? S.getText("should_be_less_than_balance") : null,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  filled: true,
                  fillColor: Colors.grey.shade200,
                  // labelText: "Input request",
                  labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                  // contentPadding: const EdgeInsets.all(12),
                  hintText: "Requested withdrawal balance",
                  hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                  prefixIcon: Icon(Icons.monetization_on_outlined, color: Theme.of(context).colorScheme.secondary),
                ),
              ),
              SizedBox(height: 10),
              InkWell(onTap: (){
                _con.sendRequest(num);
              }, child:
              Container(padding: const EdgeInsets.only(top: 10, bottom: 10),
                  decoration: BoxDecoration(color: Colors.blue, borderRadius: BorderRadius.circular(3)),
                  width: MediaQuery.of(context).size.width,
                  child: Center(child: Text("Submit", style: TextStyle(color: Colors.white)))),
              )
            ]
        )
    ));
  }
}
