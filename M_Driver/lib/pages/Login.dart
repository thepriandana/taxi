import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../Strings.dart';
import '../controller/CUser.dart';

class Login extends StatefulWidget{
  _Login createState()=> _Login();
}
class _Login extends StateMVC<Login>{
  _Login() : super(UserController()) {
    _con = controller as UserController;
  }
  late UserController _con;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Container(
              child:
              Center(
                  child: Padding(padding: const EdgeInsets.only(left: 35, right: 35), child: Form(
                      key: _con.loginFormKey,
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(Icons.local_taxi, size: 50),
                            const SizedBox(height: 15),
                              TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                onSaved: (input) => _con.user.email = input!,
                                validator: (input) => !input!.contains('@') ? S.getText("should_be_a_valid_email") : null,
                                decoration: InputDecoration(
                                  labelText: S.getText("email"),
                                  labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                                  contentPadding: const EdgeInsets.all(12),
                                  hintText: S.getText("sign_email"),
                                  hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                                  prefixIcon: Icon(Icons.alternate_email, color: Theme.of(context).colorScheme.secondary),
                                ),
                              ),
                            TextFormField(
                                keyboardType: TextInputType.text,
                                obscureText: _con.hidePassword,
                                onSaved: (input) => _con.user.password = input!,
                                validator: (input) => input!.length < 7 ? S.getText("should_be_more_than_8_characters") : null,
                                decoration: InputDecoration(
                                  labelText: S.getText("pass"),
                                  labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                                  contentPadding: const EdgeInsets.all(12),
                                  hintText: '******',
                                  hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.5)),
                                  prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).colorScheme.secondary),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _con.hidePassword = !_con.hidePassword;
                                      });
                                    },
                                    color: Theme.of(context).focusColor,
                                    icon: Icon(_con.hidePassword ? Icons.visibility_off_outlined: Icons.visibility_outlined),
                                  ),
                                )
                            ),
                            const SizedBox(height: 15),
                            ElevatedButton(onPressed: (){_con.login();}, child: Text("${S.getText("sign_in")}")),
                            const SizedBox(height: 15),
                            TextButton(onPressed: (){
                              Navigator.of(context).pushReplacementNamed("/SignUp");
                            }, child: Text("${S.getText("sign_regis")}")),
                            const SizedBox(height: 3),
                            Row(children: [const Expanded(child: Divider()), const SizedBox(width: 8), const Text("or"), const SizedBox(width: 8), const Expanded(child: Divider())],),
                            const SizedBox(height: 3),
                            TextButton(onPressed: (){}, child: Text("${S.getText("sign_reset")}"))
                          ]))
                  )
              )
          ),
        ));
  }
}