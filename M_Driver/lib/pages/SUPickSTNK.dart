import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../GlobalSettings.dart';
import '../controller/CSignUpCar.dart';
import '../element/ImageJWT.dart';
import 'CameraPhoto.dart';

class SUPickSTNK extends StatefulWidget {
  @override
  _SUPickSTNKState createState() => _SUPickSTNKState();
  SUPickSTNK({required this.cameras, this.url, required this.status});
  late List<String>? url;
  late int status;
  late List<CameraDescription> cameras;
}

class _SUPickSTNKState extends StateMVC<SUPickSTNK>{
  _SUPickSTNKState() : super(SignUpCarController()) {
    _con = controller as SignUpCarController;
  }

  late SignUpCarController _con;
  late String lastPath = "";
  late String lastPath2 = "";
  List<String> pict = [];
  @override
  Widget build(BuildContext context) {
    List<String> urlL = widget.url ?? [];
    String fst = "";
    String sec = "";
    if(urlL.isNotEmpty){
      if(urlL.any((element) => element.endsWith("-1.png"))){
        fst = urlL.firstWhere((element) => element.endsWith("-1.png")).toString();
      }
      if(urlL.any((element) => element.endsWith("-2.png"))){
        sec = urlL.firstWhere((element) => element.endsWith("-2.png")).toString();
      }
    }
    return Container(
        child: Center(child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              const Text("Upload STNK Proof", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              const SizedBox(height: 10),
              Flexible(child: Text("We need to see front and back\n information clearly printed on official document", textAlign: TextAlign.center)),
              const SizedBox(height: 15),
              Text("Front"),
              SizedBox(height: 10),
              InkWell(onTap: (){
                if(widget.status != 2) Navigator.of(context).push(MaterialPageRoute(builder: (context) => CameraPhoto(cameras: widget.cameras, path: "stnk-1", cameraNum: 0, f: updatefile1)));
              }, child: Padding(padding: EdgeInsets.all(15), child: DottedBorder(
                borderType: BorderType.RRect,
                radius: Radius.circular(15),
                color: Colors.black38,
                dashPattern: [8, 4],
                strokeWidth: 2,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child:  (urlL.isEmpty || (lastPath == "" && fst == "")) ? Container(
                        height: 200,
                        width: MediaQuery.of(context).size.width * 0.9,
                        color: Colors.blue.withOpacity(0.1),
                        child: Center(child: Icon(CupertinoIcons.plus))
                    ) : RotatedBox(quarterTurns: -1, child:  (lastPath == "") ? ImageJWT(url: fst) : Image.file(File(lastPath)))
                ),
              ))),
              SizedBox(height: 10),
              Text("Back"),
              InkWell(onTap: (){
                if(widget.status != 2) Navigator.of(context).push(MaterialPageRoute(builder: (context) => CameraPhoto(cameras: widget.cameras, path: "stnk-2", cameraNum: 0, f: updatefile2)));
              }, child: Padding(padding: EdgeInsets.all(15), child: DottedBorder(
                borderType: BorderType.RRect,
                radius: Radius.circular(15),
                color: Colors.black38,
                dashPattern: [8, 4],
                strokeWidth: 2,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child:  (urlL.isEmpty || (lastPath2 == "" && sec == "")) ? Container(
                        height: 200,
                        width: MediaQuery.of(context).size.width * 0.9,
                        color: Colors.blue.withOpacity(0.1),
                        child: Center(child: Icon(CupertinoIcons.plus))
                    ) : RotatedBox(quarterTurns: -1, child:  (lastPath2 == "") ? ImageJWT(url: sec) : Image.file(File(lastPath2)))
                ),
              ))),
              SizedBox(height: 15),
              (widget.status == -1) ? Container(child: Text("Document declined, upload new document"), padding: EdgeInsets.all(15)) : SizedBox(),
              (widget.status == 2) ? Container(child: Text("Document has been verified and accepted"), padding: EdgeInsets.all(15)) : ElevatedButton(onPressed: (){
                _con.sendSTNK(pict);
              }, child: Row(mainAxisSize: MainAxisSize.min,children:[ Icon(CupertinoIcons.arrow_up), Text("Upload")]))
            ]
        ))
    );
  }
  void updatefile1(){
    pict.add("${GS.pictLoc}/stnk-1");
    setState(() {
      lastPath = "${GS.pictLoc}/stnk-1";
    });
  }
  void updatefile2(){
    pict.add("${GS.pictLoc}/stnk-2");
    setState(() {
      lastPath2 = "${GS.pictLoc}/stnk-2";
    });
  }
}
