import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:asuka/asuka.dart';
import 'package:taxi_driver/controller/CBooking.dart';
class JobDialog extends StatefulWidget {
  JobDialog({required this.jobId});
  String jobId;
  @override
  State<JobDialog> createState() => _JobDialogState();
}

class _JobDialogState extends State<JobDialog> {
  BookingController bc = BookingController();
  String note = "WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW WWWWWWWWWWWWWWWWWWW";
  late Timer _timer;
  int _start = 15;
  void startTimer() {
    const oneSec =  Duration(seconds: 1);
    _timer =  Timer.periodic(
      oneSec,
          (Timer timer) {
        if (_start == 0) {

        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Row(
          children: [
            Expanded(child: Text("New Order")),
            TextButton(onPressed: () { denied(); }, child: Text("DENIED", style: TextStyle(color: Colors.red),))
          ],
        ), backgroundColor: Colors.black,),
        body:SafeArea(child: Column(
          children: [
            Card(child:
              Padding(padding: EdgeInsets.all(15),child: Row(
                    children: [
                      Expanded(child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text("Rp"),
                          Text("10.000", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)),
                          Wrap(children: [
                            Container(child: Text("Cash", style: TextStyle(color: Colors.white)),
                              padding: const EdgeInsets.only(top: 3, bottom: 3, left: 5, right: 5), decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Colors.green)),
                            const SizedBox(width: 5),
                            Container(child: Text("Promo", style: TextStyle(color: Colors.white)),
                                padding: const EdgeInsets.only(top: 3, bottom: 3, left: 5, right: 5), decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Colors.green))
                          ])
                        ])
                      )
                    ])
              )
            ),
            Card(child:
              Padding(padding: EdgeInsets.all(15),child: Column(children:[
                Row(children: [
                  Icon(CupertinoIcons.location_solid, color: Colors.blueAccent,),
                  SizedBox(width: 10,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("NAME"),
                      Text("Location Street")
                    ],
                  )]),
                const Divider(height: 30,),
                Row(children: [
                  Icon(CupertinoIcons.location_solid, color: Colors.orangeAccent,),
                  SizedBox(width: 10,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("NAME"),
                      Text("Location Street")
                    ]),
                ]),
                const Divider(height: 30,),
                InkWell(onTap: (){ displayNote();}, child: Row(children: [
                    Icon(CupertinoIcons.info_circle),
                    SizedBox(width: 10,),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Note"),
                        Text("${note.substring(0, 20)}..."),
                      ]),
                    ),
                    IconButton(onPressed: displayNote, icon: Icon(CupertinoIcons.arrow_right_circle_fill))
                  ]),)
                ]),
              )
            ),
            // height: kToolbarHeight,
            Expanded(child: SizedBox()),
            CircularCountDownTimer(
              duration: 10,
              initialDuration: 0,
              controller: CountDownController(),
              width: MediaQuery.of(context).size.width / 3,
              height: MediaQuery.of(context).size.height / 3,
              ringColor: Colors.grey[300]!,
              ringGradient: null,
              fillColor: Colors.green,
              fillGradient: null,
              backgroundColor: Colors.transparent,
              backgroundGradient: null,
              strokeWidth: 20.0,
              strokeCap: StrokeCap.round,
              textStyle: TextStyle(
                  fontSize: 70.0, color: Colors.green, fontWeight: FontWeight.bold),
              textFormat: CountdownTextFormat.S,
              isReverse: true,
              isReverseAnimation: true,
              isTimerTextShown: true,
              autoStart: true,
              onStart: () {
                debugPrint('Countdown Started');
              },
              onComplete: () {
                debugPrint('Countdown Ended');
                // Navigator.of(context).pop();
              },
              onChange: (String timeStamp) {
                debugPrint('Countdown Changed $timeStamp');
              },
              timeFormatterFunction: (defaultFormatterFunction, duration) {
                if (duration.inSeconds == 0) {
                  return "0";
                } else {
                  return Function.apply(defaultFormatterFunction, [duration]);
                }
              },
            ),
            Expanded(child: SizedBox()),
            InkWell(child: Container(child: Center(child: Text("ACCEPT OFFEER", style: TextStyle(color: Colors.white),)), padding: EdgeInsets.all(10), color: Colors.green, width: double.infinity, margin: EdgeInsets.only(left: 15, right: 15),) , onTap: (){accept();},),
            SizedBox(height: 15,)
          ],),)
    );
  }
  denied(){
    Asuka.showDialog(
      builder: (context) => AlertDialog(
        title: const Text('Confirmation'),
        content: const Text('Denied Job Offer?'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Confirm'),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel'),
          ),
        ],
      ),
    );
  }
  accept() async {
    bc.takeJob(widget.jobId);


  }
  displayNote(){
    Asuka.showDialog(
      builder: (context) => AlertDialog(
        title: const Text('Note'),
        content: Text('$note'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Ok'),
          ),
        ],
      ),
    );
  }

}
