import 'package:asuka/asuka.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxi_driver/controller/CUser.dart';
import 'package:taxi_driver/element/ImageJWT.dart';
import 'package:taxi_driver/model/EmergencyContact.dart';
import 'package:taxi_driver/pages/DialogEmergencyContact.dart';
import 'package:taxi_driver/pages/History.dart';
import 'package:taxi_driver/pages/Login.dart';
import 'package:taxi_driver/pages/WithdrawalList.dart';

import '../main.dart';
import 'UserSettings.dart';

class Profile extends StatefulWidget {
  Profile({required this.contextx});
  BuildContext contextx;
  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Container(padding: EdgeInsets.only(top:20, bottom: 20), color: Colors.white, child: Column(crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [Row(mainAxisAlignment: MainAxisAlignment.start,children:[ Expanded(child:
        InkWell(onTap: (){Navigator.of(context).pop();}, child: Icon(CupertinoIcons.arrow_left))
        ),
          const Expanded(flex: 5, child: Text("My Profile", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
          Expanded(child: SizedBox())]),
          // ImageJWT(url: url),
          SizedBox(height: 35),
          Center(child: Text("John", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
          Center(child: Text("Join since 2022",style: TextStyle(fontSize: 15, color: Colors.grey), )),
          SizedBox(height: 35),
          SizedBox(height: 15),
          Container(color: Colors.grey.shade200, margin: EdgeInsets.only(bottom: 5, left: 10, right: 10), padding: EdgeInsets.all(15),
              child: InkWell(onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (_) => History()));
              },child: Row(children: const [
                Icon(CupertinoIcons.cube_box),
                SizedBox(width: 5),
                Text("History", style: TextStyle( fontSize: 18)),
              ]))),
          Container(color: Colors.grey.shade200, margin: EdgeInsets.only(bottom: 5, left: 10, right: 10), padding: EdgeInsets.all(15),
              child: InkWell(onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (_) => WithdrawalList()));
              }, child: Row(children: const [
                Icon(CupertinoIcons.money_dollar_circle),
                SizedBox(width: 5),
                Text("Withdrawal", style: TextStyle( fontSize: 18)),
              ]))),
          Container(color: Colors.grey.shade200, margin: EdgeInsets.only(bottom: 5, left: 10, right: 10), padding: EdgeInsets.all(15),
              child: InkWell(onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (_) => DialogEmergencyContact()));
              }, child: Row(children: const [
                Icon(CupertinoIcons.person),
                SizedBox(width: 5),
                Text("Emergency Contact", style: TextStyle( fontSize: 18)),
              ]))),
          Expanded(child: SizedBox()),
          Padding(padding: EdgeInsets.all(15), child: InkWell(onTap: (){confirm();}, child: Column(
            children: const [
              Icon(CupertinoIcons.arrow_right_square),
              Text("Logout")
            ],
          )))
        ]
    ),));
  }
  void confirm(){
    Asuka.showDialog(
      builder: (context) => AlertDialog(
        title: const Text('Confirmation'),
        content: const Text('Log out account?'),
        actions: [
          TextButton(
            onPressed: () {
              UserController().logout().then((value){});
              Navigator.of(widget.contextx).pushReplacement(MaterialPageRoute(builder: (_) => Login()));
              Navigator.pop(context);
            },
            child: const Text('Confirm'),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel'),
          ),
        ],
      ),
    );
  }
}
