import 'package:asuka/asuka.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:taxi_driver/pages/Home.dart';
import 'package:taxi_driver/pages/DialogJobOffer.dart';
import 'package:taxi_driver/pages/SignUp.dart';

import '../controller/CBooking.dart';
import '../controller/CUser.dart';
import 'package:geolocator/geolocator.dart';

class Splash extends StatefulWidget{
  _Splash createState() => _Splash();
}
class _Splash extends State<Splash>{
  late UserController uc;
  late BookingController bc;
  @override
  void initState() {
    super.initState();
    uc = new UserController(context);
    bc = new BookingController(context);
    // Future.delayed(const Duration(milliseconds: 500), () {
    //   setState(() {
    //     Navigator.of(context).pushAndRemoveUntil(
    //         MaterialPageRoute(builder: (context) => SignUp()),
    //             (route) => false);
    //   });
    // });
    mustPass();
  }

  Future<Position> _determinePosition() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied || permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Fluttertoast.showToast(msg: "Please allow location permission to continue");
        _determinePosition();
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      await Geolocator.openAppSettings();
      _determinePosition();
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  void mustPass(){
    bc.refreshPaymentMethod().then((_){
      bc.refreshRideClassMethod().then((_){
        // bc.setBookingIntoFirstPayment();
        uc.checkLogin().then((value) async {
          if(value == true){
            await uc.checkTokenSplash().then((value) async{
              await _determinePosition().then((value){
                uc.afterLoginPage(context);
              });
            });
            FlutterBackgroundService().startService();
          }else{
            await _determinePosition().then((value){
              uc.afterLoginPage(context);
            });
          }
        });
      }).onError((error, stackTrace) {
        print("error $error");
        // ErrorVici().splashTimeout(mustPass);
      });
    }).onError((error, stackTrace) {
      // ErrorVici().splashTimeout(mustPass);/
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(child: Container()),
            Expanded(child: Column(mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.local_taxi, size: 50),
                SizedBox(height: 25),
                CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).hintColor)),
                SizedBox(height: 55),
              ],)),
            Expanded(child: Container()),
            Expanded(flex: 0, child: Text("Taxi App"),),
            SizedBox(height: 16.8)
          ],
        )),
      ),
    );
  }
}