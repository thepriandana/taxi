import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
// import 'package:taxi_app/element/ItemOrderWidget.dart';
// import 'package:taxi_app/page/OrderDetail.dart';

import '../controller/CBooking.dart';
import '../element/ItemOrderWidget.dart';
import '../model/Booking.dart';
import 'OrderDetail.dart';

class History extends StatefulWidget{
  _History createState()=> _History();
}
class _History extends StateMVC<History>{

  _History() : super(BookingController()) {
    _con = controller as BookingController;
  }
  @override
  void initState() {
    super.initState();
    _con.getBookingAll().then((value){
      setState(() {
        bookings = BookingController.booking;
        print(bookings);
      });
    });
  }
  late BookingController _con;
  late List<Booking> bookings = <Booking>[];
  int currentTab = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(key: _con.scaffoldKey,body: SafeArea(
      child: (bookings.isEmpty && bookings.length == 0) ? Container(
          child:Center(
            child: Column(mainAxisSize: MainAxisSize.min,
              children: [
                Image(image: AssetImage("lib/asset/empty.png"), height: 150,),
                Center(child: Text("No order history yet", style: TextStyle(color: Colors.black38),)),
                SizedBox(height: 200,)
              ],
            ),)
      ) :
      Column(children: [
           Padding(
            padding: EdgeInsets.all(15),child: Text("History", textAlign: TextAlign.center),
          ),
        Container(
            child: Expanded(
                child: ListView.builder(
                  itemCount: bookings.length,
                  itemBuilder: (context, index){
                    // return Container();
                    return InkWell(child: ItemOrderWidget(booking: bookings.elementAt(index)), onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (_) => OrderDetail(booking: bookings.elementAt(index))));
                    });
                  },
                )
            )
        )
      ]),
    ));
  }
}