import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../controller/CNotification.dart';

class Notifications extends StatefulWidget{
  _Notifications createState() => _Notifications();
}
class _Notifications extends StateMVC<Notifications>{
  _Notifications() : super(NotificationController()) {
    _con = controller as NotificationController;
  }

  @override
  void initState() {
    _con.getNotification();
    super.initState();
  }
  late NotificationController _con;
  @override
  Widget build(BuildContext context) {
    return Scaffold(key: _con.scaffoldKey, body: SafeArea(
        child: (_con.notification.isEmpty) ? Container(
          child:Center(
          child: Column(mainAxisSize: MainAxisSize.min,
            children: [
              Image(image: AssetImage("lib/asset/empty.png"), height: 150,),
              Center(child: Text("No notification yet", style: TextStyle(color: Colors.black38),)),
              SizedBox(height: 200,)
            ],
          ),)
        ):
        ListView.builder(itemBuilder: (context, index){
        return Container(padding: EdgeInsets.all(10),
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey.shade200))),
            child:
        Row(children: [
          CircleAvatar(child: Icon(Icons.notifications)),
          SizedBox(width: 10,),
          Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
            Text("${_con.notification.elementAt(index).title}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
            Text("${_con.notification.elementAt(index).subtitle}")
          ])),
        ],)
        );
      }, itemCount: _con.notification.length,
      ),
    ));
  }
}