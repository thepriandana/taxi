import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../GlobalSettings.dart';
import '../controller/CSignUpCar.dart';
import '../element/ImageJWT.dart';
import 'CameraPhoto.dart';

class SUPickPhotoProfile extends StatefulWidget {
  SUPickPhotoProfile({Key? key, required this.cameras, this.url, required this.status}) : super(key: key);
  List<CameraDescription> cameras;
  late String? url;
  late int status;
  @override
  _SUPickPhotoProfileState createState() => _SUPickPhotoProfileState();
}

class _SUPickPhotoProfileState extends StateMVC<SUPickPhotoProfile>{
  _SUPickPhotoProfileState() : super(SignUpCarController()) {
    _con = controller as SignUpCarController;
  }
  late SignUpCarController _con;
  late String lastPath = "";
  @override
  Widget build(BuildContext context) {
    String url = widget.url ?? "";
    return Container(
        child: Center(child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              const Text("Upload Your Photo", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              const SizedBox(height: 10),
              Flexible(child: Text("Please make sure to show your face clearly", textAlign: TextAlign.center)),
              const SizedBox(height: 15),
              InkWell(onTap: (){
                if(widget.status != 2) Navigator.of(context).push(MaterialPageRoute(builder: (context) => CameraPhoto(cameras: widget.cameras, path: "profile", cameraNum: 1, f: updatefile)));
              }, child: Padding(padding: EdgeInsets.all(15), child: DottedBorder(
                borderType: BorderType.RRect,
                radius: Radius.circular(15),
                color: Colors.black38,
                dashPattern: [8, 4],
                strokeWidth: 2,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: (url == "" && lastPath == "") ? Container(
                        height: MediaQuery.of(context).size.width * 0.7,
                        width: MediaQuery.of(context).size.width * 0.9,
                        color: Colors.blue.withOpacity(0.1),
                        child: Center(child: Icon(CupertinoIcons.plus))
                    ) : (lastPath == "") ? ImageJWT(url: url) : Image.file(File(lastPath))
                ),
              ),
              ),),
              SizedBox(height: 15),
              (widget.status == -1) ? Container(child: Text("Document declined, upload new document"), padding: EdgeInsets.all(15)) : SizedBox(),
              (widget.status == 2) ? Container(child: Text("Document has been verified and accepted"), padding: EdgeInsets.all(15)) : ElevatedButton(onPressed: (){
                _con.sendPP();
              }, child: Row(mainAxisSize: MainAxisSize.min,children:[ Icon(CupertinoIcons.arrow_up), Text("Upload")]))
            ]
        ))
    );
  }
  void updatefile(){
    setState(() {
      lastPath = "${GS.pictLoc}/profile";
    });
  }
}
