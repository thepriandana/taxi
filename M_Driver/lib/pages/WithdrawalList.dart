import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/pages/WithdrawalRequest.dart';

import '../controller/CWithdrawal.dart';
import '../element/WithdrawalItem.dart';
import '../model/Withdrawal.dart';

class WithdrawalList extends StatefulWidget{
  _WithdrawalList createState()=> _WithdrawalList();
}
class _WithdrawalList extends StateMVC<WithdrawalList>{
  _WithdrawalList() : super(WithdrawalController()) {
    _con = controller as WithdrawalController;
  }
  late List<Withdrawal> withdrawal = <Withdrawal>[];
  @override
  void initState() {
    super.initState();
    _con.getWithdrawalList().then((value){
      setState(() {
        withdrawal = _con.withdrawal;
        print(withdrawal);
      });
    });
  }
  late WithdrawalController _con;
  @override
  Widget build(BuildContext context) {
    return Scaffold(key: _con.scaffoldKey,body: SafeArea(
      child:
      Column(children: [
        Padding(
          padding: EdgeInsets.all(15),child: Text("Withdrawal", textAlign: TextAlign.center),
        ),
        (withdrawal.isEmpty && withdrawal.length == 0) ? Expanded(child: SizedBox()) : SizedBox(),
        (withdrawal.isEmpty && withdrawal.length == 0) ? Container(
            child: Center(
              child: Column(mainAxisSize: MainAxisSize.min,
                children: [
                  Image(image: AssetImage("lib/asset/empty.png"), height: 150,),
                  Center(child: Text("No withdrawal history yet", style: TextStyle(color: Colors.black38),)),
                  SizedBox(height: 200,)
                ],
              ),)
        ) :
        Container(
            child: Expanded(
                child: ListView.builder(
                  itemCount: withdrawal.length,
                  itemBuilder: (context, index){
                    return WithdrawalItem(withdrawal: withdrawal.elementAt(index));
                  },
                )
            )
        ),
        (withdrawal.isEmpty && withdrawal.length == 0) ? Expanded(child: SizedBox()) : SizedBox(),
        Padding(padding: EdgeInsets.all(10), child:ElevatedButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => WithdrawalRequest()));
        }, child:Text("Make a request"),)),
      ]),
    ));
  }
}
