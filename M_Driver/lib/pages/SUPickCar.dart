import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/controller/CSignUpCar.dart';

import '../GlobalSettings.dart';
import '../element/ImageJWT.dart';
import 'CameraPhoto.dart';

class SUPickCar extends StatefulWidget {
  SUPickCar({required this.cameras, this.url, required this.status});
  late List<String>? url;
  late List<CameraDescription> cameras;
  late int status;
  @override
  _SUPickCarState createState() => _SUPickCarState();
}

class _SUPickCarState extends StateMVC<SUPickCar> {
  _SUPickCarState() : super(SignUpCarController()) {
    _con = controller as SignUpCarController;
  }
  late SignUpCarController _con;
  List<String> pict = [];
  @override
  Widget build(BuildContext context) {
    List<String> imagus = widget.url ?? [""];
    return Container(
        child: Center(child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              const Text("Car Interior & Exterior", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              const SizedBox(height: 10),
              const Flexible(child: Text("We need to find out the condition\n on interior and exterior and plate identity of car.\n", textAlign: TextAlign.center)),
              const SizedBox(height: 15),
              Text("Car Images"),
              SizedBox(height: 10),
              Flexible(child: GridView.builder(itemBuilder: (BuildContext context, int index){
                if(imagus.length>index){
                  return GridTile(child: ImageJWT(url: imagus.elementAt(index)));
                }else if(imagus.length+pict.length>index){
                  return GridTile(child: Image.file(File(pict.elementAt(index-imagus.length))));
                }else if(imagus.length+pict.length == 0){
                  return GridTile(child: Image(image: AssetImage("lib/asset/empty.png")));
                }else{
                  return GridTile(child: Container());
                }
              }, itemCount: imagus.length+pict.length+1, gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 4,
                mainAxisSpacing: 4
              ))),
              SizedBox(height: 15),
              (widget.status != 2) ? ElevatedButton(onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CameraPhoto(cameras: widget.cameras, path: "car-${pict.length}", cameraNum: 0, f: addCarImage)));
              }, child: Row(mainAxisSize: MainAxisSize.min,children:[ Icon(CupertinoIcons.arrow_up), Text("Add Image")])) : SizedBox(),
              SizedBox(height: 15),
              (widget.status == -1) ? Container(child: Text("Document declined, upload new document"), padding: EdgeInsets.all(15)) : SizedBox(),
              (widget.status == 2) ? Container(child: Text("Document has been verified and accepted"), padding: EdgeInsets.all(15)) :ElevatedButton(onPressed: (){
                _con.sendCar(pict);
              }, child: Row(mainAxisSize: MainAxisSize.min,children:[ Icon(CupertinoIcons.arrow_up), Text("Upload")]))
            ]
        ))
    );
  }
  void addCarImage(){
    //refresh n add image
    pict.add("${GS.pictLoc}/car-${pict.length}");
    setState(() {});
  }
}
