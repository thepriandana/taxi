
class EmergencyContact{
  String? name;
  String? relation;
  String? phone;
  EmergencyContact();
  EmergencyContact.fromJSON(Map<String, dynamic> map){
    try{
      name = map["name"];
      relation = map["relation"];
      phone = map["phone"];
    } catch(e){

    }
  }
  Map toMap(){
    var map = <String, dynamic>{};
    map["name"] = name;
    map["relation"] = relation;
    map["phone"] = phone;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}