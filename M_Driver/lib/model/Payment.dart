class Payment{
  String? id;
  String? type;
  Payment(this.id);
  Payment.fromJSON(Map<String, dynamic> map){
    try{
      id = map["id"].toString();
      type = map["type"].toString();
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id ?? "";
    map["type"] = type ?? "";
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}