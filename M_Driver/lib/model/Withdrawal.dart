class Withdrawal{
  String? id;
  String? balance;
  String? date; //price /km
  int? status; //maximum pax on this ride class
  Withdrawal();
  Withdrawal.fromJSON(Map<String, dynamic> map){
    try{
      id = map["id"].toString();
      balance = map["balance"].toString();
      date = map["date"].toString();
      status = map["status"];
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["balance"] = balance;
    // map["date"] = date;
    map["status"] = status;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}