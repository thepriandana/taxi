class CustomeLatLng{
  double? lat;
  double? lng;
  CustomeLatLng(this.lat, this.lng);
  CustomeLatLng.fromJSON(Map<String, dynamic> map){
    try{
      lat = map["lat"];
      lng = map["lng"];
    } catch(e){

    }
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["lat"] = lat;
    map["lng"] = lng;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}