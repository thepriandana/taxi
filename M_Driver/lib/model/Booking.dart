
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'Car.dart';
import 'CustomeLatLng.dart';
import 'Payment.dart';

class Booking{
  String? id;
  String? driverName;
  String? driverID;
  int? status; //1 active, 2 cancel, 3 finish
  LatLng? startLoc;
  LatLng? endLoc;
  String? startLocName;
  String? endLocName;
  DateTime? startTime;
  DateTime? endTime;
  String? note;
  String? promoCode;
  String? payment;
  Car? driverCar;
  int? rating;
  Booking();
  Booking.fromJSON(Map<String, dynamic> map){
    try{
      status = map["status"] ?? 0;
      if(map["payment"] != null){
        payment = map["payment"]["type"].toString();
      }
      id = map["id"].toString();
      driverName = map["driverName"].toString();
      startLoc = (map["startLoc"] != null) ? LatLng(double.parse(map["startLoc"]["lat"].toString()), double.parse(map["startLoc"]["lng"].toString())) : null;
      endLoc = (map["endLoc"] != null) ? LatLng(double.parse(map["endLoc"]["lat"].toString()), double.parse(map["endLoc"]["lng"].toString())) : null;
      startLocName = map["startLocName"].toString();
      endLocName = map["endLocName"].toString();
      // startTime = DateTime.parse(map["startTime"].toString());
      // if(map["endTime"] != null) endTime = DateTime.parse(map["endTime"].toString());
      note = map["note"] ?? "";
      // promoCode = map["promoID"];
      // driverCar = Car.fromJSON(map["car"]);
      rating = map["rating"];

    } catch(e){
      print(e);
    }
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'driverName': driverName,
      'status': status,
      'startLoc': CustomeLatLng(startLoc!.latitude, startLoc!.longitude).toMap(),
      'endLoc': CustomeLatLng(endLoc!.latitude, endLoc!.longitude).toMap(),
      'startLocName': startLocName,
      'endLocName': endLocName,
      'startTime': startTime,
      'endTime': endTime,
      'note': note,
      'payment': Payment(payment).toMap(),
      'car': driverCar,
      'rating': rating,
    };
  }
  Map toMap(){
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["driverName"] = driverName;
    map["status"] = status;
    map["startLoc"] = {startLoc?.latitude, startLoc?.longitude};
    map["endLoc"] = endLoc;
    map["startLocName"] = startLocName;
    map["endLocName"] = endLocName;
    map["startTime"] = startTime;
    map["endTime"] = endTime;
    map["note"] = note;
    // promoCode = map["promoID"];
    map["payment"] = payment;
    map["car"] = driverCar?.toMap().toString();
    map["rating"] = rating;
    return map;
  }
  @override
  String toString() {
    return toMap().toString();
  }
}