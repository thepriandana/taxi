class S{
  static String defaultLanguage = 'en';
  static final Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'title': 'Taxi APP',
      'user_name': 'Name',
      'pass': 'Password',
      'email': 'Email',
      'sign_already': 'Already have an account',
      'sign_email': 'Please insert your email',
      'sign_name': 'Please insert your name',
      'sign_signup': 'Sign Up',
      'sign_in': 'Login',
      'sign_regis': 'Create an account',
      'sign_reset': 'Reset Password',
      'hint_email': 'driver@mail.com',
      'hint_pass': 'Password',
      'should_be_a_valid_email': 'Please fill with valid email',
      'should_be_more_than_3_letters': 'Please insert more than 3 letters',
      'should_be_more_than_8_characters': 'Please insert more than 7 characters',
      'should_be_less_than_balance': 'Please insert less than your balance',
      'emergency_name': 'Full Name',
      'emergency_relation': 'Relationship',
      'emergency_phone' : 'Phone'
    },
  };
  static set language(String lang) {
    defaultLanguage = lang;
  }
  static String? getText(String x){
    return _localizedValues[defaultLanguage]![x];
  }

  // static String? get language {
  //   return _localizedValues[defaultLanguage]!['language'];
  // }
}