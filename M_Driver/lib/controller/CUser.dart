
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/controller/CBooking.dart';
import 'package:taxi_driver/pages/Home.dart';
import 'package:taxi_driver/pages/Login.dart';

import '../GlobalSettings.dart';
import '../model/EmergencyContact.dart';
import '../model/User.dart';
import '../pages/SignUpAdvances.dart';
import 'RUser.dart';

class UserController extends ControllerMVC{
  static int statusLogin = 0;  //-1 Error, 0 Not login, 1 Login
  static User suser = User();
  static bool isOffline = false;
  static bool isTakingJob = false;
  static String token = "";
  late User user;
  EmergencyContact emergencyContact = EmergencyContact();
  bool hidePassword = true;
  bool loading = false;
  OverlayEntry? loader;
  late BuildContext? context;
  String getToken(){
    return token;
  }
  UserController([this.context]){
    loginFormKey = GlobalKey<FormState>();
    scaffoldKey = GlobalKey<ScaffoldState>();
    user = suser;
  }
  late GlobalKey<FormState> loginFormKey;
  late GlobalKey<ScaffoldState> scaffoldKey;
  Future<void> checkTokenSplash() async {
    String url = "${GS.base_url}/splash";
    await RUser().sendJWTCURL(url).then((value) async{
      var json = jsonDecode(value);
      if(json["message"] == "valid"){

        token = await RUser().getTokenJWT();
        await refreshData();
      }else{
        await logout();
      }
    }).onError((error, stackTrace){
      //show error
    });
  }
  Future<bool> checkLogin() async{
    var box = await Hive.openBox('taxi_app');
    String status = box.get('current_user').toString();
    return (status.isNotEmpty && status != "[]" && status != "null");
  }
  Future<void> logout() async{
    print("Logout called");
    var box = await Hive.openBox('taxi_app');;
    box.put('current_user', '');
    box.put('offerJob','');
    user = User();
    token = "";
    FlutterBackgroundService().invoke("stopService");
  }
  void registerEmergencyContact() async {
    FocusScope.of(state!.context).unfocus();
    // loader = Helper.overlayLoader(state!.context);
    if (loginFormKey.currentState!.validate()) {
      loginFormKey.currentState!.save();
      // Overlay.of(state!.context)!.insert(loader!);
      RUser().sendJWTCURL("${GS.base_url}/driver/emergencyProfile", emergencyContact.toMap()).then((value){
        // Map<String, dynamic> json = jsonDecode(value.toString());
        ScaffoldMessenger.of(state!.context).showSnackBar(
          const SnackBar(
            content: Text("Saved"),
          ),
        );
      }).catchError((e) {
        // Helper.hideLoader(loader!);
        // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
        ScaffoldMessenger.of(state!.context).showSnackBar(
          SnackBar(
            content: Text(e.toString()),
          ),
        );
      }).whenComplete(() {
        // Helper.hideLoader(loader!);
      });
    }
  }
  bool getOfflineOnline(){
    return isOffline;
  }
  void setOffline(bool e){
    isOffline = e;
  }
  Future<void> enabledDisabled() async{
    bool enabled = !isOffline;
    RUser().sendJWTCURL("${GS.base_url}/driver/enabled", enabled).then((value){
      var obj = jsonDecode(value);
      // isOffline = obj["message"];
      setOffline(enabled);
      //set either start service or stop service
    });
  }
  Future<void> refreshData() async{
    FlutterBackgroundService().invoke("stopService");
    await RUser().sendJWTCURL("${GS.base_url}/driver/refresh").then((value) async {
      var box = await Hive.openBox('taxi_app');
      await box.put("current_user", value.toString());

      var obj = jsonDecode(value);
      isOffline = obj["message"]["offline"];
      isTakingJob = obj["message"]["takingJob"];
      if(isTakingJob){

      }
      FlutterBackgroundService().startService();
      changeUsertoPreference();
    }).catchError((e) {

    });
  }
  void register() async{
    FocusScope.of(state!.context).unfocus();
    // loader = Helper.overlayLoader(state!.context);
    if (loginFormKey.currentState!.validate()) {
      loginFormKey.currentState!.save();
      // Overlay.of(state!.context)!.insert(loader!);
      RUser().sendCURL("${GS.base_url}/driver/signup", user.toMap()).then((value){
        // Map<String, dynamic> json = jsonDecode(value.toString());
        ScaffoldMessenger.of(state!.context).showSnackBar(
          const SnackBar(
            content: Text("Registration submitted"),
          ),
        );
        login();
      }).catchError((e) {
        // Helper.hideLoader(loader!);
        // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
        ScaffoldMessenger.of(state!.context).showSnackBar(
          SnackBar(
            content: Text(e.toString()),
          ),
        );
      }).whenComplete(() {
        // Helper.hideLoader(loader!);
      });
    }
  }
  void login() async{
    FocusScope.of(state!.context).unfocus();
    // loader = Helper.overlayLoader(state!.context);
    if (loginFormKey.currentState!.validate()) {
      loginFormKey.currentState!.save();
      // Overlay.of(state!.context)!.insert(loader!);
      RUser().sendCURL("${GS.base_url}/driver/signin", user.toMap()).then((value) async {
        var obj = jsonDecode(value)["message"];
        var box = await Hive.openBox('taxi_app');;
        box.put('current_user', value.toString());
        box.put('offerJob', "");
        isOffline = obj["offline"];
        isTakingJob = obj["takingJob"];
        token = await RUser().getTokenJWT();

        // final service = ;
        // var isRunning = await service.isRunning();
        // if (isRunning) {
        //   service.invoke("stopService");
        // } else {
        // await FlutterBackgroundService().startService();
        // }

        changeUsertoPreference();
        afterLoginPage(state!.context);
        FlutterBackgroundService().startService();
      }).catchError((e) {
        // Helper.hideLoader(loader!);
        // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
        ScaffoldMessenger.of(state!.context).showSnackBar(
          SnackBar(
            content: Text(e.toString()),
          ),
        );
      }).whenComplete(() {
        // Helper.hideLoader(loader!);
      });
    }
  }
  Future<void> changeUsertoPreference() async{

    var box = await Hive.openBox('taxi_app');;
    String status = box.get('current_user').toString();
    var obj = jsonDecode(status);
    if(isTakingJob){
      BookingController().findLastActiveBooking();
    }
    suser.name = obj["message"]["profile"]["name"];
    suser.email = obj["message"]["profile"]["email"];
    user = suser;
  }
  void afterLoginPage(BuildContext mcontext) async{
    var box = await Hive.openBox('taxi_app');;
    String status = box.get('current_user').toString();
    if(status.isEmpty){
        Navigator.of(mcontext).pushReplacement(
            MaterialPageRoute(builder: (context) => Login()));
    }else{
      var obj = jsonDecode(status);
      if(obj == "null" || obj == "[]" || obj == null || status == "" || token == "" ) {
        Navigator.of(mcontext).pushReplacement(
            MaterialPageRoute(builder: (context) => Login()));
      }else{
        obj = obj["message"];
        if(obj["profileComplete"]  == false){
          Navigator.of(mcontext).pushReplacement(MaterialPageRoute(builder: (context) => SignUpAdvances()));
        }else if(obj["verified"] == false){
          Navigator.of(mcontext).pushReplacement(MaterialPageRoute(builder: (context) => SignUpAdvances()));
        }else{
           await Geolocator.getLastKnownPosition().then((value){
             LatLng lastX = LatLng(value!.altitude, value.longitude);
             Navigator.of(mcontext).pushReplacement(MaterialPageRoute(builder: (context) => Dashboard(lastLocation: lastX,)));
          });
        }
      }
    }
  }
  void setLoginStatus(int status){
    statusLogin = status;
  }
  int getLoginStatus(){
    return statusLogin;
  }
  void setTakingJob(bool value){
    isTakingJob = value;
  }
  bool getTakingJob(){
    return isTakingJob;
  }
}