import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:taxi_driver/controller/CBooking.dart';
import 'package:taxi_driver/controller/RUser.dart';

import '../GlobalSettings.dart';
class SignUpCarController extends ControllerMVC{
  final BookingController _bc = BookingController();

  late GlobalKey<FormState> loginFormKey;
  late List<Widget> fileListThumb;
  List<File> fileList = [];
  //------------END OF INITIATION-------------
  List<DropdownMenuItem<String>> getDropdownList(){
    List<DropdownMenuItem<String>> tmp = [];
    for(var owo in _bc.getAllRideClass()){
      tmp.add(DropdownMenuItem(value: owo.id, child: Text("${owo.className}")));
    }
    return tmp;
  }
  List<Map> toBase64(List<File> fileList){
    List<Map> s = [];
    if(fileList.isNotEmpty) {
      for (var element in fileList) {
        Map a = {
          'fileName': basename(element.path),
          'encoded' : base64Encode(element.readAsBytesSync())
        };
        s.add(a);
      }
    }
    return s;
  }
  Future pickFiles() async{
    List<Widget> thumbs = [];
    for (var element in fileListThumb) {
      thumbs.add(element);
    }
    await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'png', 'jpeg', 'bmp'],
    ).then((files){
      if(files != null && files.count>0){
        for (var element in files.files) {
          List<String> picExt = ['.jpg', '.jpeg', '.bmp'];
          if(picExt.contains(extension(element.path!))){
            thumbs.add(Padding(
                padding: const EdgeInsets.all(1),
                child: Image.file(File(element.path!))
            )
            );
          } else {
            thumbs.add( Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children:<Widget>[
                  const Icon(Icons.insert_drive_file),
                  Text(extension(element.path!))
                ]
            ));
          }
          fileList.add(File(element.path!));
        }
        fileListThumb = thumbs;
      }
    });
  }


  void sendSIM(){
    RUser().sendJWTFile("${GS.base_url}/driver/SIM", "${GS.pictLoc}/sim").then((value){
      var json = jsonDecode(value);
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(json["message"].toString()),
        ),
      );
      Navigator.of(state!.context).pop();
    }).catchError((e) {
      // Helper.hideLoader(loader!);
      // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(e.toString()),
        ),
      );
    });
  }
  void sendSTNK(List<String> stnk) async{
    await RUser().sendJWTMultiFile("${GS.base_url}/driver/STNK", stnk).then((value){
      var json = jsonDecode(value);
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(json["message"].toString()),
        ),
      );
      // Navigator.of(state!.context).pop();
    }).catchError((e) {
      // Helper.hideLoader(loader!);
      // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(e.toString()),
        ),
      );
    });
  }
  void sendPP(){
    RUser().sendJWTFile("${GS.base_url}/driver/photoProfile", "${GS.pictLoc}/profile").then((value){
      var json = jsonDecode(value);
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(json["message"].toString()),
        ),
      );
      // Navigator.of(state!.context).pop();
    }).catchError((e) {
      // Helper.hideLoader(loader!);
      // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(e.toString()),
        ),
      );
    });
  }
  void sendCar(List<String> car) async{
    await RUser().sendJWTMultiFile("${GS.base_url}/driver/Car", car).then((value){
      var json = jsonDecode(value);
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(json["message"].toString()),
        ),
      );
      // Navigator.of(state!.context).pop();
    }).catchError((e) {
      // Helper.hideLoader(loader!);
      // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(e.toString()),
        ),
      );
    });
  }
  void sendAccept() async {
    bool accepted = true;
    await RUser().sendJWTCURL("${GS.base_url}/driver/acceptAndsubmit", accepted).then((value){
      var json = jsonDecode(value);
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(json["message"].toString()),
        ),
      );
      // Navigator.of(state!.context).pop();
    }).catchError((e) {
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text(e),
        ),
      );
    });
  }

  Future<String> getSignupStatus() async {
    String status = "";
    status = await RUser().sendJWTCURL("${GS.base_url}/driver/signupStatus");
    return status;
  }
}