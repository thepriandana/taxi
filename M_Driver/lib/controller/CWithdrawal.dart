import 'dart:convert';
// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../GlobalSettings.dart';
import '../model/Withdrawal.dart';
import 'RUser.dart';

class WithdrawalController extends ControllerMVC {
  List<Withdrawal> withdrawal = <Withdrawal>[];
  WithdrawalController() {
    scaffoldKey = GlobalKey<ScaffoldState>();
  }
  late GlobalKey<ScaffoldState> scaffoldKey;
  Future<void> getWithdrawalList() async {
    String url = "${GS.base_url}/driver/withdrawal";
    List<Withdrawal> p = await RUser().sendJWTCURL(url).then((value){
      if(value != '{"message":[]}'){
        var objJson = jsonDecode(value)["message"] as List;
        List<Withdrawal> preList = objJson.map((a) => Withdrawal.fromJSON(a)).toList();
        return preList;
      }
      return [];
    });
    setState(() {
      withdrawal = p;
    });
  }
  Future<void> sendRequest(num balance) async{
    String url = "${GS.base_url}/driver/withdrawal";
    await RUser().sendJWTCURL(url, balance).then((value){
      var jsonX = jsonDecode(value);
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text("${jsonX["message"]}".toString()),
        ),
      );
    }).catchError((e) {
      // Helper.hideLoader(loader!);
      // Map<String, dynamic> json = jsonDecode(e.toString().replaceFirst("Exception:", ""));
      ScaffoldMessenger.of(state!.context).showSnackBar(
        SnackBar(
          content: Text("Something error"),
        ),
      );
    });
  }
}