import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../GlobalSettings.dart';
import '../main.dart';
import '../model/Notification.dart';
import 'RUser.dart';
import 'CUser.dart';

class NotificationController extends ControllerMVC {
  List<UserNotification> notification = <UserNotification>[];
  NotificationController() {
    scaffoldKey = new GlobalKey<ScaffoldState>();
  }
  UserController uc = UserController();
  late GlobalKey<ScaffoldState> scaffoldKey;
  void getNotification() async {
    String url = "${GS.base_url}/notif";
    List<UserNotification> p = await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<UserNotification> preList = objJson.map((a) => UserNotification.fromJSON(a)).toList();
      return preList;
    });
    setState(() {
      notification = p;
    });
  }



}