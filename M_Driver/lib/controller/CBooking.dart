import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../GlobalSettings.dart';
import '../model/Booking.dart';
import '../model/CustomeLatLng.dart';
import '../model/Payment.dart';
import '../model/RideClass.dart';
import 'CMap.dart';
import 'RUser.dart';
import 'CUser.dart';

class BookingController extends ControllerMVC {
  static List<Booking> booking = <Booking>[];
  static List<Payment> payment = <Payment>[];
  static List<RideClass> rideClass = <RideClass>[];
  static Booking tmpBooking = Booking();
  static Booking offerJob = Booking();
  late BuildContext? context;
  UserController _ucon = UserController();
  BookingController([BuildContext? contextX]) {
    scaffoldKey = new GlobalKey<material.ScaffoldState>();
    context = contextX;

  }
  late GlobalKey<material.ScaffoldState> scaffoldKey;
  Future<void> getBookingAll() async {
    String url = "${GS.base_url}/booking";
    List<Booking> bookings = await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value) as List;
      List<Booking> preList = objJson.map((a) => Booking.fromJSON(a)).toList();
      return preList;
    });
    booking = bookings;
  }
  Future<void> cancelLastBooking() async{
    String url = "${GS.base_url}/booking/cancel";
    await RUser().sendJWTCURL(url).then((value) {
      if(jsonDecode(value)["message"] != "Nothing"){
        //success
        // statusJob = "-1";
      }else{
        //nothing found
      }
    });
  }
  Future<Booking> getBookingByID(String uid) async {
    String url = "${GS.base_url}/booking/$uid";
    Booking p = await RUser().sendJWTCURL(url).then((value){
      var objJson = jsonDecode(value);
      return Booking.fromJSON(objJson);
    });
    return p;
  }
  Future<void> refreshPaymentMethod() async {
    String url = "${GS.base_url}/paymentM";
    payment = await RUser().sendCURL(url).then((value) async{
      var objJson = jsonDecode(value) as List;
      List<Payment> preList = objJson.map((a) => Payment.fromJSON(a)).toList();
      return preList;
    });
  }

  Future<void> refreshRideClassMethod() async {
    String url = "${GS.base_url}/rideclass";
    rideClass = await RUser().sendCURL(url).then((value) async{
      var json = jsonDecode(value) as List;
      List<RideClass> preList = json.map((a) => RideClass.fromJSON(a)).toList();
      return preList;
    });
  }
  Future<Booking> findLastActiveBooking() async{
    String url = "${GS.base_url}/booking/active";
    Booking last = await RUser().sendJWTCURL(url).then((value) {
      if(jsonDecode(value)["message"] != "Nothing"){
        Map<String, dynamic> map = jsonDecode(value)["message"];
        Booking tmp = Booking.fromJSON(map);
        setTmpBooking(tmp);
        return tmp;
      }else{
        offerJob = Booking();
        return Booking();
      }
    });
    return last;
  }
  Booking getTmpBooking(){
    return tmpBooking;
  }
  setTmpBooking(Booking b){
    tmpBooking = b;
  }
  clearTmpBooking(){
    tmpBooking = Booking();
  }
  List<String> getAllRideClassToString(){
    return rideClass.map((e) => e.className.toString() + "-"+e.pax.toString()).toList();
  }
  List<RideClass> getAllRideClass(){
    return rideClass;
  }
  ////////////////
  Future<void> sendLastLocation(Position position) async{
    if(_ucon.getTakingJob() == true) { //delete later, so it can send wheter it taking job or not
      // position.heading for the compas
      CustomeLatLng latLng = CustomeLatLng(position.latitude, position.longitude);
      await RUser().sendJWTCURL("${GS.base_url}/booking/updateLastLocation", latLng.toMap());
    }
  }
  Future<void> takeJob(String idx) async{
    if(_ucon.getOfflineOnline() == false && _ucon.getTakingJob() == false){
      String id = idx;
      await RUser().sendJWTCURL("${GS.base_url}/booking/takejob", id).then((value) {
        var msg = jsonDecode(value);
        if(msg["message"] == "Success"){
          _ucon.setTakingJob(true);
          Navigator.of(context!).pop();
        }
      });
    }
  }
  Future<void> findNearbyJob() async{
    if(_ucon.getOfflineOnline() == false && _ucon.getTakingJob()){
      await Geolocator.getLastKnownPosition().then((value) async{
        print(value.toString());
        CustomeLatLng latLng = CustomeLatLng(value!.latitude, value!.longitude);
        RUser().sendJWTCURL("${GS.base_url}/booking/job", latLng.toMap()).then((value2) async{
          var box = Hive.box('taxi_app');
          if(value2.isNotEmpty){
            box.put('offerJob', value2);
            // const AndroidNotificationDetails androidNotificationDetails =
            // AndroidNotificationDetails('22313', 'pg.taxi_app.driver.taxi_driver',
            //     channelDescription: 'X',
            //     importance: Importance.max,
            //     priority: Priority.max,
            //     ticker: 'ticker');

            // const NotificationDetails notificationDetails =
            // NotificationDetails(android: androidNotificationDetails);
            // await flutterLocalNotificationsPlugin.show(
            //     0, 'Taxi Driver', 'New offer job found', notificationDetails,
            // payload: booking.
            // );
          }else{
            box.put("offerJob", "");
          }
        });
      });
    }
  }
  Future<void> pickUp() async{
    RUser().sendJWTCURL("${GS.base_url}/booking/pickUp").then((value) async {
      tmpBooking.status = 91;
    });
  }
  Future<void> finishJob() async{
    RUser().sendJWTCURL("${GS.base_url}/booking/finishJob").then((value) async {
      //clear tmpBooking
      tmpBooking = Booking();
      UserController().setTakingJob(false);
    });
  }
}