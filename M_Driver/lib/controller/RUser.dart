import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:asuka/asuka.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
class RUser{
  bool isLog = true;
  int timeout = 15;
  Future<String> sendJWTFile(String url, String path) async{
    final client = new http.Client();
    try{
      String token = await getTokenJWT();
      var request = http.MultipartRequest("POST", Uri.parse("$url"));
      request.headers.addAll({"Authorization": "Bearer "+token.trim().replaceAll("\ufffd", "")});

      String mime = "${lookupMimeType(path, headerBytes: [0xFF, 0xD8])}";
      request.files.add(await http.MultipartFile.fromPath("file", path, contentType: MediaType.parse(mime)));
      var mesend = await request.send();
      var response = await http.Response.fromStream(mesend);
      if(isLog == true) print(response.body);
      if (response.statusCode == 200) {
          return response.body;
      } else {
          return response.body;
      }
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e'); throw new Exception(e);
    } finally{
      client.close();
    }
  }
  Future<String> sendJWTMultiFile(String url, List<String> paths) async{
    final client = new http.Client();
    try{
      String token = await getTokenJWT();
      var request = http.MultipartRequest("POST", Uri.parse("$url"));
      request.headers.addAll({"Authorization": "Bearer "+token.trim().replaceAll("\ufffd", "")});
      for(String path in paths){
        String mime = "${lookupMimeType(path, headerBytes: [0xFF, 0xD8])}";
        request.files.add(await http.MultipartFile.fromPath("file", path, contentType: MediaType.parse(mime)));
      }
      var mesend = await request.send();
      var response = await http.Response.fromStream(mesend);
      if(isLog == true) print(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return response.body;
      }
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e');
      if(e.toString().contains('"message":')){
        throw Exception(jsonDecode(e.toString())["message"]);
      }else{
        throw Exception(e);
      }
    } finally{
      client.close();
    }
  }
  Future<String> sendJWTCURL(String url, [Object? obj]) async{
    try {
      Uri uri = Uri.parse("$url");
      String token = await getTokenJWT();
      final client = new http.Client();
      Map<String, String> requestHeaders = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': "Bearer "+token.trim().replaceAll("\ufffd", "")
      };
      if(isLog == true) print(uri.toString());
      var response;
      if(obj!=null){
        response = await client.post(
          uri,
          headers: requestHeaders,
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
        if(isLog == true) print(json.encode(obj));
      }else{
        response = await client.get(
          uri,
          headers: requestHeaders,
        ).timeout(Duration(seconds: timeout));
      }
      if(isLog == true) print(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        if(response.body.toString().contains('"message":')){
          return jsonDecode(response.body.toString())["message"];
        }else{
          return response.body;
        }
      }
    } on TimeoutException catch (e) {
      if(isLog == true) print('Timeout Error: $e');
      // Fluttertoast.showToast(msg: S.errno_timeout);
      throw new Exception(e);
    } on SocketException catch (e) {
      if(isLog == true) print('Socket Error: $e');
      throw new Exception(e);
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e');
      throw new Exception(e);
    }
  }
  Future<String> sendCURL(String url, [Object? obj]) async{
    try{
      Uri uri = Uri.parse("$url");
      final client = new http.Client();
      Map<String, String> requestHeaders = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
      if(isLog == true) print(uri.toString());
      var response;
      if(obj!=null){
        response = await client.post(
          uri,
          headers: requestHeaders,
          body: json.encode(obj),
        ).timeout(Duration(seconds: timeout));
        if(isLog == true) print(json.encode(obj));
      }else{
        response = await client.get(
          uri,
          headers: requestHeaders,
        ).timeout(Duration(seconds: timeout));
      }
      if(isLog == true) print(response.body);
      if (response.statusCode == 200) {
        if(isLog == true) print("200 Body");
        return response.body;
      } else {
        if(isLog == true) print("${response.statusCode} Body");
        throw new Exception(response.body);
      }
    } on TimeoutException catch (e) {
      if(isLog == true) print('Timeout Error: $e');
      // Fluttertoast.showToast(msg: S.errno_timeout);
      throw new Exception(e);
    } on SocketException catch (e) {
      if(isLog == true) print('Socket Error: $e');
      throw new Exception(e);
    } on Error catch (e) {
      if(isLog == true) print('General Error: $e');
      throw new Exception(e);
    }
  }
  Future<String> getTokenJWT() async{
    var box = Hive.box('taxi_app');
    var j = box.get("current_user").toString();
    return  json.decode(j)["message"]["token"].toString();
  }
  Future<String> getUserID() async{
    String j = await getTokenJWT();
    String token = j.split(".")[1];
    return  token;
  }
}
