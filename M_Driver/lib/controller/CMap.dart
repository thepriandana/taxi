
import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:http/http.dart' as http;

import '../GlobalSettings.dart';
import '../model/PlaceX.dart';
import 'CBooking.dart';

class MapsController extends ControllerMVC {
  BitmapDescriptor? carIcon;
  BitmapDescriptor? originIcon;
  BitmapDescriptor? destinationIcon;
  List<Marker> customMarkers = [];
  static double screenSize =0;
  static bool currentLocD = false;
  static bool currentLocO = false;
  late LatLng position;
  static Position? mPosition = Position(longitude: 0, latitude: 0, timestamp: DateTime.now(), accuracy: 0, altitude: 0, heading:0, speed: 0, speedAccuracy: 0);
  late CameraPosition cameraPosition;
  late List<PlacesX> placeX = <PlacesX>[];
  late BuildContext context;
  late final BookingController _bookingController = BookingController();
  // static List<Polyline> polyline = <Polyline>[];
  Function superFunction;
  MapsController(BuildContext buildContext, LatLng latLng, this.superFunction){
    position = latLng;
    cameraPosition  = CameraPosition(target: latLng);
    context = buildContext;
    scaffoldKey = GlobalKey<ScaffoldState>();
  }
  late GlobalKey<ScaffoldState> scaffoldKey;
  Future<void> getLocation() async {
    late LocationSettings locationSettings;
    if (defaultTargetPlatform == TargetPlatform.android) {
      locationSettings = AndroidSettings(
          accuracy: LocationAccuracy.high,
          distanceFilter: 1,
          // forceLocationManager: true,
          intervalDuration: const Duration(seconds: 5),
      );
    } else if (defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.macOS) {
      locationSettings = AppleSettings(
        accuracy: LocationAccuracy.high,
        activityType: ActivityType.fitness,
        distanceFilter: 1,
        pauseLocationUpdatesAutomatically: true,
        // Only set to true if our app will be started up in the background.
        showBackgroundLocationIndicator: false,
      );
    } else {
      locationSettings = const LocationSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 100,
      );
    }
    Geolocator.getPositionStream(locationSettings: locationSettings).listen((Position? _position) async {
     if(_position != null) {
       position = LatLng(_position.latitude, _position.longitude);
       mPosition = _position;
       _bookingController.sendLastLocation(_position);
     }
     updateMapX();
   });
  }
  Future<void> updateMapX() async {
    late List<Marker> tmp = [];
    // if (BookingController.statusFinder == "-1" || BookingController.statusFinder == "0" || BookingController.statusFinder == "3") {
      // List<Car> carAround = await getCarAround(LatLng(position.latitude, position.longitude));
      //check if the user is offline or online, if on, check if it taking job or not

        tmp.add(Marker(markerId: MarkerId("c${tmp.length}"),
            position: LatLng(position.latitude, position.longitude), rotation: mPosition!.heading, icon: carIcon!));

      if(BookingController.tmpBooking.startLoc != null) tmp.add(Marker(markerId: MarkerId("Origin"), position: BookingController.tmpBooking.startLoc!, icon: originIcon!));
      if(BookingController.tmpBooking.endLoc != null) tmp.add(Marker(markerId: MarkerId("Destination"), position: BookingController.tmpBooking.endLoc!, icon: destinationIcon!));
      customMarkers = tmp;
    // } else if (BookingController.statusFinder == "1") {
    //   tmp.add(Marker(markerId: MarkerId("Origin"), position: BookingController.tmpBooking.startLoc!, icon: originIcon!));
    //   tmp.add(Marker(markerId: MarkerId("Destination"), position: BookingController.tmpBooking.endLoc!, icon: destinationIcon!));
    //   tmp.add(Marker(markerId: MarkerId("Current"), position: LatLng(position.latitude, position.longitude)));
    //   // tmp.add(Marker(markerId: MarkerId("Partner"), position: LatLng(BookingController.tmpBooking.driverCar!.lat!, BookingController.tmpBooking.driverCar!.lng!), icon: carIcon!));
    //   customMarkers = tmp;
    // }
    cameraPosition = await moveCameraAfter(tmp);
    superFunction();
  }

  Future<LatLng> getPlaceInfo(String placeID) async {
    Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/place/details/json?fields=name%2Cgeometry&place_id=$placeID&key="+GS.aizaKey);
    final client = new http.Client();
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json'
    };
    print("$uri");
    var response = await client.get(
      uri,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      var objJson = await jsonDecode(response.body);
      if(objJson["status"] == "OK"){
        // print("retrive:"+objJson["result"]["geometry"]["location"]["lat"].toString()+","+objJson["result"]["geometry"]["location"]["lng"].toString());
        return LatLng(objJson["result"]["geometry"]["location"]["lat"], objJson["result"]["geometry"]["location"]["lng"]);
      }else{
        return LatLng(-1, -1);
      }
    } else {
      throw new Exception(response.body);
    }
  }
  setCustomeMarkers(List<Marker> _m){
    customMarkers = _m;
  }
  double calculateD(LatLng ax, LatLng bx) {
    var R = 6371;
    var dLat = (ax.latitude - bx.latitude) * pi / 180;
    var dLon = (ax.longitude - bx.longitude) * pi / 180;
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(ax.latitude*  pi / 180) * cos(bx.latitude * pi / 180) *
            sin(dLon / 2) * sin(dLon / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    return R * c;
  }
  double calculateZoom(double widthPixel,double ratio,double lat,double length){
    length = length *1000;
    double k = widthPixel * 156543.03392 * cos(lat * pi / 180);
    double myZoom = ( log( (ratio * k)/(length*100) )/ln2 );
    return(myZoom);
  }
  Future<void> getLastLocation() async{
    var box = Hive.box('taxi_app');
    String lastLocation = box.get("last_location").toString();
    if(lastLocation.isNotEmpty && lastLocation != "[]" && lastLocation != "null"){
      var json = jsonDecode(lastLocation);
      LatLng location = LatLng(json[0], json[1]);
      cameraPosition = CameraPosition(target: location, zoom: 14.4746);
    }else{
      cameraPosition = CameraPosition(target: LatLng(0,0));
    }
  }
  Future<CameraPosition> moveCameraAfter(List<Marker> m) async {
    double latMin = 0, latMax=0, lngMax = 0, lngMin = 0;
    if (m.length <= 1) {
      return CameraPosition(target: LatLng(mPosition!.latitude, mPosition!.longitude), zoom:14.4746 );
    }
    for (var i = 0; i < m.length; i++) {
      if(i==0){
        latMin = m.elementAt(i).position.latitude;
        latMax = m.elementAt(i).position.latitude;
        lngMin = m.elementAt(i).position.longitude;
        lngMax = m.elementAt(i).position.longitude;
      }else{
        if(latMin>m.elementAt(i).position.latitude){
          latMin = m.elementAt(i).position.latitude;
        }
        if(latMax<m.elementAt(i).position.latitude){
          latMax = m.elementAt(i).position.latitude;
        }
        if(lngMin>m.elementAt(i).position.longitude){
          lngMin = m.elementAt(i).position.longitude;
        }
        if(lngMax<m.elementAt(i).position.longitude){
          lngMax = m.elementAt(i).position.longitude;
        }
      }
    }
    if(screenSize == 0){
      screenSize = MediaQuery.of(context).size.width;
    }
    double latX= (latMin + latMax) / 2,
        dis = calculateD(LatLng(latMin, lngMin), LatLng(latMax, lngMax)),
        jom = calculateZoom(screenSize, 90, latX, dis);
    // final GoogleMapController controller = await mController.future;
    return CameraPosition(
        target: LatLng((latMin+latMax)/2,(lngMin+lngMax)/2),
        zoom: jom);
  }
  Future<Polyline> getPolyline(LatLng ori, LatLng des) async{
    // check if polly line is empty
    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(GS.aizaKey,
      PointLatLng(ori.latitude, ori.longitude),
      PointLatLng(des.latitude, des.longitude),
      travelMode: TravelMode.driving,
    ).then((value){
      print("Polyline requested");
      return value;
    });

    List<LatLng> polylineCoordinates = [];
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    Polyline _polyline = Polyline(polylineId:  PolylineId("poly"),
        color: Colors.blue, points: polylineCoordinates, width: 3);
    // polyline
    // polyline.add(_polyline);
    return _polyline;
  }
}