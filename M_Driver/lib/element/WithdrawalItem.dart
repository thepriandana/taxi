import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../model/Withdrawal.dart';

class WithdrawalItem extends StatelessWidget {
  WithdrawalItem({required this.withdrawal});
  Withdrawal withdrawal;
  @override
  Widget build(BuildContext context) {
    String status;
    if(withdrawal.status == 0){
      status = "requested";
    }else if(withdrawal.status == 1){
      status = "Accepted";
    }else{
      status = "Denied";
    }

    return Container(padding: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.grey.shade200),
        margin: EdgeInsets.only(bottom: 10, left: 15, right: 15),
        child: Row(children: [
      Expanded(child: Text("Rp "+(withdrawal.balance??"0"))),
      Text("$status")
    ]));
  }
}
