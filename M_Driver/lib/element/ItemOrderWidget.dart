import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../model/Booking.dart';

class ItemOrderWidget extends StatelessWidget{
  ItemOrderWidget({required this.booking});
  late Booking booking;
  late String status = "";
  late Color color = Colors.blueAccent;
  @override
  Widget build(BuildContext context){
    switch(booking.status){
      case 0: status = "REQUESTED"; color = Colors.blueAccent; break;
      case 1: status = "ACTIVE"; color = Colors.green; break;
      case 2: status = "CANCELED"; color = Colors.grey; break;
      case 3: status = "FINISHED"; color = Colors.grey; break;
      default: status = ""; break;
    }

    DateFormat dateFormat = DateFormat("dd MMM yyyy");
    DateFormat timeFormat = DateFormat("HH:mm");
    return Container(
      child: Card(
        margin: EdgeInsets.only(top: 10, left: 15, right: 15),
        child: Container(padding: EdgeInsets.all(10),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey.shade300, width: 0.3)),
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(child: Text("$status", style: TextStyle(color: Colors.white)) ,
                    decoration: BoxDecoration(color: color, borderRadius: BorderRadius.circular(3)),
                  padding: EdgeInsets.only(top: 2, bottom: 2, left: 5, right: 5),
                ),
                // Expanded(child: Text("${dateFormat.format(this.booking.startTime!)}", textAlign: TextAlign.end,))
              ],
            ),
            SizedBox(height: 7,),
            Row(children: [
              Icon(Icons.circle, size: 15,color: Colors.orange,),
              SizedBox(width: 5),
              Expanded(child: Text("${booking.startLocName}", style: TextStyle(fontWeight: FontWeight.bold)))
            ],),
            SizedBox(height: 5,),
            Row(children: [
              Icon(Icons.circle, size: 15,color: Colors.orange),
              SizedBox(width: 5),
              Expanded(child: Text("${booking.endLocName}", style: TextStyle(fontWeight: FontWeight.bold)))
            ],),
            SizedBox(height: 3),
            Divider(),
            SizedBox(height: 3),
            Row(children: [
              Icon(Icons.timer_sharp, size: 18),
              SizedBox(width: 3),
              // Text("${timeFormat.format(this.booking.startTime!)}"),
              SizedBox(width: 7),
              Icon(Icons.local_taxi, size: 18),
              SizedBox(width: 3),
              Expanded(child: Text("${booking.driverCar?.idRideClass}")),
              Icon(Icons.credit_card, size: 18),
              SizedBox(width: 3),
              Text("${booking.payment}")
            ],)
          ],
        ),)
      )
    );
  }
}