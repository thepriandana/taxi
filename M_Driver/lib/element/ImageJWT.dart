import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxi_driver/controller/CUser.dart';

import '../GlobalSettings.dart';

class ImageJWT extends StatefulWidget {
  ImageJWT({Key? key, required this.url}) : super(key: key);
  late String url;
  @override
  State<ImageJWT> createState() => _ImageJWTState();
}

class _ImageJWTState extends State<ImageJWT> {
  @override
  Widget build(BuildContext context) {
    late String url = widget.url;
    String token = UserController().getToken();
    return Image.network("${GS.base_url}/driver/files/$url", headers: {'Authorization': "Bearer "+token.trim().replaceAll("\ufffd", "")},
        fit: BoxFit.fill,
        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
          if (loadingProgress == null) return child;
          return Center(
              child: CircularProgressIndicator( value: loadingProgress.expectedTotalBytes != null
                  ? loadingProgress.cumulativeBytesLoaded /
                  loadingProgress.expectedTotalBytes! : null));

        });
  }
}
