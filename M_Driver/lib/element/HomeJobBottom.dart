import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxi_driver/controller/CBooking.dart';

class HomeJobBottom extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return
      Card(child:Container(color: Colors.white,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(left: 10, right: 20, bottom: 10, top: 15),
      child: Column(mainAxisSize: MainAxisSize.max,
          children: [
            Row(children: [
              const CircleAvatar(backgroundColor: Colors.green,child: Icon(CupertinoIcons.car, size: 24, color: Colors.white,),),
              const SizedBox(width: 10),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text("Ordered By", style: TextStyle(color: Colors.black54, fontSize: 16)),
                Text("CUSTOMER NAME", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))
              ]),
            ]),
            const Divider(),
            Row(children: [
              const CircleAvatar(backgroundColor: Colors.blue,child: Icon(CupertinoIcons.map_pin_ellipse, size: 24, color: Colors.white),),
              const SizedBox(width: 10),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text("Location Name", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold)),
                Text("Location Address", style: TextStyle(color: Colors.black54, fontSize: 16))
              ]),
            ]),
            SizedBox(height: 10,),
            Row(children: [
              Expanded(child: ElevatedButton(child: (BookingController().getTmpBooking().status == 1) ? Text("PICK UP") : Text("Finish"), onPressed: (){
                if(BookingController().getTmpBooking().status == 1){
                  BookingController().pickUp();
                }else{
                  BookingController().finishJob();
                }

              })),
              SizedBox(width: 10,),
              Container(padding: EdgeInsets.only(left:5, right:5),
                  child: Icon(CupertinoIcons.ellipsis_vertical))
            ],)
        ])
    ));
  }
}
