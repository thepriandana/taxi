import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserListComponent } from './components/user-list/user-list.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { httpInterceptorProviders } from './helper/http.interceptor';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { HomeComponent } from './components/home/home.component';
import { BookingComponent } from './components/booking/booking.component';
import { PaymentMethodComponent } from './components/payment-method/payment-method.component';
import { HistoryComponent } from './components/history/history.component';
import { NotificationComponent } from './components/notification/notification.component';
import { CouponComponent } from './components/coupon/coupon.component';
import { TakejobComponent } from './components/takejob/takejob.component';
import { BookingDetailsComponent } from './components/booking-details/booking-details.component';
import { NavSideComponent } from './components/nav-side/nav-side.component';
import { NavTopComponent } from './components/nav-top/nav-top.component';
import { CarComponent } from './components/car/car.component';
import { RideclassComponent } from './components/rideclass/rideclass.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { WithdrawalsComponent } from './components/withdrawals/withdrawals.component';
import { CarDetailsComponent } from './components/car-details/car-details.component';
import { SecureImagePipe } from './components/secure-image.pipe';
import { WithdrawalsDetailComponent } from './components/withdrawals-detail/withdrawals-detail.component';
import { PaymentsDetailComponent } from './components/payments-detail/payments-detail.component';
import { DriverListComponent } from './components/driver-list/driver-list.component';
import { DriverDetailComponent } from './components/driver-detail/driver-detail.component';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  declarations: [
    AppComponent,
    UserAddComponent,
    UserDetailComponent,
    UserListComponent,
    UserLoginComponent,
    HomeComponent,
    BookingComponent,
    PaymentMethodComponent,
    HistoryComponent,
    NotificationComponent,
    CouponComponent,
    TakejobComponent,
    BookingDetailsComponent,
    NavSideComponent,
    NavTopComponent,
    CarComponent,
    RideclassComponent,
    PaymentsComponent,
    WithdrawalsComponent,
    CarDetailsComponent,
    SecureImagePipe,
    WithdrawalsDetailComponent,
    WithdrawalsDetailComponent,
    PaymentsDetailComponent,
    DriverListComponent,
    DriverDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  LeafletModule,
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
