import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookingComponent } from './components/booking/booking.component';
import { HistoryComponent } from './components/history/history.component';
import { HomeComponent } from './components/home/home.component';
import { PaymentMethodComponent } from './components/payment-method/payment-method.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserLoginComponent } from './components/user-login/user-login.component'
import { NotificationComponent } from './components/notification/notification.component';
import { CouponComponent } from './components/coupon/coupon.component';
import { TakejobComponent } from './components/takejob/takejob.component';
import { BookingDetailsComponent } from './components/booking-details/booking-details.component';
import { RideclassComponent } from './components/rideclass/rideclass.component';
import { CarComponent } from './components/car/car.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { WithdrawalsComponent } from './components/withdrawals/withdrawals.component';
import { CarDetailsComponent } from './components/car-details/car-details.component';
import { WithdrawalsDetailComponent } from './components/withdrawals-detail/withdrawals-detail.component';
import { PaymentsDetailComponent } from './components/payments-detail/payments-detail.component';
import { DriverListComponent } from './components/driver-list/driver-list.component';
import { DriverDetailComponent } from './components/driver-detail/driver-detail.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full' },
  {path: 'auth/signin', component: UserLoginComponent},
  {path: 'auth/signup', component: UserAddComponent},
  {path: 'user', component: UserListComponent},
  {path: 'user/booking', component: BookingComponent},
  {path: 'user/history', component: HistoryComponent},
  {path: 'user/rideclass', component: RideclassComponent},
  {path: 'user/notification', component: NotificationComponent},
  {path: 'user/car', component: CarComponent},
  {path: 'user/car/:id', component: CarDetailsComponent},
  {path: 'user/payments', component: PaymentsComponent},
  {path: 'user/payments/:id', component: PaymentsDetailComponent},
  {path: 'user/withdrawal', component: WithdrawalsComponent},
  {path: 'user/withdrawal/:id', component: WithdrawalsDetailComponent},
  {path: 'user/coupon', component: CouponComponent},
  {path: 'user/paymentM', component: PaymentMethodComponent},
  {path: 'user/booking/:id', component: BookingDetailsComponent},
  {path: 'user/takejob', component: TakejobComponent},
  {path: 'user/list', component: UserListComponent},
  {path: 'user/detail/:id', component: UserDetailComponent},
  {path: 'driver/list', component: DriverListComponent},
  {path: 'driver/detail/:id', component: DriverDetailComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes), 
    LeafletModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
