import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Car } from '../models/car.model';
import { PaymentMethod } from '../models/payment-m.model';
import { Promo } from '../models/promo.model';
import { Rideclass } from '../models/rideclass.model';
import { Withdrawal } from '../models/withdrawal.model';

const baseUrl = 'http://localhost:8080/api/';
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  getBaseUrl(){
    return baseUrl;
  }
  constructor(private http: HttpClient) {  }
  dashboard(): Observable<any>{
    return this.http.get<any>(baseUrl+'booking/dashboard');
  }
  nav(): Observable<any>{
    return this.http.get<any>(baseUrl+'booking/nav');
  }
  //coupon
  postCoupon(data: any): Observable<any>{
    return this.http.post<Promo>(baseUrl+'promo', data);
  }
  deleteCoupon(id: any): Observable<any>{
    return this.http.delete(baseUrl+'promo/'+id);
  }
  updateCoupon(data: any, id: any): Observable<any>{
    return this.http.put<Promo>(baseUrl+'promo/'+id, data);
  }
  //Ride Class
  postRideC(data: any): Observable<any>{
    return this.http.post<Rideclass>(baseUrl+'rideclass', data);
  }
  deleteRideC(id: any): Observable<any>{
    return this.http.delete(baseUrl+'rideclass/'+id);
  }
  updateRideC(data: any, id: any): Observable<any>{
    return this.http.put<Rideclass>(baseUrl+'rideclass/'+id, data);
  }
  //Payment Method
  postPaymentM(data: any): Observable<any>{
    return this.http.post<PaymentMethod>(baseUrl+'paymentM', data);
  }
  deletePaymentM(id: any): Observable<any>{
    return this.http.delete(baseUrl+'paymentM/'+id);
  }
  updatePaymentM(data: any, id: any): Observable<any>{
    return this.http.put<PaymentMethod>(baseUrl+'paymentM/'+id, data);
  }
  //Withdrawal
  postWithdrawal(data: any): Observable<any>{
    return this.http.post<Withdrawal>(baseUrl+'withdrawal', data);
  }
  deleteWithdrawal(id: any): Observable<any>{
    return this.http.delete(baseUrl+'withdrawal/'+id);
  }
  updateWithdrawal(data: any, id: any): Observable<any>{
    return this.http.put<Withdrawal>(baseUrl+'withdrawal/'+id, data);
  }
  //
  getAllCar(): Observable<any>{
    return this.http.get(baseUrl+'car/all');
  }
  getCarByID(id: any): Observable<any>{
    return this.http.get(`${baseUrl}car/${id}`);
  }
  verifCar(id: any, verif?: boolean): Observable<any>{
    return this.http.put<boolean>(`${baseUrl}car/${id}/verification`, verif);
  }
  updateCar(id: any, data: any): Observable<any>{
    return this.http.put<Car>(`${baseUrl}car/${id}`, data);
  }
  //
  getAllPayment(): Observable<any>{
    return this.http.get(baseUrl+'payment');
  }
  getPayment(id: any): Observable<any>{
    return this.http.get(baseUrl+'payment/'+id);
  }
  //
  getAllUser(): Observable<any>{
    return this.http.get(baseUrl+'user');
  }
  getUser(id: any): Observable<any>{
    return this.http.get(baseUrl+'user/'+id);
  }
  //
  getAllBookingById(id: any, roles: any): Observable<any>{
    if(roles == 'client'){
      return this.http.get(baseUrl+'booking?customer='+id);
    }else if(roles == 'driver'){
      return this.http.get(baseUrl+'booking?driver='+id);
    }else{
      return this.http.get(baseUrl+'booking');
    }
  }
  //
  getAllDriver(): Observable<any>{
    return this.http.get(baseUrl+'driver');
  }
  getDriver(id: any): Observable<any>{
    return this.http.get(baseUrl+'driver/'+id);
  }
}
