import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Booking } from '../models/booking.model';
import { PaymentMethod } from '../models/payment-m.model';
import { Payment } from '../models/payment.model';
import { Promo } from '../models/promo.model';
import { User } from '../models/user.model';

const baseUrl = 'http://localhost:8080/api/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {  }
  getAllUser(): Observable<User[]>{
    return this.http.get<User[]>(baseUrl);
  }
  findUserByEmail(email: String): Observable<User>{
    return this.http.get<User>(baseUrl);
  }
  login(email: String, password: String): Observable<any>{
    return this.http.post<User>(baseUrl+'web/signin', {email, password}, httpOptions);
  }
  register(data: any): Observable<any>{
    return this.http.post(baseUrl + 'user/signup', data);
  }
  update(id: any, data: any): Observable<any>{
    return this.http.put(`${baseUrl}/${id}`, data);
  }
  delete(id: any): Observable<any>{
    return this.http.delete(`${baseUrl}/${id}`);
  }
  makeBook(body: any, id: String): Observable<any>{
    return this.http.post<User>(baseUrl+'booking/'+id, body);
  }
  //
  getAllPaymentM(): Observable<Payment[]>{
    return this.http.get<Payment[]>(baseUrl+'paymentM');
  }
  savePaymentM(data: any): Observable<any>{
    return this.http.post(baseUrl+'paymentM', data);
  }
  getAllPromo(): Observable<Promo[]>{
    return this.http.get<Promo[]>(baseUrl+'promo');
  }
  getAllBooking(): Observable<any>{
    return this.http.get<any>(`${baseUrl}booking`);
  }
  getUserBooking(id: String): Observable<any>{
    return this.http.get<any>(`${baseUrl}booking/${id}`);
  }
  getAllNotif(): Observable<any>{
    return this.http.get<Notification[]>(`${baseUrl}notif`);
  }

  getAllRideClass(): Observable<any>{
    return this.http.get<any>(`${baseUrl}rideclass`);
  }
  //
  getAllJob(lat: String, lng: String): Observable<any>{
    return this.http.post<any>(`${baseUrl}job`, {lat, lng});
  }
  takeJob(idx: String, uid: String): Observable<any>{
    const id = [idx, uid];
    return this.http.post(`${baseUrl}takejob`, JSON.stringify(id),httpOptions);
  }
  //
  getAllWithdrawal(): Observable<any>{
    return this.http.get(`${baseUrl}withdrawal`);
  }
  getWithdrawal(id: any): Observable<any>{
    return this.http.get(`${baseUrl}withdrawal/${id}`);
  }
}
