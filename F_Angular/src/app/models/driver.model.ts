export class Driver {
    id?: any;
    offline?: any;
    verified?: any;
    takingJob?: any;
    profile?: any;
    balance?: any;
    name?: any;
    sim?: any;
    stnk: String[] = [];
    //status -1 denied, 0 not yet, 1 uploaded, 2 accepted
    statusSIM?: any;
    statusSTNK?: any;
    statusPhotoProfile?: any;
}
