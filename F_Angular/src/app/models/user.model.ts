import { Booking } from "./booking.model";

export class User {
    id?: any;
    email?: String;
    password?: String;
    booking?: Booking[];
    notif?: Notification[];
    name?: String;
    balance?: number;
}
