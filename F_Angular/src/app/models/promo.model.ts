export class Promo {
    id?: any;
    title?:String;
    body?: String;
    dateStart?: any;
    dateEnd?: any;
    code?: String;
    discount?: any;
}
