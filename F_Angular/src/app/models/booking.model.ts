import { NotExpr } from "@angular/compiler";
import { Payment } from "./payment.model";

export class Booking {
    
    id?: any;
    customer?: String;
    driver?: String;
    payment?: String;
    rideclass?: String;
    status?: number;

    startLoc?: any;
    startLocName?: String;
    startTime?: any;

    endLoc?: any;
    endLocName?: String;
    endTime?: any;

    driverLoc?: any;

    note?: String;
    rating?: number;
    totalKM?: number;
}
