export class PaymentMethod {
    id?: any;
    name?: String;
    url?: String;
    icon?: any;
    fee?: number;
}
