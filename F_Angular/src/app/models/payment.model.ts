export class Payment {
    id?: any;
    userID?: String;
    paymentMethod?: String;
    promo?: String;
    paymentRetrieve?: any;
    paymentType?: any;
    total?: number;
}
