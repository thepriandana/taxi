import { Rideclass } from "./rideclass.model";

export class Car {
    id?: any;
    rideClass?: String;
    driver?: String;
    verified?: boolean;
    photo?: any;
    plate?: String;
    lat?: any;
    lng?: any;
    rotationDeg?: any;
}
