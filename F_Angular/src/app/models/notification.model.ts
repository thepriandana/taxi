export class Notification {
    id?: any;
    title?: String;
    subtitle?: String;
    htmlString?: String;
}
