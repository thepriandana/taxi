import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-nav-side',
  templateUrl: './nav-side.component.html',
  styleUrls: ['./nav-side.component.scss']
})
export class NavSideComponent implements OnInit {

  isLoggedIn = false;
  roles: string = '';
  names: string = '';
  booking: number= 0;
  car: number=0;
  payment : number = 0;
  constructor(private storageService: StorageService, public router: Router, private service: AdminService) { }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.names = this.storageService.getUser().name;
      if(this.roles == "admin"){
      this.service.nav().subscribe({
        next: (data) =>{
          this.car = data.message.car;
          this.booking = data.message.booking;
          this.payment = data.message.payment;
        }
      });
      }
    }else{
      this.router.navigate(["/auth/signin"]);
    }
  }
  logout(): void{
    this.storageService.clean();
    this.isLoggedIn = false;
    this.router.navigate(["/auth/signin"]);
  }

  viewDriver(){
    this.router.navigateByUrl('driver/list', {
      state: {role: 'driver'}
    });
  }
  viewClient(){
    this.router.navigateByUrl('user/list', {
      state: {role: 'client'}
    });
  }
}
