import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Booking } from 'src/app/models/booking.model';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  textPickUp="";
  textDropOff="";
  isLoggedIn = false;
  booking: Booking[] = [];
  timeLeft: number = 60;
  interval: any;
  subscribeTimer: any;
  isTimerStart: boolean = false;
  constructor(private services: UserService, private storageService: StorageService, private router: Router) { }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.services.getAllBooking().subscribe({
        next: (data) =>{
          this.booking = data;
          this.booking.forEach((val) => {
            return val;
          })
          }
      });
    }else{
      this.router.navigate(['/auth/signin']);
    }
  }
  fetchLocation(event: any, isDropoff: boolean) {
    if(isDropoff){
      this.textDropOff = event.target.value;
    }else{
      this.textPickUp = event.target.value;
    }
    if(this.isTimerStart == false){
      this.isTimerStart = true;
       this.observableTimer(event, isDropoff);
      }
  };
  observableTimer(event: any, isDropoff: boolean) {
    const source = timer(0, 10);
    const abc = source.subscribe(val => {
      this.subscribeTimer = this.timeLeft - val;
      if(this.subscribeTimer == 0) {
        console.log(event.target.value);
        this.isTimerStart = false;
        abc.unsubscribe();
      }
    });
  }
  peekDetail(id: any){
    this.router.navigate(['/user/booking/'+id]);
  }
  filterActive(){
    return this.booking.filter(x=> x.status == 1)
  }
  filterRequested(){
    return this.booking.filter(x=> x.status == 0)
  }
  filterFinish(){
    return this.booking.filter(x=> x.status == 3)
  }
}
