import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Car } from 'src/app/models/car.model';
import { Rideclass } from 'src/app/models/rideclass.model';
import { AdminService } from 'src/app/services/admin.service';
import { ScriptsService } from 'src/app/services/scripts.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  roles: string = '';
  car: Car[] = [];
  rideClass: Rideclass[] = [];
  isLoggedIn = false;
  constructor(private storageService: StorageService, 
    public router: Router, private adminService: AdminService,
     private userService: UserService,
     private script: ScriptsService) { }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.adminService.getAllCar().subscribe({
        next: (res) => {
          this.car = res;
        }
      });
      this.userService.getAllRideClass().subscribe({
        next: (res) => {
          this.rideClass = res;
        }
      });
    }
  }
  rideClassMe(id: any){
    var a = this.rideClass.filter(x => x.id == id)[0];
    if(a == null) return "(empty)";
    else return a.className;
  }
  filterVerified(){
    return this.car.filter(x => x.verified == false);
  }
}
