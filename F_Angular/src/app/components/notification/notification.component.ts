import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { Notification } from 'src/app/models/notification.model';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  isLoggedIn = false;
  notif?: Notification[];
  roles: string = '';
  constructor(private userService: UserService, private storageService: StorageService, public router: Router) { }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.userService.getAllNotif().subscribe({
        next: (res) => {
          this.notif = res;
        }
      }
      );
    }else{
      this.router.navigate(["/auth/signin"]);
    }
  }

}
