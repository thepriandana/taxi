import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentMethod } from 'src/app/models/payment-m.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;
@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss']
})
export class PaymentMethodComponent implements OnInit {
  isLoggedIn = false;
  isEdit = false;
  isSuccessfull = false;
  roles: string = '';
  constructor(private userService: UserService, private storageService: StorageService,
    public router: Router, private adminService: AdminService) { }
  id_selected?: String;
  form = {
    name: '',
    url: '',
    icon: '',
    fee: '',
  };
  isFailed = false;
  errorMessage = '';
  paymentM: PaymentMethod[] = [];
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.userService.getAllPaymentM().subscribe({
        next: (data) => {
          this.paymentM = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }
  onSubmit(): void {
    if (this.roles == 'admin') {
      const data = {
        name: this.form.name,
        url: this.form.url,
        icon: this.form.icon,
        fee: this.form.fee,
      }
      if (this.isEdit) {
        this.adminService.updatePaymentM(data, this.id_selected).subscribe({
          next: (res) => {
            this.isSuccessfull = true;
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
              this.router.navigate(['user/paymentM']);
            });
          },
          error: (err) => {
            this.errorMessage = err.error.message;
            this.isFailed = true;
          }
        });
      } else {
        this.adminService.postPaymentM(data).subscribe({
          next: (res) => {
            this.isSuccessfull = true;
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
              this.router.navigate(['user/paymentM']);
            });
          },
          error: (err) => {
            this.errorMessage = err.error.message;
            this.isFailed = true;
          }
        });
      }
    }
  }
  openModal(num: String) {
    (function ($) {
      $("#modal-remove").modal("toggle");
    })(jQuery);
    this.id_selected = num;
  }
  deactiveItem() {
    this.adminService.deletePaymentM(this.id_selected).subscribe({
      next: (res) => {
        //refresh all
        this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
          this.router.navigate(['user/paymentM']);
        });
      }, error: (err) => {

      }
    });
  }
  peekItem(id: String) {
    this.isEdit = true;
    this.id_selected = id;
    var c: PaymentMethod = this.paymentM.filter(x => x.id == id)[0];
    this.form = {
      name: `${c.name}`,
      url: `${c.url??''}`,
      icon: `${c.icon??''}`,
      fee: `${c.fee}`,
    };
  }
  cancelPeek() {
    this.isEdit = false;
    this.form = {
      name: '',
      url: '',
      icon: '',
      fee: '',
    };
  }
}
