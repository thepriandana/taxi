import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Promo } from 'src/app/models/promo.model';
import { AdminService } from 'src/app/services/admin.service';
import { ScriptsService } from 'src/app/services/scripts.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;
@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.scss']
})  

export class CouponComponent implements OnInit {
  isEdit = false;
  isLoggedIn = false;
  isSuccessfull = false;
  roles: string = '';
  coupon: Promo[] = [];
  todayDate: number = Date.parse(formatDate(new Date(), 'yyyy-MM-dd', 'en'));
  id_selected?: String;
  form = {
    title: '',
    dateStart: '',
    dateEnd: '',
    discount: '',
    code: '',
    body: '',
  };
  isFailed = false;
  errorMessage = '';
  constructor(private userService: UserService, private storageService: StorageService, 
    public router: Router, private adminService: AdminService, private script: ScriptsService) { }
  ngOnInit(): void {
    
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.userService.getAllPromo().subscribe({
        next: (response) => {
          this.coupon = response;
        }
      })
      this.script.load('summernote').then(data => {
        (function ($) {
          // $(document).ready(function(){
            $('body').append(`<link rel="stylesheet" href="assets/plugins/summernote/summernote-bs4.min.css">`);
            $('#compose-textarea').summernote();
            $("#compose-textarea").summernote('reset');
          // });
        })(jQuery);
    }).catch(error => console.log(error));

    }else{
      this.router.navigate(["/auth/signin"]);
    }
  }
  onSubmit(){
    if(this.roles=='admin'){
      const data = {
        title: this.form.title,
        discount: this.form.discount,
        code: this.form.code,
        dateStart: this.form.dateStart,
        dateEnd: this.form.dateEnd,
        body: (<HTMLInputElement>document.getElementById("compose-textarea")).value
      }
      if(this.isEdit){
        this.adminService.updateCoupon(data, this.id_selected).subscribe({
          next: (res) =>{
            this.isSuccessfull = true;
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
              this.router.navigate(['user/coupon']);
            }); 
          },
          error: (err) =>{
            this.errorMessage = err.error.message;
            this.isFailed = true;
          }
        });
      }else{
        this.adminService.postCoupon(data).subscribe({
          next: (res) =>{
            this.isSuccessfull = true;
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
              this.router.navigate(['user/coupon']);
            }); 
          },
          error: (err) =>{
            this.errorMessage = err.error.message;
            this.isFailed = true;
          }
        });
      }
      
    }
  }

  filterIncoming(){
    return this.coupon.filter(x => this.todayDate < Date.parse(x.dateStart));
  }
  filterActive(){
    return this.coupon.filter(x => this.todayDate >= Date.parse(x.dateStart)
    && Date.parse(x.dateEnd) >= this.todayDate);
  }

  openModal(num: String){
    (function($){
      $("#modal-remove").modal("toggle");
    })(jQuery);
    this.id_selected = num;
  }
  deactivePromo(){
    this.adminService.deleteCoupon(this.id_selected).subscribe({
      next: (res) =>{
        //refresh all
        this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
          this.router.navigate(['user/coupon']);
        }); 
      }, error: (err) =>{

      }
    });
  }
  peekPromo(id: String){
    this.isEdit = true;
    this.id_selected = id;
    var c: Promo = this.coupon.filter(x => x.id == id)[0];
    this.form = {
      title: `${c.title}`,
      dateStart: `${c.dateStart}`,
      dateEnd: `${c.dateEnd}`,
      discount: `${c.discount}`,
      code: `${c.code}`,
      body: `${c.body}`,
    };
    (function($){
      $("#compose-textarea").summernote('reset');
      $("#compose-textarea").summernote('pasteHTML', `${c.body}`);
    })(jQuery);
  }
  cancelPeek(){
    this.isEdit = false;
    this.form = {
      title: '',
      dateStart: '',
      dateEnd: '',
      discount: '',
      code: '',
      body: '',
    };
    
    (function($){
      $("#compose-textarea").summernote('reset');
    })(jQuery);
  }
}

