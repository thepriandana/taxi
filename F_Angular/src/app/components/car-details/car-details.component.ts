import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Booking } from 'src/app/models/booking.model';
import { Car } from 'src/app/models/car.model';
import { Rideclass } from 'src/app/models/rideclass.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;
@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {
  isEditPlate = false;
  isEditRideClass = false;
  isLoggedIn = false;
  id: string = '';
  car?: Car;
  roles: string = '';
  isAccepted?: boolean;
  rideclass: Rideclass[] = [];
  form = {
    plate: '',
    rideClass: ''
  }
  constructor(private userService: UserService, 
    private services: AdminService, private storageService: StorageService, public router: Router,
    private route: ActivatedRoute) { }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.id = this.route.snapshot.params["id"];
      this.roles = this.storageService.getUser().roles;
      this.userService.getAllRideClass().subscribe({
        next: (response) => {
          this.rideclass = response;
        }
      })
      this.services.getCarByID(this.id).subscribe({
        next: (data) => {
          this.car = JSON.parse(JSON.stringify(data));
        }
      });
      this.form = {
        plate: `${this.car?.plate}`,
        rideClass: `${this.car?.rideClass}`,
      };
    } else {
      this.router.navigate(['/auth/signin']);
    }
  }
  urlFix(id: any) {
    return `${this.services.getBaseUrl()}car/files/${id}`;
  }
  setVerification() {
    this.services.verifCar(this.car?.id, this.isAccepted).subscribe();
    this.services.getCarByID(this.id).subscribe({
      next: (data) => {
        this.car = JSON.parse(JSON.stringify(data));
        (function ($) {
          $("#modal-detail").modal("toggle");
        })(jQuery);
      }
    });
  }
  openModal(owo: boolean) {
    this.isAccepted = owo;
    (function ($) {
      $("#vstatus").html((owo) ? 'accept' : 'decline');
      $("#modal-detail").modal("toggle");
    })(jQuery);
  }
  changeRideClass(e: any){
    this.form = {
      plate: this.form.plate,
      rideClass: e.target.value
    };
  }
  update(type: any) {
    if (this.roles == 'admin') {
      var data;
      if(type == "plate"){
        data = {
          plate: this.form.plate
        }
      }else if(type == "rideclass"){
        data = {
          rideClass: this.form.rideClass
        }
      }
      this.services.updateCar(this.car?.id, data).subscribe({
        next: (res) => {
          if(type=='plate'){
            this.isEditPlate = false;
          }else if(type=='rideclass'){
            this.isEditRideClass = false;
          }
        },
        error: (err) => {

        }
      });
    }
  }
  
  rideClassMe(id: any){
    var a = this.rideclass.filter(x => x.id == id)[0];
    if(a == null) return "(empty)";
    else return a.className;
  }
}
