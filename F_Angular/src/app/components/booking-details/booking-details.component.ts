import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/models/booking.model';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { icon, latLng, marker, tileLayer } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import * as L from 'leaflet';
@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: [
    './booking-details.component.scss',
]
})

export class BookingDetailsComponent implements OnInit{
  // options = {
  //   layers: [
  //     tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  //       attribution: '&copy; OpenStreetMap contributors'
  //     })
  //   ],
  //   zoom: 7,
  //   center: latLng([ 46.879966, -121.726909 ])
  // };
  isLoggedIn = false;
  id: string = '';
  booking?: Booking;
  constructor(private services: UserService, private storageService: StorageService, public router: Router,
    private route: ActivatedRoute) { }
  ngOnInit(): void {
    L.Icon.Default.imagePath = "assets/";
    if (this.storageService.isLoggedIn()) {
      let map = L.map('map').setView([0, 0], 13);
      this.isLoggedIn = true;
      this.id = this.route.snapshot.params["id"];
      this.services.getUserBooking(this.id).subscribe({
        next: (data) =>{
          // console.log(data);
          // this.booking = JSON.parse(data).message["message"];
          this.booking = JSON.parse(JSON.stringify(data)).message;
          L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga', {
              attribution: '© Google', detectRetina: true
          }).addTo(map);
          map.panTo(new L.LatLng(this.booking?.startLoc.lat, this.booking?.startLoc.lng));
          L.marker([this.booking?.startLoc.lat, this.booking?.startLoc.lng], {zIndexOffset: 1000}).addTo(map);
          L.marker([this.booking?.endLoc.lat, this.booking?.endLoc.lng], {zIndexOffset: 1000}).addTo(map);
        }
      });
    }else{
      this.router.navigate(['/auth/signin']);
    }
  }

  
  // onMapReady(map: L.Map) {
  // }
  // // Define our base layers so we can reference them multiple times
  // streetMaps = tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga', {
  //   detectRetina: true,
  //   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  // });
  // // Marker for the top of Mt. Ranier
  // summit = marker([ 46.8523, -121.7603 ], {
  //   icon: icon({
  //     iconSize: [ 25, 41 ],
  //     iconAnchor: [ 13, 41 ],
  //     iconUrl: 'assets/marker-icon.png',
  //     shadowUrl: 'assets/marker-shadow.png'
  //   })
  // });

  // // Marker for the parking lot at the base of Mt. Ranier trails
  // paradise = marker([ 46.78465227596462,-121.74141269177198 ], {
  //   icon: icon({
  //     iconSize: [ 25, 41 ],
  //     iconAnchor: [ 13, 41 ],
  //     iconUrl: 'assets/marker-icon.png',
  //     iconRetinaUrl: 'assets/marker-icon-2x.png',
  //     shadowUrl: 'assets/marker-shadow.png'
  //   })
  // });

  // // Layers control object with our two base layers and the three overlay layers
  // layersControl = {
  //   baseLayers: {
  //     'Street Maps': this.streetMaps,
  //   },
  //   overlays: {
  //     'Mt. Rainier Summit': this.summit,
  //     'Mt. Rainier Paradise Start': this.paradise,
  //   }
  // };


  // // Set the initial set of displayed layers (we could also use the leafletLayers input binding for this)
  // options = {
  //   layers: [ this.streetMaps, this.summit, this.paradise ],
  //   zoom: 7,
  //   center: latLng([ 46.879966, -121.726909 ])
  // };

}
