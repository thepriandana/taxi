import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakejobComponent } from './takejob.component';

describe('TakejobComponent', () => {
  let component: TakejobComponent;
  let fixture: ComponentFixture<TakejobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakejobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakejobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
