import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-takejob',
  templateUrl: './takejob.component.html',
  styleUrls: ['./takejob.component.scss']
})
export class TakejobComponent implements OnInit {
  isLoggedIn = false;
  roles: string = '';
  job?:any[];
  constructor(private userService: UserService, private storageService: StorageService, public router: Router) { }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      if(this.roles != 'driver')
        this.router.navigate(["/auth/home"]);
      else{
        this.userService.getAllJob('0','0').subscribe({
          next: (res) => {
            this.job = res;
          }
        });
      }
    }else{
      this.router.navigate(["/auth/signin"]);
    }
  }
  takejob(id: String) {
    this.userService.takeJob(id, this.storageService.getUser().id).subscribe({
      next: (res) => {
        if(res.message == "Success"){
          this.router.navigate(["/user/booking/detail"]);
        }
      }
    });
  }
}
