import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Booking } from 'src/app/models/booking.model';
import { Driver } from 'src/app/models/driver.model';
import { User } from 'src/app/models/user.model';
import { Withdrawal } from 'src/app/models/withdrawal.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.component.html',
  styleUrls: ['./driver-detail.component.scss']
})
export class DriverDetailComponent implements OnInit {
  isLoggedIn = false;
  user?: Driver;
  booking?: Booking[] = [];
  withdrawals?: Withdrawal[] = [];
  roles: string = '';
  id_selected?: String;
  id: String = '';
  constructor(private storageService: StorageService,
    public router: Router, private adminService: AdminService,
    private route: ActivatedRoute) {
      // console.log('>>authenticate-username:41:',
      // this.router.getCurrentNavigation()?.extras.state);
    }
    
  urlFix(id: any) {
    return `${this.adminService.getBaseUrl()}driver/files/${id}`;
  }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.id = this.route.snapshot.params["id"];
      this.adminService.getDriver(this.id).subscribe({
        next: (data) => {
          this.user = data;
          this.withdrawals = data.withdrawals;
          for(let x of data.stnk.List){
            this.user?.stnk.push(x);
          }
        }
      });
      this.adminService.getAllBookingById(this.id, 'driver').subscribe({
        next: (data) => {
          this.booking = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }
  peekPayment(id: any){
    this.router.navigate(['user/payments/'+id])
  }
  peekBooking(id: any){
    this.router.navigate(['/user/booking/'+id]);
  }

}
