import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Promo } from 'src/app/models/promo.model';
import { Rideclass } from 'src/app/models/rideclass.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;
@Component({
  selector: 'app-rideclass',
  templateUrl: './rideclass.component.html',
  styleUrls: ['./rideclass.component.scss']
})
export class RideclassComponent implements OnInit {
  isEdit = false;
  isLoggedIn = false;
  isSuccessfull = false;
  roles: string = '';
  rideclass: Rideclass[] = [];
  id_selected?: String;
  form = {
    className: '',
    price: '',
    pax: '',
  };
  isFailed = false;
  errorMessage = '';
  constructor(private userService: UserService, private storageService: StorageService,
    public router: Router, private adminService: AdminService) { }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.userService.getAllRideClass().subscribe({
        next: (response) => {
          this.rideclass = response;
        }
      })
    } else {
      this.router.navigate(["/auth/signin"]);
    }
  }
  onSubmit() {
    if (this.roles == 'admin') {
      const data = {
        className: this.form.className,
        price: this.form.price,
        pax: this.form.pax,
      }
      if (this.isEdit) {
        this.adminService.updateRideC(data, this.id_selected).subscribe({
          next: (res) => {
            this.isSuccessfull = true;
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
              this.router.navigate(['user/rideclass']);
            });
          },
          error: (err) => {
            this.errorMessage = err.error.message;
            this.isFailed = true;
          }
        });
      } else {
        this.adminService.postRideC(data).subscribe({
          next: (res) => {
            this.isSuccessfull = true;
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
              this.router.navigate(['user/rideclass']);
            });
          },
          error: (err) => {
            this.errorMessage = err.error.message;
            this.isFailed = true;
          }
        });
      }
    }
  }
  openModal(num: String) {
    (function ($) {
      $("#modal-remove").modal("toggle");
    })(jQuery);
    this.id_selected = num;
  }
  deactiveItem() {
    this.adminService.deleteRideC(this.id_selected).subscribe({
      next: (res) => {
        //refresh all
        this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
          this.router.navigate(['user/rideclass']);
        });
      }, error: (err) => {

      }
    });
  }
  peekItem(id: String) {
    this.isEdit = true;
    this.id_selected = id;
    var c: Rideclass = this.rideclass.filter(x => x.id == id)[0];
    this.form = {
      className: `${c.className}`,
      pax: `${c.pax}`,
      price: `${c.price}`,
    };
  }
  cancelPeek() {
    this.isEdit = false;
    this.form = {
      className: '',
      price: '',
      pax: '',
    };
  }

}
