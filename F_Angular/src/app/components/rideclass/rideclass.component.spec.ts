import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RideclassComponent } from './rideclass.component';

describe('RideclassComponent', () => {
  let component: RideclassComponent;
  let fixture: ComponentFixture<RideclassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RideclassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RideclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
