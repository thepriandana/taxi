import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { Booking } from 'src/app/models/booking.model';
import { User } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  isLoggedIn = false;
  user?: User;
  booking?: Booking[] = [];
  roles: string = '';
  id_selected?: String;
  id: String = '';
  constructor(private storageService: StorageService,
    public router: Router, private adminService: AdminService,
    private route: ActivatedRoute) {
      // console.log('>>authenticate-username:41:',
      // this.router.getCurrentNavigation()?.extras.state);
    }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.id = this.route.snapshot.params["id"];
      this.adminService.getUser(this.id).subscribe({
        next: (data) => {
          this.user = data;
        }
      });
      this.adminService.getAllBookingById(this.id, 'client').subscribe({
        next: (data) => {
          this.booking = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }
  peekPayment(id: any){
    this.router.navigate(['user/payments/'+id])
  }
  peekBooking(id: any){
    this.router.navigate(['/user/booking/'+id]);
  }
}
