import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Withdrawal } from 'src/app/models/withdrawal.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;
@Component({
  selector: 'app-withdrawals',
  templateUrl: './withdrawals.component.html',
  styleUrls: ['./withdrawals.component.scss']
})
export class WithdrawalsComponent implements OnInit {
  isLoggedIn = false;
  isSuccessfull = false;
  roles: string = '';
  constructor(private userService: UserService, private storageService: StorageService,
    public router: Router, private adminService: AdminService) { }
  id_selected?: String;
  withdrawals: Withdrawal[] = [];
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.userService.getAllWithdrawal().subscribe({
        next: (data) => {
          this.withdrawals = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }

  filterRequest(){
    return this.withdrawals.filter(x => x.status == 0);
  }
  filterNeedFinishing(){
    return this.withdrawals.filter(x => x.status == 1);
  }
}
