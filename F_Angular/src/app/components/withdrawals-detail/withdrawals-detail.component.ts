import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Withdrawal } from 'src/app/models/withdrawal.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

declare var jQuery: any;
@Component({
  selector: 'app-withdrawals-detail',
  templateUrl: './withdrawals-detail.component.html',
  styleUrls: ['./withdrawals-detail.component.scss']
})
export class WithdrawalsDetailComponent implements OnInit {
  id_action: any = 0;
  id: string = '';
  withdrawal?: Withdrawal;
  roles: string = '';
  isLoggedIn: boolean = false;
  constructor(private userService: UserService,
    private services: AdminService, private storageService: StorageService, public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.id = this.route.snapshot.params["id"];
      this.roles = this.storageService.getUser().roles;

      this.userService.getWithdrawal(this.id).subscribe({
        next: (data) => {
          this.withdrawal = JSON.parse(JSON.stringify(data));
        }
      });
    } else {
      this.router.navigate(['/auth/signin']);
    }
  }
  proccess() {
    const data = {
      status: this.id_action
    };
    this.services.updateWithdrawal(data, this.id).subscribe({
      next: (data) => {
        this.userService.getWithdrawal(this.id).subscribe({
          next: (data) => {
            this.withdrawal = JSON.parse(JSON.stringify(data));
          }
        });
        (function ($) {
          $("#modal-detail").modal("toggle");
        })(jQuery);
        
      }
    })
  }

  openModal(owo: any) {
    (function ($) {
      if (owo == 'd') {
        $("#vstatus").html("Decline Request");
      } else if (owo == 'a') {
        $("#vstatus").html("Accept Request");
      } else if (owo == 'c') {
        $("#vstatus").html("Finish Transfering Request");
      }
      $("#modal-detail").modal("toggle");
    })(jQuery);
    if (owo == 'd') {
      this.id_action = -1;
    } else if (owo == 'a') {
      this.id_action = 1;
    } else if (owo == 'c') {
      this.id_action = 2;
    }
  }
}
