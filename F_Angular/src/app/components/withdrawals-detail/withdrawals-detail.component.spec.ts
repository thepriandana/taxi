import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawalsDetailComponent } from './withdrawals-detail.component';

describe('WithdrawalsDetailComponent', () => {
  let component: WithdrawalsDetailComponent;
  let fixture: ComponentFixture<WithdrawalsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WithdrawalsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
