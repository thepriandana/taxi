import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Point } from 'leaflet';
import { Booking } from 'src/app/models/booking.model';
import { DashboardTarget } from 'src/app/models/dashboard-target.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

import { icon, latLng, marker, tileLayer } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import * as L from 'leaflet';
import 'heatmap.js';
declare const HeatmapOverlay: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private map: any;
  isLoggedIn = false;
  roles: string = '';
  data?: any;
  wToday: number = 0;
  wMonth: number = 0;
  wYear: number = 100;
  targetDashboard: DashboardTarget[] = [];
  activeBooking: Booking[] = [];
  date = new Date();
  firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
  point: Point[] = [];
  constructor(private storageService: StorageService, public router: Router, private service: AdminService) { }
  ngOnInit(): void {
    L.Icon.Default.imagePath = "assets/";
    if (this.storageService.isLoggedIn()) {
      this.map = L.map('map').setView([0, 0], 13);
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      if (this.roles == "admin") {
        L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga', {
          attribution: '© Google', detectRetina: true
        }).addTo(this.map);
        this.map.panTo(new L.LatLng(0,0));
        // const heatLayerConfig = {
        //   "radius": 5,
        //   "maxOpacity": .8,
        //   "scaleRadius": true,
        //   // property below is responsible for colorization of heat layer
        //   "useLocalExtrema": true,
        //   // here we need to assign property value which represent lat in our data
        //   latField: 'lat',
        //   // here we need to assign property value which represent lng in our data
        //   lngField: 'lng',
        //   // here we need to assign property value which represent valueField in our data
        //   valueField: 'count'
        // };
        // const heatmapLayer = new HeatmapOverlay(heatLayerConfig);
        this.service.dashboard().subscribe({
          next: (data) => {
            this.data = data.message;
            this.wToday = (this.data?.todayBooking / this.data?.todayGoal) * 100;
            this.wMonth = (this.data?.monthBooking / this.data?.monthGoal) * 100;
            this.wYear = (this.data?.yearBooking / this.data?.yearGoal) * 100;
            this.targetDashboard = this.data?.monthActivity;
            this.activeBooking = this.data?.activeToday;
            // for (var val of this.activeBooking) {
            //   this.point.push(val.startLoc);
            // }
            
            // heatmapLayer.setData(this.point);
          }
          // heatmapLayer.addTo(map);
        });
      }
    } else {
      this.router.navigate(["/auth/signin"]);
    }
  }

  logout(): void {
    this.storageService.clean();
    this.isLoggedIn = false;
    window.location.reload();
    // this.router.navigate(["/auth/signin"]);
  }
}
