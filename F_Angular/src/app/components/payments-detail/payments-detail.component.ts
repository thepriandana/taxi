import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Payment } from 'src/app/models/payment.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-payments-detail',
  templateUrl: './payments-detail.component.html',
  styleUrls: ['./payments-detail.component.scss']
})
export class PaymentsDetailComponent implements OnInit {

  isLoggedIn = false;
  payment?: Payment;
  roles: string = '';
  constructor(private storageService: StorageService,
    public router: Router, private adminService: AdminService,
    private route: ActivatedRoute) { }
  id_selected?: String;
  id: String = '';
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.id = this.route.snapshot.params["id"];
      this.adminService.getPayment(this.id).subscribe({
        next: (data) => {
          this.payment = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }

}
