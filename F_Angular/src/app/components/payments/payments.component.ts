import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Payment } from 'src/app/models/payment.model';
import { Withdrawal } from 'src/app/models/withdrawal.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';
@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  isLoggedIn = false;
  payment: Payment[] = [];
  roles: string = '';
  constructor(private storageService: StorageService,
    public router: Router, private adminService: AdminService) { }
  id_selected?: String;
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.adminService.getAllPayment().subscribe({
        next: (data) => {
          this.payment = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }
  payments: Payment[] = [];
  filterRecent(){
    return this.payments;
  }

}
