import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Driver } from 'src/app/models/driver.model';
import { User } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.scss']
})
export class DriverListComponent implements OnInit {

  isLoggedIn = false;
  user: Driver[] = [];
  roles: string = '';
  constructor(private storageService: StorageService,
    public router: Router, private adminService: AdminService,
    private route: ActivatedRoute) { }
  id_selected?: String;
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.adminService.getAllDriver().subscribe({
        next: (data) => {
          this.user = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }
  peekDetail(id: any){
    this.router.navigateByUrl('driver/detail/'+id, {
      state: {role: 'driver'}
    });
  }
  filterVerification(){
    return this.user.filter(x=> x.verified == 0)
  }
}
