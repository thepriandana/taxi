import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  isLoggedIn = false;
  user: User[] = [];
  roles: string = '';
  constructor(private storageService: StorageService,
    public router: Router, private adminService: AdminService,
    private route: ActivatedRoute) { }
  id_selected?: String;
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.adminService.getAllUser().subscribe({
        next: (data) => {
          this.user = data;
        }
      });
    } else {
      this.router.navigate(['user/signin']);
    }
  }
  peekDetail(id: any){
    this.router.navigateByUrl('user/detail/'+id, {
      state: {role: 'driver'}
    });
  }
}
