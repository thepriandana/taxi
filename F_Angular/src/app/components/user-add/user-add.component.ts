import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  typeU: any = [
    {name: "Client", value:"client"},
    {name: "Admin", value:"admin"},
    {name: "Driver", value:"driver"},
  ];
  form: any = {
    name: '',
    email: '',
    password: '',
    repassword: '',
    selectedType: ''
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  constructor(private userService: UserService, public route: Router) {}
  ngOnInit(): void {}
  onSubmit(): void{
    const data = {
      email: this.form.email,
      name: this.form.name,
      password: this.form.password,
      roles: this.form.selectedType
    }
    if(this.form.password != this.form.repassword){
      this.errorMessage = "Password and re-password is not same";
      this.isSignUpFailed = true;
    }else{
      this.userService.register(data).subscribe({
        next: (res) =>{
          this.isSuccessful = true;
          this.isSignUpFailed = false;
          this.route.navigate(['']);
        },
        error: (err) =>{
          this.errorMessage = err.error.message;
          this.isSignUpFailed = true;
        }
      });
    }
  }
  onTypeChange():void{
    console.log(this.form.selectedType.value);
  }
}
